<?php
$CONFIG_FILE_PATH = "./config/config.ini";
$config = parse_ini_file($CONFIG_FILE_PATH,true);
set_include_path('.' . PATH_SEPARATOR .
    $config['paths']['libraryPath'] . PATH_SEPARATOR .
    get_include_path()
);

$captchaLength = 5;

require_once 'Exoof/Session.php';

    $string = '';
    //$chars = "abcdefghijkmnopqrstuvwxyz23456789";
    $chars = "0123456789";
    for($i=0;$i<$captchaLength;$i++) {
        $pos = rand(0,strlen($chars)-1);
        $string .= $chars{$pos};
    }
    Exoof_Session::set('CaptchaWord',$string);

$w      =   120;
$h      =   48;
$font   =   "./scripts/font.ttf";

$img   = imagecreatetruecolor($w, $h);

$draw_function    = "imagettftext";

$white_color = imagecolorallocate($img, 255, 255, 255);
$bg_color  = imagecolorallocatealpha($img, 154, 188, 166,rand(30,70));

// Define some common colors
$black = imagecolorallocate($img, 0, 0, 0);
$white = imagecolorallocate($img, 255, 255, 255);
$red = imagecolorallocatealpha($img, 255, 0, 0, 75);
$green = imagecolorallocatealpha($img, 0, 255, 0, 75);
$blue = imagecolorallocatealpha($img, 0, 0, 255, 75);

//imagefill($img, 0, 0, $bg_color);
imagefill($img, 0, 0, IMG_COLOR_TRANSPARENT);



for ($i=0;$i<strlen($string);$i++) {
    $colR           = rand(20,70);$colG = rand(20,70);$colB = rand(20,70);
    $text_color = imagecolorallocatealpha($img, $colR, $colG, $colB,rand(10,40));

    if (function_exists($draw_function) && file_exists($font)) {
        imagettftext($img, rand(15,24), rand(-25,25), rand(2+($i*19),10+($i*19)), rand(24,48), $text_color, $font,$string{$i});
    }
    else {
        $x = rand(2+($i*27), 10+($i*20));
        $y = rand(1,33);
        imagechar($img, 5, $x+1, $y+1, $string{$i}, $white_color);
        imagechar($img, 5, $x, $y, $string{$i}, $text_color);
    }
}


for($j=0; $j<10; $j++){
    $colR           = rand(220,235);$colG = rand(220,235);$colB = rand(220,235);
    $f_color = imagecolorallocatealpha($img, $colR, $colG, $colB, rand(50,127));
    imagefilledellipse($img, ceil(rand(5,$w - 5)), ceil(rand(0,$h)), rand(20,50), rand(20,50), $f_color);
}


for($j=0; $j<1000; $j++){
    $colR           = rand(180,200);$colG = rand(180,200);$colB = rand(180,200);
    $f_color = imagecolorallocate($img, $colR, $colG, $colB);
    imagesetpixel($img, ceil(rand(5,$w - 5)), ceil(rand(0,$h)), $f_color);
}

for($j=0; $j<20; $j++){
    $colR           = rand(200,225);$colG = rand(200,225);$colB = rand(200,225);
    $f_color = imagecolorallocatealpha($img, $colR, $colG, $colB, rand(50,70));
    imageline($img, rand(0,$w), rand(0,$h), rand(0,$w), rand(0,$h), $f_color);
}

// Turn off alpha blending and set alpha flag
imagealphablending($img, false);
imagesavealpha($img, true);

header( 'Content-Type: image/png' );

imagepng($img);