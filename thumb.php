<?php
/**
 * Thumbnail Generator
 *
 * @author  Eugene Belyaev
 * @version 1.0
 *
 * USAGE: Just call url  "http://your_site_path/thumbs/$width/$height/$type/$pathToFile"
 * TYPES:
        0 = FIXED
        1 = PROPORTIONAL
        2 = CUT CENTER
        3 = CUT TOP CENTER
        4 = CUT TOP LEFT
        5 = 4:3
        6 = 16:9
 */

$thumbSubDir = "thumb/";

$sizeW = $_GET['sizeW'];
$sizeH = $_GET['sizeH'];
$type  = $_GET['type']; // See `Exoof_Tools_Image` defines
$file  = stripslashes(urldecode($_GET['file']));
$dir   = dirname($file).'/';

if (!file_exists($file) || filetype($file) != 'file') {
    $file = "./images/system/no_image.jpg";
    $type = 1;
    $dir  = "./files/";
}

$fname = basename($file);
$th_file_name   = 'thumb_'.$sizeW.'x'.$sizeH.'_t'.$type.'_'.$fname;
$th_file_path   = $dir.$thumbSubDir.$th_file_name;

if (!file_exists($dir.$thumbSubDir))
    @mkdir($dir.$thumbSubDir);

if (!file_exists($th_file_path)) {

    @include_once('./library/Exoof/Tools/Image.php');
    @include_once('../library/Exoof/Tools/Image.php');

    $th = Exoof_Tools_Image::makeThumbnail($file, $dir.$thumbSubDir, $sizeW, $sizeH, $type);
}

$info = getimagesize($th_file_path);
if ($info[2]) {
    $mime = image_type_to_mime_type($info[2]);
    header('Content-Description: File Transfer');
    header('Content-Type: '.$mime);
    //header('Content-Disposition: attachment; filename='.basename($th_file_path));
    header('Content-Transfer-Encoding: binary');
    header('Content-Length: ' . filesize($th_file_path));
    //ob_clean();
    //flush();
    readfile($th_file_path);
    exit;
}