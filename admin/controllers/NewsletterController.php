<?php
/**
 * @author Eugene Belyaev
 * @version 1.0 Last Updated 11 October 2010 / elearning
 */
class NewsletterController extends Exoof_Admin_Controller {

    public function sendAction() {
        if ($this->request->isPost()) {
            $subject = $this->request->getParam('subject');
            $body    = $this->request->getParam('body');

            $list    = Exoof_Db_Table::getDb()->fetchAll(Exoof_Db_Table::prefixize('mail_subscribe'));
            for($s=0,$ms=sizeof($list);$s < $ms; $s++) {
                Exoof_Tools_Mail::sendMail($list[$s], $subject, $body);
            }
            $this->view->message = 'Mails have been sent';
        }
    }

}
?>
