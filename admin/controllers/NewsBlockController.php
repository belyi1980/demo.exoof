<?php

/**
 * NewsBlock Controller
 *
 * @author Eugene Belyaev
 * @version 1.2 / Last Updated: 9 December 2010 / Project: DEMO / Author: Eugene Belyaev
 */
class NewsBlockController extends Exoof_Admin_Controller {

    public function indexAction() {
        News::purgeOrphans();
        Exoof_Session::set('structureIface', false);
        $this->view->list = $this->buildList(
                        $this->_('News Blocks'), array(
                    new Exoof_List_Column('nb_id', 'ID'),
                    new Exoof_List_Column('nb_name', $this->_('Title')),
                    new Exoof_List_Column('lang', 'Lang'),
                    new Exoof_List_ActionDialog('nb_id', array('NewsBlock', 'edit'), $this->_('Edit'), 'wrench'),
                    new Exoof_List_ConfirmDialog('nb_id', array('NewsBlock', 'delete'), $this->_('Delete'), 'minus'),
                        ), array(
                    'NewsBlock', 'fetchAll'
                        )
        );
    }

    public function newAction() {
        if ($this->request->isParamExists('page_title'))
            $this->request->addParam('nb_name', $this->request->getParam('page_title'));
        if ($this->request->isParamExists('page_id'))
            $this->view->page_id = $this->request->getParam('page_id');
        else
            throw new Exception('Page_Id is undefined ! Try to find the bug...');

        if ($this->request->isPost() && $this->request->isParamExists('nextstep')) {
            $obj = new NewsBlock();
            $obj->fillFromArray($this->request->getAllParams());
            $saved = $obj->save();
            if ($saved) {
                $this->view->ok = $this->_('Object has been created');
            } else {
                $this->view->error = $this->_('Object has not been created');
            }

            if (Exoof_Session::get('structureIface'))
                $this->forward('structure');
        }
    }

    public function editAction() {
        $obj = new NewsBlock();
        if ($page_id = $this->request->getParam('page_id'))
            $obj->findBy('page_id', $page_id);
        else
            $obj->load($this->request->getParam('nb_id'));

        if ($this->request->isPost()) {
            $obj->fillFromArray($this->request->getAllParams());
            $saved = $obj->save();
            if ($saved) {
                $this->view->ok = $this->_('Object has been saved');
            } else {
                $this->view->error = $this->_('Object has not been saved');
            }
            if (Exoof_Session::get('structureIface'))
                $this->forward('structure');
        }
        $this->view->nb = $obj;
    }

    public function deleteAction() {
        $obj_id = $this->request->getParam('nb_id');
        $obj = new NewsBlock($obj_id);
        $deleted = $obj->delete();
        if ($deleted) {
            $this->view->ok = $this->_('Object has been deleted');
        } else {
            $this->view->error = $this->_('Object has not been deleted');
        }
        if (Exoof_Session::get('structureIface'))
            $this->forward('structure');
        else
            $this->forward($this);
    }

    public function updateRecordName($page_id, $page_title) {
        $obj = new NewsBlock();
        $obj->findBy('page_id', $page_id);
        if ($obj->isLoaded()) {
            $obj->nb_name = $page_title;
            $obj->save(false);
            return true;
        }
        return false;
    }

}
