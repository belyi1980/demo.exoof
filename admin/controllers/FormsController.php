<?php
/**
 * Feedback Forms Content Module
 * @author Eugene Belyaev
 * @version 1.2 / Last Updated: 22 November 2010 / Project: DEMO / Author: Eugene Belyaev
 */
class FormsController extends Exoof_Admin_Controller {

    public function init() {
        $ftypes = array(
            'String',
            'E-mail',
            'Checkboxes',
//            'Radioboxes',
            'Select',
            'Text'
        );
        $this->view->ftypes = array_combine($ftypes, $ftypes);
    }

    public function indexAction() {
        Form::purgeOrphans();
        Exoof_Session::set('structureIface', false);        
        $this->view->list = $this->buildList(
                        $this->_('Forms'),
                        array(
                            new Exoof_List_Column('form_id', 'ID'),
                            new Exoof_List_Column('form_title', $this->_('Title')),
                            new Exoof_List_Column('lang', 'Lang'),
                            new Exoof_List_ActionDialog('form_id', array('Forms', 'edit'), $this->_('Edit'), 'pencil'),
                            new Exoof_List_ConfirmDialog('form_id',array('Forms','delete'),$this->_('Delete'),'minus'),
                        ),
                        array(
                            'Form', 'fetchAll'
                        ));
    }

    public function newAction() {
        if ($this->request->isParamExists('page_title'))
            $this->request->addParam('form_title', $this->request->getParam('page_title'));
        if ($this->request->isParamExists('page_id'))
            $this->view->page_id = $this->request->getParam('page_id');
        else
            throw new Exception('Page_Id is undefined ! Try to find the bug...');

        if ($this->request->isPost() && $this->request->isParamExists('nextstep')) {
            $obj = new Form();
            $obj->fillFromArray($this->request->getAllParams());

            $obj->form_enable_captcha = ($this->request->isParamExists('form_enable_captcha'))?1:0;

            $obj->form_fields_data = serialize(array(
                    'labels'  => $this->request->getParam('form_labels'),
                    'types'   => $this->request->getParam('form_types'),
                    'values'  => $this->request->getParam('form_values'),
                    'required'=> $this->request->getParam('form_req'))
                    );
            $saved = $obj->save();
            if ($saved)
                $this->view->ok = $this->_('Object has been created');
            else
                $this->view->error = $this->_('Object has not been created');

            if (Exoof_Session::get('structureIface'))
                $this->forward('structure');            
        }
    }

    public function editAction() {
        $obj = new Form();
        if ($page_id = $this->request->getParam('page_id'))
            $obj->findBy('page_id', $page_id);
        else
            $obj->load($this->request->getParam('form_id'));

        if ($this->request->isPost()) {
            $obj->fillFromArray($this->request->getAllParams());

            $obj->form_enable_captcha = ($this->request->isParamExists('form_enable_captcha'))?1:0;

            $delete   = $this->request->getParam('form_delete');
            $labels   = $this->request->getParam('form_labels');
            $types    = $this->request->getParam('form_types');
            $values   = $this->request->getParam('form_values');
            $required = $this->request->getParam('form_req');
            if (sizeof($delete))
                foreach ($delete as $id => $flag) {
                    unset($labels[$id]);
                    unset($types[$id]);
                    unset($values[$id]);
                    unset($required[$id]);
                }
            $obj->form_fields_data = serialize(array(
                    'labels'  => $labels,
                    'types'   => $types,
                    'values'  => $values,
                    'required'=> $required
                    ));

            $saved = $obj->save();
            if ($saved)
                $this->view->ok = $this->_('Object has been saved');
            else
                $this->view->error = $this->_('Object has not been saved');

            if (Exoof_Session::get('structureIface'))
                $this->forward('structure');            
        }
        $this->view->obj = $obj;
    }

    public function deleteAction() {
        $obj_id = $this->request->getParam('form_id');
        $obj    = new Form($obj_id);
        $deleted =$obj->delete();
        if ($deleted) {
            $this->view->ok = $this->_('Object has been deleted');
        } else {
            $this->view->error =  $this->_('Object has not been deleted');
        }
        if (Exoof_Session::get('structureIface'))
            $this->forward('structure');        
    }

    public function updateRecordName($page_id, $page_title) {
        $obj = new Form();
        $obj->findBy('page_id', $page_id);
        if ($obj->isLoaded()) {
            $obj->form_title = $page_title;
            $obj->save(false);
            return true;
        }
        return false;
    }
}