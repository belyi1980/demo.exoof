<?php
/**
 * Gallery Controller
 *
 * @author eugene
 * @version 1.0 / Last Updated: 9 December 2010 / Project: DEMO / Author: Eugene Belyaev
 */
class GalleryController extends Exoof_Admin_Controller {

    // GALLERY OBJECT METHODS
    public function indexAction() {
        Gallery::purgeOrphans();
        Exoof_Session::set('structureIface', false);
        $this->view->list = $this->buildList(
            $this->_('Gallery Pages'),
            array(
            new Exoof_List_Column('gal_id','ID'),
            new Exoof_List_Column('gal_name',$this->_('Title')),
            new Exoof_List_Column('lang','Lang'),
            new Exoof_List_ActionDialog('gal_id',array('gallery','edit'),$this->_('Edit'),'wrench'),
            new Exoof_List_ConfirmDialog('gal_id',array('gallery','delete'),$this->_('Delete'),'minus'),
            new Exoof_List_ActionDialog('gal_id',array('photo','index'),$this->_('Photo'),'image'),
            ),
            array(
            'Gallery','fetchAll'
            )
        );
    }

    public function newAction() {
        if ($this->request->isParamExists('page_title'))
            $this->request->addParam('gal_name', $this->request->getParam('page_title'));
        if ($this->request->isParamExists('page_id'))
            $this->view->page_id = $this->request->getParam('page_id');
        else
            throw new Exception('Page_Id is undefined ! Try to find the bug...');

        if ($this->request->isPost() && $this->request->isParamExists('nextstep')) {
            $gal = new Gallery();
            $gal->fillFromArray($this->request->getAllParams());
            $saved = $gal->save();
            if ($saved) {
                $this->view->ok = $this->_('Object has been created');
            } else {
                $this->view->error = $this->_('Object has not been created');
            }

            if (Exoof_Session::get('structureIface'))
                $this->forward('structure');
        }
    }

    public function editAction() {
        $obj = new Gallery();
        if ($this->request->getParam('page_id'))
            $obj->findBy('page_id', $this->request->getParam('page_id'));
        else
            $obj->load($this->request->getParam('gal_id'));

        if ($this->request->isPost()) {
            $obj->fillFromArray($this->request->getAllParams());
            $saved = $obj->save();
            if ($saved) {
                $this->view->ok = $this->_('Object has been saved');
            } else {
                $this->view->error =  $this->_('Object has not been saved');
            }
            
            if (Exoof_Session::get('structureIface'))
                $this->forward('structure');
        }
        $this->view->obj = $obj;
    }

    public function deleteAction() {
        $obj = new Gallery($this->request->getParam('gal_id'));
        $deleted = $obj->delete();
        if ($deleted) {
            if (Exoof_Session::get('gal_id') == $this->request->getParam('gal_id'))
                Exoof_Session::kill('gal_id');
            $this->view->ok = $this->_('Object has been deleted');
        } else {
            $this->view->error = $this->_('Object has not been deleted');
        }
        if (Exoof_Session::get('structureIface'))
            $this->forward('structure');
        else
            $this->forward($this);
    }

    public function updateRecordName($page_id, $page_title) {
        $obj = new Gallery();
        $obj->findBy('page_id', $page_id);
        if ($obj->isLoaded()) {
            $obj->gal_name = $page_title;
            $obj->save(false);
            return true;
        }
        return false;
    }


}