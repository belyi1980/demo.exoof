<?php

/**
 * Photo Controller
 *
 * @author eugene
 * @version 1.0 / Last Updated: 9 December 2010 / Project: DEMO / Author: Eugene Belyaev
 */
class PhotoController extends Exoof_Admin_Controller {

    // GALLERY OBJECT METHODS
    public function indexAction() {

        $this->view->galleries = Gallery::fetchGalleriesAssoc();

        $gal_id = null;
        if (intval($this->request->getParam('gal_id')) > 0) {
            $gal_id = $this->request->getParam('gal_id');
            Exoof_Session::set('gal_id', $gal_id);
        } elseif ($this->request->getParam('gal_id') === 'no') {
            Exoof_Session::kill('gal_id');
        } elseif (Exoof_Session::get('gal_id')) {
            $gal_id = Exoof_Session::get('gal_id');
        }

        if ($gal_id) {
            $this->view->gal_id = $gal_id;
            $this->view->list = $this->buildList(
                            $this->_('Photos'),
                            array(
                                new Exoof_List_Column('photo_id', 'ID'),
                                new Exoof_List_Thumb('photo_file', 'File'),
                                new Exoof_List_Column('photo_description', $this->_('Description')),
                                new Exoof_List_ActionDialog('photo_id', array('photo', 'edit'), $this->_('Edit'), 'wrench'),
                                new Exoof_List_ConfirmDialog('photo_id', array('photo', 'delete'), $this->_('Delete'), 'minus'),
                            ),
                            array(
                                'Photo', 'fetchAll'
                            )
            );
        }
    }

    public function newAction() {
        if ($this->request->isPost() && $this->request->isFileUploaded('photo_file')) {

            $file = $this->request->getFile('photo_file');
            if (Exoof_Tools_Image::getImageExtension($file['tmp_name']) != false) { //File is image and is uploaded
                $fileName = uniqid('photo_') . $file['name'];
                $moved = move_uploaded_file($file['tmp_name'], $this->config->paths->uploadPath . '/' . $fileName);
                if ($moved) {
                    @chmod($this->config->paths->uploadPath . '/' . $fileName, 0644);
                    $obj = new Photo();
                    $obj->gal_id = $this->request->getParam('gal_id');
                    $obj->photo_file = $fileName;
                    $obj->photo_description = $this->request->getParam('photo_description');
                    $saved = $obj->save();
                    if ($saved) {
                        $this->view->ok = $this->_('Object has been created');
                        $this->forward($this);
                    } else {
                        $this->view->error = $this->_('Object has not been created');
                    }
                } else
                    $this->view->error = $this->_('Image was not uploaded !');
            } else
                $this->view->error = $this->_('Uploaded file is not a valid image file !');
        }

        $this->view->galleries = Gallery::fetchGalleriesAssoc();
    }

    public function editAction() {
        $obj = new Photo();
        $obj->load($this->request->getParam('photo_id'));

        if ($this->request->isPost()) {

            if ($this->request->isFileUploaded('photo_file')) {
                $file = $this->request->getFile('photo_file');
                if (Exoof_Tools_Image::getImageExtension($file['tmp_name']) != false) { //File is image and is uploaded
                    $fileName = uniqid('photo_') . $file['name'];
                    $moved = move_uploaded_file($file['tmp_name'], $this->config->paths->uploadPath . '/' . $fileName);
                    if ($moved) {
                        @unlink($this->config->paths->uploadPath . '/' . $obj->photo_file);
                        @chmod($this->config->paths->uploadPath . '/' . $fileName, 0644);
                        $obj->photo_file = $fileName;
                    }
                } else
                    $this->view->error = $this->_('Uploaded file is not a valid image file !');
            }

            $obj->fillFromArray($this->request->getAllParams());
            $saved = $obj->save();
            if ($saved)
                $this->view->ok = $this->_('Object has been saved');
            else
                $this->view->error = $this->_('Object has not been saved');
        }

        $this->view->obj = $obj;
        $this->view->galleries = Gallery::fetchGalleriesAssoc();
    }

    public function deleteAction() {
        $obj = new Photo($this->request->getParam('photo_id'));
        $file= $obj->photo_file;
        $deleted = $obj->delete();
        if ($deleted) {
            @unlink($this->config->paths->uploadPath.'/'.$file);
            $this->view->ok = $this->_('Object has been deleted');
        } else {
            $this->view->error = $this->_('Object has not been deleted');
        }
        $this->forward($this);
    }

}