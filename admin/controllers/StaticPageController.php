<?php

/**
 * Static Page Controller
 *
 * @author Eugene Belyaev
 * @version 1.2 / Last Updated: 22 November 2010 / Project: DEMO / Author: Eugene Belyaev
 */
class StaticPageController extends Exoof_Admin_StructureController {

    protected $_listFilters = array('ptype_name'=>'Static Page');
    protected $_showToolbar = false;

    public function init() {
        $this->_listColumns = array(
            new Exoof_List_Column('page_id', 'ID'),
            new Exoof_List_Column('page_title', $this->_('Title'), array('Exoof_List_Filters', 'slashPaddings'), array('page_path', 'page_url', 'page_is_linked')),
            new Exoof_List_Column('page_url', 'URL', array($this, 'fullPageUrl')),
            new Exoof_List_Column('ptype_name', 'Type'),
            new Exoof_List_Column('page_date_added', $this->_('Date'), array('Exoof_List_Filters', 'normalizeDate')),
            new Exoof_List_Column('lang', 'Lang'),
            new Exoof_List_ActionDialog('page_id', array(__CLASS__, 'linkedit'), $this->_('Edit'), 'pencil'),
        );
    }

    public function newAction() {
        if ($this->request->isParamExists('page_title'))
            $this->request->addParam('sp_name', $this->request->getParam('page_title'));
        if ($this->request->isParamExists('page_id'))
            $this->view->page_id = $this->request->getParam('page_id');
        else
            throw new Exception('Page_Id is undefined ! Try to find the bug...');

        if ($this->request->isPost() && $this->request->isParamExists('nextstep')) {
            $sp = new StaticPage();
            $sp->fillFromArray($this->request->getAllParams());
            $saved = $sp->save();
            if ($saved) {
                $this->view->ok = $this->_('Object has been created');
            } else {
                $this->view->error = $this->_('Object has not been created');
            }

            if (Exoof_Session::get('structureIface'))
                $this->forward('structure');
        }
    }

    public function linkeditAction() {
        $sp = new StaticPage();
        if ($page_id = $this->request->getParam('page_id'))
            $sp->findBy('page_id', $page_id);
        else
            $sp->load($this->request->getParam('sp_id'));

        if ($this->request->isPost()) {
            $sp->fillFromArray($this->request->getAllParams());
            $saved = $sp->save();
            if ($saved) {
                $this->view->ok = $this->_('Object has been saved');
            } else {
                $this->view->error = $this->_('Object has not been saved');
            }

            if (Exoof_Session::get('structureIface'))
                $this->forward('structure');
        }
        $this->view->sp = $sp;
        $this->view->setRenderScript("staticpage/edit.phtml");
    }

    public function deleteAction() {
        $sp_id = $this->request->getParam('sp_id');
        $sp = new StaticPage($sp_id);
        $deleted = $sp->delete();
        if ($deleted) {
            $this->view->ok = $this->_('Object has been deleted');
        } else {
            $this->view->error = $this->_('Object has not been deleted');
        }
        if (Exoof_Session::get('structureIface'))
            $this->forward('structure');
        else
            $this->forward($this);
    }

    public function updateRecordName($page_id, $page_title) {
        $sp = new StaticPage();
        $sp->findBy('page_id', $page_id);
        if ($sp->isLoaded()) {
            $sp->sp_name = $page_title;
            $sp->save(false);
            return true;
        }
        return false;
    }

}

?>
