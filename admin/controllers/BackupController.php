<?php
/**
 * Backup Controller
 *
 * @author Eugene Belyaev
 * @version 1.0 / Last Updated: 13 December 2010 / Project: DEMO / Author: Eugene Belyaev /
 */
class BackupController extends Exoof_Admin_Controller {
	
    public function indexAction() {

    }

    public function dbAction() {
        exec("which mysqldump", $output);
        $binPath  = array_shift($output);
        $basename = "db-backup-".$this->config->database->dbname."-".strftime("%F-%H%M%S").".sql.bz2";
        $file     = $this->config->paths->tmpPath."/".$basename;
        $fileR    = realpath($this->config->paths->tmpPath)."/".$basename;
        $command  = sprintf("%s --add-drop-table --host=%s --user=%s --password=%s %s | bzip2 -c > %s",$binPath, $this->config->database->host, $this->config->database->username, $this->config->database->password, $this->config->database->dbname, $fileR);
        @exec($command,$output);
        $hash = Exoof_Tools_Crypt::Encrypt($file);
        echo "<a href='../getfile.php?".$hash."' target='_blank'>Download `".$basename."`</a>";
        exit;
    }

}