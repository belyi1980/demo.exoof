<?php
/**
 * IndexController
 *
 * @author eugene
 * @version 1.4 / Last Updated: 30 November 2010 / Project: DEMO / Author: Eugene Belyaev
 */
class IndexController extends Exoof_Controller {

    public function indexAction() {
        
    }

    public function menuAction() {
        $admin       = Exoof_Session::get('admin');
        $permissions = @array_keys(unserialize($admin['adm_sections']));
        $this->view->admin = $admin;
        $this->view->permissions = $permissions;
        $this->view->sections = simplexml_load_file('./config/admin.xml');
    }

    public function logoutAction() {
        $admin       = Exoof_Session::get('admin');
        Exoof_Tools_Logger::log("admin", " ******** Admin `%s` logged out ",$admin['adm_login']);
        $_SERVER['PHP_AUTH_USER'] = null;
        Exoof_Session::set('adminAuthorized',false);
        Exoof_Session::set('admin', null);
        $_SESSION['adminAuthorized'] = false;//flag for TinyMCE File Browser
        // Bugfixed: Incorrect logout admin in IE
        header('Location: ' . $this->view->baseHref().$this->config->admin->adminURL);
        exit;
    }

    public function loginAction() {
        
    }
}
?>
