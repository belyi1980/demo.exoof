<?php
/**
 * Structure Controller
 *
 * @author Eugene Belyaev
 * @version 1.4 / Last Updated: 2 November 2010 / Project: DEMO / Author: Eugene Belyaev
 */
class StructureController extends Exoof_Admin_Controller {

    public function indexAction() {

        $this->view->list = $this->buildList(
            $this->_('Pages'),
            array(
            new Exoof_List_Column('page_id','ID'),
            new Exoof_List_Column('page_title',$this->_('Title'),array('Exoof_List_Filters','slashPaddings'),array('page_path','page_url','page_is_linked')),
            new Exoof_List_Column('page_url','URL',array($this,'fullPageUrl')),
            new Exoof_List_Column('ptype_name','Type'),
            new Exoof_List_Column('page_date_added',$this->_('Date'),array('Exoof_List_Filters','normalizeDate')),
            new Exoof_List_Column('lang','Lang'),
            new Exoof_List_ActionDialog('page_id',array('structure','sortUp'),$this->_('Up'),'arrowthick-1-n'),
            new Exoof_List_ActionDialog('page_id',array('structure','sortDown'),$this->_('Down'),'arrowthick-1-s'),
            new Exoof_List_ActionDialog('page_id',array('structure','edit'),$this->_('Settings'),'wrench'),
            new Exoof_List_ActionDialog('page_id',array('structure','linkedit'),$this->_('Edit'),'pencil'),
            new Exoof_List_ConfirmDialog('page_id',array('structure','delete'),$this->_('Delete'),'minus'),
            ),
            array(
            'Pages','fetchAll'
            )
        );
    }

    public function newAction() {
        //select box update
        if ($this->request->isParamExists('type')) {
            $pt = new PageType($this->request->getParam('type'));
            if ($pt->ptype_depends_on)
                $this->view->parents = $this->getParentsSelectOptions(array('ptype_name'=>$pt->ptype_depends_on));
            else
                $this->view->parents = $this->getParentsSelectOptions();
            $this->setRenderScript('structure/_parents.phtml');
            return;
        }

        $page = new Pages();

        $count = Pages::countLangPages(Exoof_Lang::getLang());

        $ptypes   = PageType::fetchTypes();
        if ($count > 0) {
            $ptkeys = array_keys($ptypes);
            array_shift($ptkeys);
            array_shift($ptypes);
            $ptypes = array_combine($ptkeys, $ptypes);
        }
        else
            $ptypes = array(array_shift(array_keys($ptypes)) => array_shift($ptypes));
        $this->view->types   = $ptypes;
        $this->view->parents = $this->getParentsSelectOptions();

        if ($this->request->isPost()) {
            $page->fillFromArray($this->request->getAllParams());
            $saved = $page->save();
            if ($saved) {
                try {
                    $ptype = new PageType($page->ptype_id);
                    $this->request->addParam('page_id', $page->page_id);
                    $this->request->addParam('page_title', $page->page_title);
                    //debug($page->toArray());
                    //debug($this->request->getAllParams());
                    Exoof_Session::set('structureIface', true);
                    $this->forward($ptype->ptype_controller, 'new');
                    //echo $ctrl->render();
                    //exit;
                } catch (Exception $e) {
                    throw new Exception($e->getMessage());
                }

            //$this->view->ok = $this->_("Object has been created");
            }
            else
                debug(Exoof_Factory::factory('Db')->getError());
        }
        $this->view->page = $page;
        $this->
    }

    public function editAction() {
        $page = new Pages($this->request->getParam('page_id'));

        $ptypes   = PageType::fetchTypes();
        $this->view->types   = $ptypes;
        $this->view->parents = $this->getParentsSelectOptions();

        if ($this->request->isPost()) {
            $page->fillFromArray($this->request->getAllParams());
            $saved = $page->save();
            if ($saved) {
                $ptype   = new PageType($page->ptype_id);
                $this->call($ptype->ptype_controller, 'updateRecordName',array($page->page_id, $page->page_title));
                $this->view->ok = $this->_('Object has been saved');
                $this->forward($this);
            } else {
                $this->view->error =  $this->_('Object has not been saved');
            }
        }
        $this->view->page = $page;
    }

    public function linkeditAction() {
        $page    = new Pages($this->request->getParam('page_id'));
        $ptype   = new PageType($page->ptype_id);
        Exoof_Session::get('structureIface', true);
        $this->forward($ptype->ptype_controller, 'edit');
    }

    public function sortupAction() {

        $page_id = $this->getParam('page_id');
        $page   = new Pages($page_id);
        $page->moveSortUp();
        
        Exoof_Cache::getInstance()->engine->delete('ExoofStructure');
        $this->forward($this,'index');
    }

    public function sortdownAction() {
        $page    = new Pages($this->request->getParam('page_id'));
        $page->moveSortDown();

        Exoof_Cache::getInstance()->engine->delete('ExoofStructure');
        $this->forward($this,'index');
    }

    public function deleteAction() {
        $page    = new Pages($this->request->getParam('page_id'));
        $deleted = $page->delete();
        if ($deleted) {
            $this->view->ok = $this->_('Object has been deleted');
        } else {
            $this->view->error =  $this->_('Object has not been deleted');
        }
        
        Exoof_Cache::getInstance()->engine->delete('ExoofStructure');
        $this->forward($this);
    }

    public function fullPageUrl($field, $title, $row) {
        if (!empty($row['page_path']))
            $url = $row['page_path'].$row[$field];//join('/',array($row['page_path'],$row[$field]));
        else
            $url = $row[$field];

        $url = Exoof_Lang::getLangUrl().$url;

        $trunc = 50;
        $url = "<a href='../{$url}' target='_blank'>".(strlen($url)>$trunc?substr($url,0,$trunc).'...':$url)."</a>";
        return $url;
    }

    private function getParentsSelectOptions($where=null) {
        $where['ptype_id'] = Exoof_Db::directField('<>',5);// PATCH ONLY FOR KINGS
        $pages_arr = Pages::fetchAll($where);
        $pages_arr_new = array();
        $mp   = sizeof($pages_arr);
        for($p=0;$p<$mp;$p++) {
            $key   = $pages_arr[$p]['page_id'];
            $title = $pages_arr[$p]['page_title'];
            $title = str_repeat('—',$pages_arr[$p]['page_level']).' '.$title;

            $pages_arr_new[$key] = $title;
        }

        return $pages_arr_new;
    }
}
?>
