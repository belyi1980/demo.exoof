<?php

/**
 * Static Page Controller
 *
 * @author Eugene Belyaev
 * @version 1.2 / Last Updated: 22 November 2010 / Project: DEMO / Author: Eugene Belyaev
 */
class StaticPageController extends Exoof_Admin_Controller {

    public function indexAction() {
        StaticPage::purgeOrphans();
        Exoof_Session::set('structureIface', false);
        $this->view->list = $this->buildList(
                        $this->_('Static Pages'), array(
                    new Exoof_List_Column('sp_id', 'ID'),
                    new Exoof_List_Column('sp_name', $this->_('Title')),
                    new Exoof_List_Column('lang', 'Lang'),
                    new Exoof_List_ActionDialog('sp_id', array('staticPage', 'edit'), $this->_('Edit'), 'wrench'),
                    new Exoof_List_ConfirmDialog('sp_id', array('staticPage', 'delete'), $this->_('Delete'), 'minus'),
                        ), array(
                    'StaticPage', 'fetchAll'
                        )
        );
    }

    public function newAction() {
        if ($this->request->isParamExists('page_title'))
            $this->request->addParam('sp_name', $this->request->getParam('page_title'));
        if ($this->request->isParamExists('page_id'))
            $this->view->page_id = $this->request->getParam('page_id');
        else
            throw new Exception('Page_Id is undefined ! Try to find the bug...');

        if ($this->request->isPost() && $this->request->isParamExists('nextstep')) {
            $sp = new StaticPage();
            $sp->fillFromArray($this->request->getAllParams());
            $saved = $sp->save();
            if ($saved) {
                $this->view->ok = $this->_('Object has been created');
            } else {
                $this->view->error = $this->_('Object has not been created');
            }

            if (Exoof_Session::get('structureIface'))
                $this->forward('structure');
        }
    }

    public function editAction() {
        $sp = new StaticPage();
        if ($page_id = $this->request->getParam('page_id'))
            $sp->findBy('page_id', $page_id);
        else
            $sp->load($this->request->getParam('sp_id'));

        if ($this->request->isPost()) {
            $sp->fillFromArray($this->request->getAllParams());
            $saved = $sp->save();
            if ($saved) {
                $this->view->ok = $this->_('Object has been saved');
            } else {
                $this->view->error = $this->_('Object has not been saved');
            }

            if (Exoof_Session::get('structureIface'))
                $this->forward('structure');
        }
        $this->view->sp = $sp;
    }

    public function deleteAction() {
        $sp_id = $this->request->getParam('sp_id');
        $sp = new StaticPage($sp_id);
        $deleted = $sp->delete();
        if ($deleted) {
            $this->view->ok = $this->_('Object has been deleted');
        } else {
            $this->view->error = $this->_('Object has not been deleted');
        }
        if (Exoof_Session::get('structureIface'))
            $this->forward('structure');
        else
            $this->forward($this);
    }

    public function updateRecordName($page_id, $page_title) {
        $sp = new StaticPage();
        $sp->findBy('page_id', $page_id);
        if ($sp->isLoaded()) {
            $sp->sp_name = $page_title;
            $sp->save(false);
            return true;
        }
        return false;
    }

}

?>
