<?php
/**
 * Options Controller
 *
 * @author eugene
 * @version 1.1 Last Updated: 12 OCtober 2010 / elearning
 */
class OptionsController extends Exoof_Admin_Controller {
	
	public $types = array(
	   'string' => 'String',
	   'text'   => 'Text',
	   'number' => 'Number',
	   'html'   => 'HTML',
//	   'file'   => 'File',
//	 )  'image'  => 'Image',
	);
	

    public function indexAction() {

        $this->view->list = $this->buildList(
            $this->_('Options'),
            array(
            new Exoof_List_Column('opt_id','ID'),
            new Exoof_List_Column('opt_variable',$this->_('Title')),
            new Exoof_List_Column('opt_type',$this->_('Type')),
            new Exoof_List_Column('opt_value',$this->_('Value'),array('Exoof_List_Filters','normalizeHtml')),
            new Exoof_List_Column('lang','Lang'),
            new Exoof_List_ActionDialog('opt_id',array('options','edit'),$this->_('Settings'),'wrench'),
            new Exoof_List_ActionDialog('opt_id',array('options','value'),$this->_('Edit'),'pencil'),
            new Exoof_List_ConfirmDialog('opt_id',array('options','delete'),$this->_('Delete'),'minus'),
            ),
            array(
            'Option','fetchAll'
            )
        );
    }

    public function newAction() {
        $obj = new Option();
        
        if ($this->request->isPost()) {
            $obj->fillFromArray($this->request->getAllParams());
            $saved = $obj->save();
            if ($saved) {
                //$this->view->ok = $this->_('Object has been created');
                $this->request->addParam('opt_id', $obj->opt_id);
                $this->forward($this, 'value');
            } else {
                $this->view->error = $this->_('Object has not been created');
            }
        }

        $this->view->types = $this->types;
    }

    public function editAction() {
        $obj = new Option($this->request->getParam('opt_id'));

        if ($this->request->isPost()) {
            $obj->fillFromArray($this->request->getAllParams());
            $saved = $obj->save();
            if ($saved) {
                $this->view->ok = $this->_('Object has been saved');
            } else {
                $this->view->error =  $this->_('Object has not been saved');
            }
        }
        $this->view->types = $this->types;
        $this->view->option = $obj;
    }

    public function valueAction() {
        $obj = new Option($this->request->getParam('opt_id'));

        if ($this->request->isPost() && $this->request->getParam('nextstep')) {
            $obj->fillFromArray($this->request->getAllParams());
            $saved = $obj->save();
            if ($saved) {
                $this->view->ok = $this->_('Object has been saved');
            } else {
                $this->view->error =  $this->_('Object has not been saved');
            }
        }
        $this->view->option = $obj;
    }

    public function deleteAction() {
        $obj = new Option($this->request->getParam('opt_id'));
        $deleted = false;
        if ($obj->isLoaded() && $obj->opt_deletable)
                $deleted = $obj->delete();

        if ($deleted)
            $this->view->ok = $this->_('Object has been deleted');
        else
            $this->view->error = $this->_('Object has not been deleted');

        $this->forward($this);
    }

}