<?php

/**
 * Administrators Controller
 *
 * @author eugene
 * @version 1.4 / Last Updated: 14 December 2010 / Project: DEMO / Author: Eugene Belyaev
 */
class AdministratorsController extends Exoof_Admin_Controller {

    public function indexAction() {

        $this->view->list = $this->buildList(
                        $this->_('Administrators'),
                        array(
                            new Exoof_List_Column('adm_id', 'ID'),
                            new Exoof_List_Column('adm_login', $this->_('Login')),
                            new Exoof_List_Column('adm_is_superuser', $this->_('SuperUser'), array('Exoof_List_Filters', 'YesNo')),
                            new Exoof_List_ActionDialog('adm_id', array('administrators', 'edit'), $this->_('Edit'), 'pencil'),
                            new Exoof_List_ConfirmDialog('adm_id', array('administrators', 'delete'), $this->_('Delete'), 'minus'),
                        ),
                        array(
                            'Administrator', 'fetchAll'
                        )
        );
    }

    public function newAction() {
        $admin = Exoof_Session::get('admin');
//        if (!isset($admin['adm_id']) || (isset($admin['adm_id']) && !$admin['adm_is_superuser'])) {
//            $this->view->error = $this->_('Access denied ! Only superuser can manage administrators !');
//            $this->forward($this);
//            return;
//        }

        $obj = new Administrator();
        $this->view->sections = simplexml_load_file('./config/admin.xml');
        $this->view->permissions = @array_keys(unserialize($admin['adm_sections']));
//        debug($this->view->permissions);
        $this->view->isRoot   = $admin['adm_is_superuser']?true:false;

        if ($this->request->isPost()) {

            if ($this->request->getParam('adm_passw') == $this->request->getParam('confirm_passw')) {
                $obj->fillFromArray($this->request->getAllParams());
                $perms = $this->request->getParam('s');
                if (!sizeof($perms))
                    $this->view->error = $this->_('You should set permissions');
                else {
                    $obj->adm_sections = serialize($perms);
                    $saved = $obj->save();
                    if ($saved) {
                        if ($this->request->isParamExists('send')) {
                            //send mail
                            $keys = array_merge($obj->toArray(),array('adm_password_clear'=>$this->request->getParam('adm_passw'),'admin_link'=>$this->view->baseHref().$this->config->admin->adminURL));
                            $mailText = Exoof_Translate::getInstance()->loadFile("mail_admin.txt");
                            Exoof_Tools_Mail::sendMail(array($obj->adm_email,$obj->adm_login), $this->l("New Administrator"), $mailText, $keys, array($admin['adm_login'],$admin['adm_email']));
                        }
                        $this->view->ok = $this->_('Object has been created');
                        $this->forward($this);
                    } else {
                        $this->view->error = $this->_('Object has not been created');
                    }
                }
            } else {
                $this->view->error = $this->_('Passwords are not equal');
            }
        }
    }

    public function editAction() {
        $admin = Exoof_Session::get('admin');
        if (!isset($admin['adm_id']) || (isset($admin['adm_id']) && !$admin['adm_is_superuser'])) {
            $this->view->error = $this->_('Access denied ! Only superuser can manage administrators !');            
            $this->forward($this);
            return;
        }

        $obj = new Administrator($this->request->getParam('adm_id'));
        $this->view->sections = simplexml_load_file('./config/admin.xml');

        if ($this->request->isPost()) {
            //if ($this->request->getParam('adm_passw')==$this->request->getParam('confirm_passw')) {
            $obj->adm_login = $this->request->getParam('adm_login');
            $obj->adm_email = $this->request->getParam('adm_email');
            if (strlen($this->request->getParam('adm_passw')) && ($this->request->getParam('adm_passw') == $this->request->getParam('confirm_passw'))) {
                $obj->adm_passw = $this->request->getParam('adm_passw');
            }

            $perms = $this->request->getParam('s');
            if (!sizeof($perms))
                $this->view->error = $this->_('You should set permissions');
            else {
                $obj->adm_sections = serialize($perms);
                $saved = $obj->save();
                if ($saved) {
                    $this->view->ok = $this->_('Object has been saved');
                } else {
                    $this->view->error = $this->_('Object has not been saved');
                }
            }
            //} else {
            //    $this->view->error = $this->_('Passwords are not equal');
            //}
        }
        $this->view->admin = $obj;
        $this->view->permissions = @array_keys(unserialize($obj->adm_sections));
    }

    public function deleteAction() {
        $admin = Exoof_Session::get('admin');
        if (!isset($admin['adm_id']) || (isset($admin['adm_id']) && !$admin['adm_is_superuser'])) {
            $this->view->error = $this->_('Access denied ! Only superuser can manage administrators !');            
            $this->forward($this);
            return;
        }

        $obj = new Administrator($this->request->getParam('adm_id'));
        if ($obj->adm_is_superuser) {
            $this->view->error = $this->_('SuperUser can not be deleted');
        } else {
            $deleted = $obj->delete();
            if ($deleted) {
                $this->view->ok = $this->_('Object has been deleted');
            } else {
                $this->view->error = $this->_('Object has not been deleted');
            }
            $this->forward($this);
        }
    }

}