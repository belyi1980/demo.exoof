<?php

/**
 * @author Eugene Belyaev
 * @version 1.2 / Last Updated: 22 November 2010 / Project: DEMO / Author: Eugene Belyaev
 */
class NewsController extends Exoof_Admin_Controller {

    public function indexAction() {
        NewsArticle::purgeOrphans();
        Exoof_Session::set('structureIface', false);
        $this->view->list = $this->buildList(
                        $this->_('News'), array(
                    new Exoof_List_Column('news_id', 'ID'),
                    new Exoof_List_Column('news_title', $this->_('Title')),
                    new Exoof_List_Column('news_date', $this->_('Date'), array('Exoof_List_Filters', 'normalizeDate')),
                    new Exoof_List_Thumb('news_thumbnail', $this->_('Thumbnail')),
                    new Exoof_List_Column('news_announce', $this->_('Announce'), array('Exoof_List_Filters', 'truncate')),
                    new Exoof_List_Column('lang', 'Lang'),
                    new Exoof_List_ActionDialog('news_id', array('News', 'edit'), $this->_('Edit'), 'pencil'),
                    new Exoof_List_ConfirmDialog('news_id', array('News', 'delete'), $this->_('Delete'), 'minus'),
                        ), array(
                    'NewsArticle', 'fetchAll'
                ));
    }

    public function newAction() {
        if ($this->request->isParamExists('page_title'))
            $this->request->addParam('news_title', $this->request->getParam('page_title'));
        if ($this->request->isParamExists('page_id'))
            $this->view->page_id = $this->request->getParam('page_id');
        else
            throw new Exception('Page_Id is undefined ! Try to find the bug...');

        if ($this->request->isPost() && $this->request->isParamExists('nextstep')) {
            $obj = new NewsArticle();
            $obj->fillFromArray($this->request->getAllParams());

            //icon upload
            if ($this->request->isFileUploaded('news_thumbnail')) {
                $fp = $this->request->getFile('news_thumbnail');
                $file = uniqid('news_thumb_') . $fp['name'];
                $moved = @move_uploaded_file($fp['tmp_name'], $this->config->paths->uploadPath . '/' . $file);
                if ($moved)
                    $obj->news_thumbnail = $file;
            }

            $obj->news_date = $this->request->getDateParam('news_date');
            $saved = $obj->save();
            if ($saved)
                $this->view->ok = $this->_('Object has been created');
            else
                $this->view->error = $this->_('Object has not been created');
            if (Exoof_Session::get('structureIface'))
                $this->forward('structure');
        }
    }

    public function editAction() {
        $obj = new NewsArticle();
        if ($this->request->isParamExists('page_id'))
            $obj->findBy('page_id', $this->request->getParam('page_id'));
        else
            $obj->load($this->request->getParam('news_id'));

        if ($this->request->isPost()) {
            $obj->fillFromArray($this->request->getAllParams());

            //icon upload
            if ($this->request->isFileUploaded('news_thumbnail')) {
                $fp = $this->request->getFile('news_thumbnail');
                $file = uniqid('news_thumb_') . $fp['name'];
                $moved = @move_uploaded_file($fp['tmp_name'], $this->config->paths->uploadPath . '/' . $file);
                if ($moved)
                    $obj->news_thumbnail = $file;
            }
            $obj->news_date = $this->request->getDateParam('news_date');

            $saved = $obj->save();
            if ($saved)
                $this->view->ok = $this->_('Object has been saved');
            else
                $this->view->error = $this->_('Object has not been saved');
            if (Exoof_Session::get('structureIface'))
                $this->forward('structure');
        }
        $this->view->obj = $obj;
    }

    public function deleteAction() {
        $obj_id = $this->request->getParam('news_id');
        $obj = new News($obj_id);
        $deleted = $obj->delete();
        if ($deleted) {
            $this->view->ok = $this->_('Object has been deleted');
        } else {
            $this->view->error = $this->_('Object has not been deleted');
        }
        if (Exoof_Session::get('structureIface'))
            $this->forward('structure');
        else
            $this->forward($this);
    }

    public function updateRecordName($page_id, $page_title) {
        $obj = new NewsArticle();
        $obj->findBy('page_id', $page_id);
        if ($obj->isLoaded()) {
            $obj->news_title = $page_title;
            $obj->save(false);
            return true;
        }
        return false;
    }

}