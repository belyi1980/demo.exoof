<pre><?php

$fL = 'en';
$tL = 'ru';
$onlyFile = ($_REQUEST['file'])?$_REQUEST['file']:'_auto_translate.ini';

$pathToIniFiles = "./langs/{$fL}/";

$dir = array_slice(scandir($pathToIniFiles), 2);
if (sizeof($dir))
    foreach ($dir as $file) {
        if (!is_dir($file) && substr($file,-4) == '.ini') 
            if ($onlyFile != null && $file == $onlyFile)
                {

            echo "Processing `{$file}` - ";
            flush();
            $newData = processIniFile($pathToIniFiles.$file, $fL, $tL);
            echo "OK<br/>";
            flush();
            if (sizeof($newData)) {
                echo "Creating `{$tL}` version of `{$file}` - ";
                flush();
                makeNewIniFile($file, dirname($pathToIniFiles).'/'.$tL.'/', $newData);
                echo "OK<br/>";
                flush();
            }
        }
    }

function makeNewIniFile($filename, $path, $data) {
    $f = fopen($path.$filename,"w");
    @chmod($path.$filename, 0666);
    $header = "; Translated via TRANSLATE.GOOGLE.COM\n\n";
    fputs($f, $header, strlen($header));
    foreach ($data as $key => $value) {
        $str = "{$key} = \"{$value}\"\n";
        fputs($f, $str, strlen($str));
    }
    fclose($f);
}

function processIniFile($filename, $fL, $tL) {
    $data   = parse_ini_file($filename,false);
    $ndata  = array();
    foreach ($data as $key => $text) {
        $ndata[$key] = getTranslation($fL, $tL, $text);
    }
    return $ndata;
}

function getTranslation($fL, $tL, $text) {

    //$url = sprintf("http://translate.google.com/translate_a/t?client=t&text=%s&&sl=%s&tl=%s&otf=1&pc=0", urlencode($text), $fL, $tL);
    $url = sprintf("http://ajax.googleapis.com/ajax/services/language/translate?v=1.0&q=%s&langpair=%s|%s", urlencode($text), $fL, $tL);
    $c = curl_init($url);

    curl_setopt($c, CURLOPT_RETURNTRANSFER, true);

    $return = curl_exec($c);
    $obj = json_decode($return);

    return $obj->responseData->translatedText;
}
?>
