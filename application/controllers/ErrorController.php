<?php
/**
 * Error Controller
 *
 * @author eugene
 */
class ErrorController extends Exoof_Controller {

    private $exception = null;

    public function init() {
        $this->exception = Exoof_Factory::factory('Exception');
    }

    public function indexAction() {
        $this->view->message = $this->exception->getMessage();
        
        switch (get_class($this->exception)) {
            case 'Exoof_Exception_NotFound':
                header("HTTP/1.0 404 Not Found");
                break;

            case 'Exoof_Exception_Security':
                header('HTTP/1.1 403 Forbidden');
                break;

            case 'Exoof_Exception_Access':
                $this->view->message = "";
                $this->forward('user','register');
                return;
                break;

        }
        $this->setRenderScript('error/error_common.phtml');
    }
}
?>
