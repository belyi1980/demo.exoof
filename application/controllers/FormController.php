<?php
/**
 * Feedback Form Controller
 *
 * @author Eugene Belyaev
 * @version 1.0 / Last Updated : 18 October 2010 / Project : DEMO / Author : Eugene Belyaev
 */
class FormController extends Exoof_Controller {

    public function indexAction() {

        $pageObject = $this->request->getParam('page');
        $this->view->seoTitle($pageObject->page_meta_title);
        $this->view->seoDescription($pageObject->page_meta_description);
        $this->view->seoKeywords($pageObject->page_meta_keywords);

        $obj = new Form();
        $obj->findBy('page_id', $pageObject->page_id);
        $this->view->form = $obj;

        if ($this->request->isPost()) {
            if ($this->request->isParamExists('captcha') && !$this->request->isCaptchaValid()) {
                $this->view->error = $this->l('Wrong security code');
            }
            else {
             $content  = $obj->form_title."\n";
             $content .= "<hr/>\n";

             $values  = $this->request->getParam('field');

             foreach ($obj->fields['labels'] as $idx => $label) {
                $value = $values[$idx];
                if (is_array($value)) {
                    $value = "\n - ".implode("\n - ",$value);
                }
                $content .= "\n<b>".$label."</b> : ".$value."<br/>";
             }

             Exoof_Tools_Mail::sendMail($obj->form_mailto, $obj->form_title, $content);

             $this->view->message = $this->l('Form has been processed');
            }
        }

    }

}
?>