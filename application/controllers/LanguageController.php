<?php
/**
 * Language Controller
 *
 * @author eugene
 */
class LanguageController extends Exoof_Controller {

    public function indexAction() {
        $url = Exoof_Lang::getLangUrl();
        if ($url != null)
            $this->redirect($url);
    }

}
?>
