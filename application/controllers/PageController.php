<?php
/**
 * Page Controller
 *
 * @author Eugene Belyaev
 * @version 1.1 / Last Updated : 15 October 2010 / Project : eLearning / Author : Eugene Belyaev
 */
class PageController extends Exoof_Controller {
    //put your code here

    public function indexAction() {

        $pageObject = $this->request->getParam('page');
        $this->view->seoTitle($pageObject->page_meta_title);
        $this->view->seoDescription($pageObject->page_meta_description);
        $this->view->seoKeywords($pageObject->page_meta_keywords);

        $sp = new StaticPage();
        $sp->findBy('page_id', $pageObject->page_id);
        $this->view->spage = $sp;


    }

}
?>