<?php

/**
 * Main Page Controller
 *
 * @author eugene
 */
class MainController extends Exoof_Controller {

    public function indexAction() {

    }

    public function contactusAction() {

        if ($this->request->isPost()) {
            if ($this->request->isCaptchaValid()) {
                $user = Exoof_Session::get('user');
                //Send mail
                if ($this->request->isParamExists('c_email'))
                        $from = $this->request->getParam('c_email');
                else
                        $from = array($user['u_login'],$user['u_fname']);
                $mailText = $this->request->getParam('c_message');
                $mailSubject = $this->request->getParam('c_subject', 'Contact Us Form');
                Exoof_Tools_Mail::sendMail($this->config->mail->contactEmail, $mailSubject, $mailText,null,$from);
//                Exoof_Tools_Mail::sendMail('eugene@belinux', $mailSubject, $mailText,null,$from);
                $this->view->message = 'Message has been sent';
            } else
                $this->view->error = 'Wrong Security Code';
        }
    }

    public function sitemapACtion() {
        
    }

}
