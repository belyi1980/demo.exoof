<?php
/**
 * News Controller
 *
 * @author Eugene
 * @version 1.1 / Last Updated : 15 October 2010 / Project : eLearning / Author : Eugene Belyaev
 */
class NewsController extends Exoof_Controller {
    //put your code here

    public function blockAction() {

        $pageObject = $this->request->getParam('page');
        $this->view->seoTitle($pageObject->page_meta_title);
        $this->view->seoDescription($pageObject->page_meta_description);
        $this->view->seoKeywords($pageObject->page_meta_keywords);

        $nb = new NewsBlock();
        $nb->findBy('page_id', $pageObject->page_id);
        $this->view->nb = $nb;

        $this->view->news = NewsArticle::fetchWithBlock(array('parent_page_id' => $pageObject->page_id));
    }

    public function articleAction() {
        $pageObject = $this->request->getParam('page');
        $this->view->seoTitle($pageObject->page_meta_title);
        $this->view->seoDescription($pageObject->page_meta_description);
        $this->view->seoKeywords($pageObject->page_meta_keywords);

        $obj = new NewsArticle();
        $obj->findBy('page_id', $pageObject->page_id);
        $this->view->news = $obj;
    }

    public function newsbarAction() {
        $this->view->newsbar = NewsArticle::fetchWithBlock(null, 5);
    }

    public function subscribeAction() {
        if ($this->request->isPost() && $this->request->isParamExists('s_email')) {

            $rec = array(
              's_name' => $this->request->getParam('s_name'),
              's_email' => $this->request->getParam('s_email')
            );
            try {
            $added = Exoof_Db_Table::getDb()->insert(Exoof_Db_Table::prefixize('mail_subscribe'), $rec);
            } catch (Exception $e) {
                $added = false;
            }
            if ($added) {
                $mb = new MailBlackList($this->request->getParam('s_email'));
                if ($mb->isLoaded())
                        $mb->delete();
                $this->view->message = $this->l('Your e-mail has been added');
            }
            else
                $this->view->message = $this->l('Your e-mail has not been added');
        }
        $this->setRenderScript('message.phtml');
    }

}
?>