<?php
/**
 * Gallery Controller
 *
 * @author eugene
 * @version 1.0 / Last Updated: 9 December 2010 / Project: DEMO / Author: Eugene Belyaev
 */
class GalleryController extends Exoof_Controller {
    //put your code here

    public function indexAction() {

        $pageObject = $this->request->getParam('page');
        $this->view->seoTitle($pageObject->page_meta_title);
        $this->view->seoDescription($pageObject->page_meta_description);
        $this->view->seoKeywords($pageObject->page_meta_keywords);

        $obj = new Gallery();
        $obj->findBy('page_id', $pageObject->page_id);
        $this->view->gallery = $obj;
        $this->view->photos  = $obj->getPhotos();
    }

}
?>
