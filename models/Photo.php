<?php
/**
 * Photo model
 *
 * @author eugene
 * @version 1.0 / Last Updated: 9 December 2010 / Project: DEMO / Author: Eugene Belyaev
 */
class Photo extends Exoof_Db_Table {

    const TABLE_NAME      = 'gallery_photo';

    protected $_tableName = 'gallery_photo';

    static public function fetchAll($where=null, $count=null, $offset=null, $orderBy='photo_sort', $orderDest=null) {
        $table1  = self::prefixize(self::TABLE_NAME);
        $sql    = "SELECT * FROM ".$table1;

        return self::getDb()->fetch($sql, $where, $count, $offset, $orderBy, $orderDest);
    }

    static public function fetchByGallery($gal_id) {
        return self::fetchAll(array('gal_id'=>$gal_id));
    }

}
?>
