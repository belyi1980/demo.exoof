<?php
/**
 * Gallery Model
 *
 * @author eugene
 * @version 1.0 / Last Updated: 9 December 2010 / Project: DEMO / Author: Eugene Belyaev
 */
class Gallery extends Exoof_Db_PageTable {

    const TABLE_NAME      = 'gallery';

    protected $_tableName = 'gallery';
    protected $_nameColumn= 'gal_name';

    public function getPhotos() {
        return Photo::fetchByGallery($this->gal_id);
    }

    public function delete() {
        $photos = $this->getPhotos();

        $phTable= $this->prefixize(Photo::TABLE_NAME);

        $path   = Exoof_Factory::factory('Config')->paths->uploadPath.'/';
        if (sizeof($photos))
            foreach($photos as $photo) {
                @unlink($path.$photo['photo_file']);
        }
        $this->getDb()->delete($phTable, array('gal_id'=>$this->gal_id));

        return parent::delete();
    }

/**
* ====================================== STATIC FUNCTIONS =======================================
*/

    static function purgeOrphans() {
        $table1 = self::prefixize(self::TABLE_NAME);
        $table2 = self::prefixize('pages');

        self::getDb()->query("DELETE {$table1}
                                FROM  {$table1} LEFT JOIN  {$table2}
                                ON  {$table1}.page_id= {$table2}.page_id
                                WHERE  {$table2}.page_id IS NULL");
    }

    static public function fetchAll($where=null, $count=null, $offset=null, $orderBy='sort_code', $orderDest=null) {
        $table1  = self::prefixize(self::TABLE_NAME);
        $table2 = self::prefixize('pages');
        $sql    = "SELECT * FROM ".$table1." INNER JOIN ".$table2." USING(page_id)";

        $where[$table1.'.lang'] = Exoof_Lang::getLang();

        return self::getDb()->fetch($sql, $where, $count, $offset, $orderBy, $orderDest);
    }

    static public function fetchGalleriesAssoc() {
        $table1  = self::prefixize(self::TABLE_NAME);
        $sql     = "SELECT gal_id, gal_name FROM ".$table1." WHERE lang=?";

        return self::getDb()->fetchAssoc($sql, array(Exoof_Lang::getLang()));
    }
}
?>
