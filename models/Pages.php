<?php
/**
 * Pages model
 *
 * @author eugene
 */
class Pages extends Exoof_Db_Table {

    const TABLE_NAME      = 'pages';
    protected $_tableName = 'pages';

    public function findByFullURLAndLang($url, $path, $lang) {
        return $this->find("SELECT * FROM ".$this->_tableName." WHERE page_url=? AND page_path=? AND lang=?", array($url, $path, $lang));
    }

    public function save() {

        if (!$this->page_date_added)
            $this->page_date_added = time();


        $this->parent_page_id = ($this->parent_page_id>0)?$this->parent_page_id:null;

        if (strlen($this->page_url)>1 && substr($this->page_url,-1,1)!='/')
            $this->page_url .= '/';

        $this->lang = Exoof_Lang::getLang();
        if ($this->parent_page_id) // REFRESH PATH FROM PARENT
        {
            $parent = new Pages($this->parent_page_id);
            $this->page_path = $parent->page_path.$parent->page_url;
            //if (empty($this->page_path))
            //    $this->page_path = '/';
        }

        //sort code for new objects
        if (!strlen($this->sort_code)) {
            if (isset($parent)) {
                $next_sort_code = $this->getNextSortCode($this->parent_page_id);
                $this->sort_code= "{$parent->sort_code}-{$next_sort_code}";
            } elseif (!isset($parent)) {
                $next_sort_code = $this->getNextSortCode(null);
                $this->sort_code= $next_sort_code;
            }
        }

        //level
        $this->page_level = sizeof(explode('/',$this->page_path.$this->page_url))-1;
        return parent::save();
    }

    protected function getNextSortCode($parent, $formatted=true) {

        $select  = "SELECT MAX(sort_code) as max_code FROM ".$this->_tableName;
        $select .= " WHERE lang = '".$this->lang."'";
        if ($parent)
            $select .= " AND parent_page_id = ".$parent;
        else
            $select .= " AND parent_page_id IS NULL";
        $select .= " GROUP BY parent_page_id";

        $maxSort = $this->_pdo->fetchColumn($select);
        
        $code  = ($maxSort) ? intval(array_pop(explode('-',$maxSort))) : 0;

        if (!$formatted)
            return ($code+1);
        else
            return str_pad($code+1, 6, '0', STR_PAD_LEFT);
    }

    public function moveSortUp() {
        //find next nested element
        $sql = "SELECT * FROM ".$this->_tableName." WHERE parent_page_id = ? AND sort_code < ? ORDER BY sort_code DESC";
        $params = array(
            $this->parent_page_id,
            $this->sort_code
        );
        $prevPage = $this->_pdo->fetchRow($sql, $params);


        if ($prevPage)  {
            $sc2             = $this->sort_code;
            $sc1             = $prevPage['sort_code'];

            $this->_pdo->query("UPDATE ".$this->_tableName." SET sort_code=REPLACE(sort_code,'".$sc2."','TMP".$sc1."') WHERE sort_code LIKE '".$sc2."%'");
            $this->_pdo->query("UPDATE ".$this->_tableName." SET sort_code=REPLACE(sort_code,'".$sc1."','".$sc2."') WHERE sort_code LIKE '".$sc1."%'");
            $this->_pdo->query("UPDATE ".$this->_tableName." SET sort_code=REPLACE(sort_code,'TMP".$sc1."','".$sc1."') WHERE sort_code LIKE 'TMP".$sc1."%'");
        }
    }

    public function moveSortDown() {
        //find next nested element
        $sql = "SELECT * FROM ".$this->_tableName." WHERE parent_page_id = ? AND sort_code > ? ORDER BY sort_code ASC";
        $params = array(
            $this->parent_page_id,
            $this->sort_code
        );
        $nextPage = $this->_pdo->fetchRow($sql, $params);

        if ($nextPage)  {
            $sc2             = $this->sort_code;
            $sc1             = $nextPage['sort_code'];

            $this->_pdo->query("UPDATE ".$this->_tableName." SET sort_code=REPLACE(sort_code,'".$sc2."','TMP".$sc1."') WHERE sort_code LIKE '".$sc2."%'");
            $this->_pdo->query("UPDATE ".$this->_tableName." SET sort_code=REPLACE(sort_code,'".$sc1."','".$sc2."') WHERE sort_code LIKE '".$sc1."%'");
            $this->_pdo->query("UPDATE ".$this->_tableName." SET sort_code=REPLACE(sort_code,'TMP".$sc1."','".$sc1."') WHERE sort_code LIKE 'TMP".$sc1."%'");
        }
    }

    public function delete() {
        $path = $this->page_path.$this->page_url;
        $deleted = parent::delete();
        if ($deleted)
            $this->_pdo->query("DELETE FROM ".$this->_tableName." WHERE page_path LIKE '".$path."%'");
        return $deleted;
    }
/*
 *  ================== STATIC FUNCTIONS ==============================================================================
 */

    static public function fetchAll($where=null, $count=null, $offset=null, $orderBy='sort_code', $orderDest=null) {
        $table  = self::prefixize(self::TABLE_NAME);
        $ptypes = self::prefixize('page_types');
        $sql    = "SELECT * FROM ".$table." INNER JOIN ".$ptypes." USING(ptype_id)";

        $where['lang'] = Exoof_Lang::getLang();

        return self::getDb()->fetch($sql, $where, $count, $offset, $orderBy, $orderDest);
    }

    static public function countLangPages($lang) {
        $table  = self::prefixize(self::TABLE_NAME);
        $sql    = "SELECT COUNT(*) as count FROM ".$table." WHERE lang=?";

        return self::getDb()->fetchColumn($sql, array($lang));
    }
}
?>
