<?php
/**
 * Option model
 *
 * @author eugene
 * @version 1.1 Last Updated: 12 OCtober 2010 / elearning
 */
class Option extends Exoof_Db_Table {

    const TABLE_NAME      = 'options';

    protected $_tableName = 'options';

    static public function fetchAll($where=null, $count=null, $offset=null, $orderBy='opt_id', $orderDest=null) {
        $table1  = self::prefixize(self::TABLE_NAME);

        $where['lang'] = Exoof_Lang::getLang();
        
        return self::getDb()->fetch($table1, $where, $count, $offset, $orderBy, $orderDest);
    }

    public function search($variable) {
        $lang = Exoof_Lang::getLang();
        return $this->find("SELECT * FROM ".$this->_tableName." WHERE opt_variable=? AND lang=?", array($variable, $lang));
    }
}
?>
