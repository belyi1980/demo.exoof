<?php
/**
 * Administrator model
 *
 * @author eugene
 */
class Administrator extends Exoof_Db_Table {

    const TABLE_NAME      = 'administrators';

    protected $_tableName = 'administrators';

    public function save() {
    	if (strlen($this->adm_passw) && (!preg_match("/^[abcdef0-9]{40}$/i",$this->adm_passw)))
    		$this->adm_passw = sha1($this->adm_passw);

        return parent::save();
    }

    public function findByLoginAndPassw($login, $password) {
        return $this->find("SELECT * FROM ".$this->_tableName." WHERE adm_login=? AND adm_passw=?", array($login, sha1($password)));
    }



    static public function fetchAll($where=null, $count=null, $offset=null, $orderBy='adm_id', $orderDest=null) {
        $table1  = self::prefixize(self::TABLE_NAME);
        $sql    = "SELECT * FROM ".$table1;

        return self::getDb()->fetch($sql, $where, $count, $offset, $orderBy, $orderDest);
    }
}
?>
