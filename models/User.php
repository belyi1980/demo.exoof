<?php
/**
 * User
 *
 * @author eugene
 */
class User extends Exoof_Db_Table {

    const TABLE_NAME   = 'users';
    public $_tableName = 'users';

    public function save() {

        if (strlen($this->u_password) && (!preg_match("/^[abcdef0-9]{40}$/i",$this->u_password)))
            $this->u_password = sha1($this->u_password);

        if (!$this->u_date_added)
            $this->u_date_added = time();

        if (!$this->u_hash) {
            $this->u_hash = sha1(uniqid(time(),true));
        }

        //if (!$this->u_age)
                $this->u_age = $this->getAge();
        Exoof_Cache::getInstance()->engine->delete('user'.$this->u_id);

        return parent::save();
    }

    public function getSmoke() {
        $keys = array('1'=>'NoValue','2'=>'YesValue');
        $val  = $this->u_smoke;
        if ($val)
            return Exoof_Translate::getInstance()->translate($keys[$val]);
    }
    
    public function getChildren() {
        $keys = array('1'=>'NoValue','2'=>'YesValue');
        $val  = $this->u_children;
        if ($val)
            return Exoof_Translate::getInstance()->translate($keys[$val]);
    }

    public function getAge() {
        //if ($this->u_age > 0)
        //        return $this->u_age;

        if (!$this->u_bday)
                return null;

        $year_diff  = date("Y") - date("Y",$this->u_bday);
        $month_diff = date("m") - date("m",$this->u_bday);
        $day_diff   = date("d") - date("d",$this->u_bday);
        if ($month_diff < 0)
            $year_diff--;
        elseif (($month_diff==0) && ($day_diff < 0))
            $year_diff--;
        return $year_diff;
    }

    public function getFavorites() {
        $table = $this->prefixize('cgr_favorites');
        $where['u_id'] = $this->u_id;
        return $this->getDb()->fetch($table, $where);
    }

    public function getOnlineFavorites() {
        $table = $this->prefixize('cgr_favorites');

        $time  = time()-60;
        $sql   = "SELECT uf.* FROM ".$table." AS uf INNER JOIN ".$this->_tableName." AS u
                  ON uf.u_id=".$this->u_id." AND uf.f_u_id=u.u_id WHERE u_visible=1 AND u.u_last_activity > ".$time;
        return $this->getDb()->fetch($sql);
    }



    public function getVisitors() {
        $table = $this->prefixize('cgr_visitors');
        $this->_pdo->update($table, array('visit_viewed'=>1), "u_id = '{$this->u_id}' AND visit_viewed=0");
        return $this->getDb()->fetch($table, array('u_id'=>$this->u_id), 20, null, 'id', 'DESC');
    }

    public function getNewVisitorsCount() {
        $table = $this->prefixize('cgr_visitors');
        return $this->getDb()->fetchCount($table, array('u_id'=>$this->u_id,'visit_viewed'=>0));
    }

    public function addVisit($visitor_id) {
        $arr = array();
        $arr['u_id'] = $this->u_id;
        $arr['visitor_id'] = $visitor_id;
        $arr['visit_date'] = time();

        return $this->getDb()->insert($this->prefixize('cgr_visitors'), $arr);
    }

    public function removeFavorite($id) {
        $table = $this->prefixize('cgr_favorites');
        $where['u_id'] = $this->u_id;
        $where['f_u_id'] = $id;

        return $this->getDb()->delete($table, $where);
    }


    public function addFavorite($id, $fname) {
        $table = $this->prefixize('cgr_favorites');
        $add['u_id'] = $this->u_id;
        $add['f_u_id'] = $id;
        $add['f_u_fname'] = $fname;

        try {
            $added = $this->getDb()->insert($table, $add);
        } catch (PDOException $e) {
            $added = 0;
        }
        return $added;
    }

    public function getUserPhotos() {

        $table = $this->prefixize('users_photo');
        $where['u_id'] = $this->u_id;
        return $this->getDb()->fetch($table, $where);
    }

 /*   public function deleteUserPhotos() {

        $table = $this->prefixize('users_photo');
        $where['u_id'] = $this->u_id;
        return $this->getDb()->delete($table, $where);
    }*/

    public function getUserPhoto($uph_id) {
        $table = $this->prefixize('users_photo');
        $where['uph_id'] = $uph_id;
        $where['u_id'] = $this->u_id;
        return @array_shift($this->getDb()->fetch($table, $where));
    }

    public function addUserPhoto($file, $description) {
        $table = $this->prefixize('users_photo');

        $photo = array(
            'u_id'   => $this->u_id,
            'uph_file'=>$file,
            'uph_description'=>$description
        );

        return $this->_pdo->insert($table,$photo);
    }

    public function editUserPhoto($id, $file, $description) {
        $table = $this->prefixize('users_photo');

        $photo = array(
            'u_id'   => $this->u_id
        );

        if ($description)
            $photo['uph_description'] = $description;
        if ($file)
            $photo['uph_file'] = $file;

        return $this->_pdo->update($table, $photo, array('uph_id'=>$id));
    }

    public function delUserPhoto($idx) {
        $table = $this->prefixize('users_photo');
        return $this->_pdo->delete($table, array('u_id'=>$this->u_id, 'uph_id'=>$idx));
    }


    public function purgeUserObjects() {
        //PHOTOS
        $this->_pdo->delete($this->prefixize('users_photo'), array('u_id'=>$this->u_id));
        //MESSAGES
        $table = $this->prefixize('cgr_messages');
        $this->_pdo->delete($table, array('u_id'=>$this->u_id));
        $this->_pdo->delete($table, array('u_id_2'=>$this->u_id));
        //GIFTS
        $table = $this->prefixize('cgr_gifts_sent');
        $this->_pdo->delete($table, array('u_id'=>$this->u_id));
        $this->_pdo->delete($table, array('f_u_id'=>$this->u_id));
        //VISITS
        $table = $this->prefixize('cgr_visitors');
        $this->_pdo->delete($table, array('u_id'=>$this->u_id));
        $this->_pdo->delete($table, array('visitor_id'=>$this->u_id));
        //FAVORITES
        $table = $this->prefixize('cgr_favorites');
        $this->_pdo->delete($table, array('u_id'=>$this->u_id));
        $this->_pdo->delete($table, array('f_u_id'=>$this->u_id));
        //BLOG COMMENTS
        //$this->_pdo->delete($this->prefixize('blog_comments'), array('u_id'=>$this->u_id));

    }

    public function mainUserPhoto($idx) {
        $table = $this->prefixize('users_photo');
        $row   = $this->_pdo->fetchRow("SELECT * FROM {$table} WHERE u_id=? AND uph_id=?", array($this->u_id, $idx));

        if ($row['uph_file']) {
            $this->u_avatar = $row['uph_file'];
            $this->save();
        }
    }

    public function findByLoginAndPassword($login, $password) {
        $result = $this->_pdo->fetchRow("SELECT * FROM ".$this->_tableName." WHERE u_login=? AND u_password=? AND u_active > 0", array($login, sha1($password)));
        if ($result != false && sizeof($result)) {
            $this->_fill($result);
            return true;
        }
        else
            return false;
    }

    // STATIC FUNCTIONS
    static public function fetchOnlineCount() {
        $table1  = self::prefixize(self::TABLE_NAME);

        $sql   = "SELECT * FROM ".$table1." WHERE u_active=1 AND u_visible=1  AND u_last_activity > ".(time()-60);
        
        return self::getDb()->fetchCount($sql);
    }

    static public function fetchLastRegistered($count=20, $sex) {
        $table1  = self::prefixize(self::TABLE_NAME);

        $sql   = "SELECT * FROM ".$table1." WHERE u_active=1 AND u_visible=1 AND u_sex='".$sex."'";

        return self::getDb()->fetch($sql, null, $count, null, 'u_id', 'DESC');
    }

    static public function fetchOnline($u_id, $u_sex=null, $age_f=null, $age_t=null, $city=null, $photo=null, $online=null, $count=null, $offset=null, $orderBy='u_id', $orderDest='DESC') {
        $table1  = self::prefixize(self::TABLE_NAME);

        $sql   = "SELECT * FROM ".$table1." WHERE u_active=1 AND u_visible=1" ;

        if ($u_id)
            $sql .=  " AND u_id <> ".$u_id;

        if ($u_sex) {
            //$u_sex = ($u_sex=='man')?'woman':'man';
            $sql .=  " AND u_sex = '".$u_sex."'";
        }

        if ($online)
            $sql .= " AND u_last_activity > ".(time()-60);

        if ($age_f && $age_t)
            $sql .= ' AND u_age BETWEEN '.$age_f.' AND '.$age_t.(sizeof($where)?' AND':'');
        elseif ($age_f)
            $sql .= " AND u_age > ".(int)$age_f;
        elseif ($age_t)
            $sql .= " AND u_age < ".(int)$age_t;

        if ($city)
            $sql .= " AND u_city LIKE '%".$city."%'";

        if ($photo)
            $sql .= " AND u_avatar <> ''";
        return array(
                'count'=> self::getDb()->fetchCount($sql),
                'data' => self::getDb()->fetch($sql, null, $count, $offset, $orderBy, $orderDest)
            );
    }

    static public function fetchAll($where=null, $count=null, $offset=null, $orderBy='u_id', $orderDest=null) {
        $table1  = self::prefixize(self::TABLE_NAME);
        $sql    = "SELECT * FROM ".$table1;

        return self::getDb()->fetch($sql, $where, $count, $offset, $orderBy, $orderDest);
    }

    static public function fetchUnactive($where=null, $count=null, $offset=null, $orderBy='u_id', $orderDest=null) {
        $where['u_active'] = 0;
        return self::fetchAll($where, $count, $offset, $orderBy, $orderDest);
    }

    static public function searchQuick($u_id, $iam, $lookfor, $count=null, $offset=null, $orderBy=null, $orderDest=null) {
        $table1  = self::prefixize(self::TABLE_NAME);

        if ($u_id)
            $where['u_id']      = Exoof_Db::directField('<>',$u_id);
        $where['u_visible'] = 1;
        $where['u_active']  = 1;
        $where['u_sex']     = $lookfor;
        $where['u_search']  = $iam;
        return
            array(
                'count'=> self::getDb()->fetchCount($table1, $where),
                'data' => self::getDb()->fetch($table1, $where, $count, $offset, $orderBy, $orderDest)
            );
    }

    static public function searchAdvanced($u_id, $u_sex, $params, $count=null, $offset=null, $orderBy=null, $orderDest=null) {
        $table1  = self::prefixize(self::TABLE_NAME);
        $sql     = "SELECT * FROM ".$table1;

        $where = array();
        if ($u_id)
            $where['u_id']      = Exoof_Db::directField('<>',$u_id);

        if ($u_sex) {
            //$u_sex = ($u_sex=='man')?'woman':'man';
            $where['u_sex']      = $u_sex;
        }
        $where['u_active']  = 1;
        $where['u_visible'] = 1;
        
        $age_f = null; $age_t = null;

        foreach ($params as $key=>$val) {
            if (empty($val))
                continue;
            if (strstr($key,'u_') && !strstr($key,'u_age_'))
                    $where[$key] = $val;
            elseif ($key == 'u_age_1')
                $age_f = $val;
            elseif ($key == 'u_age_2')
                $age_t = $val;
        }

        if ($age_f && $age_t)
            $sql .= ' WHERE u_age BETWEEN '.$age_f.' AND '.$age_t.(sizeof($where)?' AND':'');
        elseif ($age_f)
            $where['u_age'] = Exoof_Db::directField(" > ",(int)$val);
        elseif ($age_t)
            $where['u_age'] = Exoof_Db::directField(" < ",(int)$val);
        
        return
            array(
                'count'=> self::getDb()->fetchCount($sql, $where),
                'data' => self::getDb()->fetch($sql, $where, $count, $offset, $orderBy, $orderDest)
            );
    }

}
?>
