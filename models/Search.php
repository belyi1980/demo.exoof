<?php
class Search extends Exoof_Db_Table {

    static public function makeSearch($string) {
        $tableP = self::prefixize(PageType::TABLE_NAME);

        $ptypes = self::getDb()->fetchAll($tableP);
        $items  = array();
        foreach ($ptypes as $ptype) {
            $model = $ptype['ptype_model'];
            if (method_exists($model, 'search'))
                $items += $model::search($string);
        }

        return $items;
    }

}
