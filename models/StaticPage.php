<?php

/**
 * StaticPage
 *
 * @author eugene
 */
class StaticPage extends Exoof_Db_PageTable {
    const TABLE_NAME = 'static_pages';

    protected $_tableName = 'static_pages';
    protected $_nameColumn = 'sp_name';

    /**
     * ====================================== STATIC FUNCTIONS =======================================
     */
    static function search($string, $count=null, $offset=null) {
        $table1 = self::prefixize(self::TABLE_NAME);
        $table2 = self::prefixize('pages');

        $sql = "SELECT p.*,t.sp_text as text FROM " . $table1 . " AS t INNER JOIN " . $table2 . " AS p USING(page_id)
                WHERE MATCH(p.page_title,p.page_meta_description,p.page_meta_keywords,t.sp_text) AGAINST('" . $string . "' IN BOOLEAN MODE) AND p.lang='".Exoof_Lang::getLang()."'";

        return self::getDb()->fetch($sql, null, $count, $offset);
    }

    static function purgeOrphans() {
        $table1 = self::prefixize(self::TABLE_NAME);
        $table2 = self::prefixize('pages');

        self::getDb()->query("DELETE {$table1}
                                FROM  {$table1} LEFT JOIN  {$table2}
                                ON  {$table1}.page_id= {$table2}.page_id
                                WHERE  {$table2}.page_id IS NULL");
    }

    static public function fetchAll($where=null, $count=null, $offset=null, $orderBy='sort_code', $orderDest=null) {
        $table1 = self::prefixize(self::TABLE_NAME);
        $table2 = self::prefixize('pages');
        $sql = "SELECT * FROM " . $table1 . " INNER JOIN " . $table2 . " USING(page_id)";

        $where[$table1 . '.lang'] = Exoof_Lang::getLang();

        return self::getDb()->fetch($sql, $where, $count, $offset, $orderBy, $orderDest);
    }

}

?>
