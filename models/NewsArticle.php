<?php
/**
 * News
 *
 * @author eugene
 * @version 1.0
 */
class NewsArticle extends Exoof_Db_PageTable {

    const TABLE_NAME   = 'news_articles';
    public $_tableName = 'news_articles';
    protected $_nameColumn= 'news_title';

    static function search($string, $count=null, $offset=null) {
        $table1 = self::prefixize(self::TABLE_NAME);
        $table2 = self::prefixize('pages');

        $sql = "SELECT p.*,t.news_announce as text FROM " . $table1 . " AS t INNER JOIN " . $table2 . " AS p USING(page_id)
                WHERE MATCH(p.page_title,p.page_meta_description,p.page_meta_keywords,t.news_announce,t.news_text) AGAINST('" . $string . "' IN BOOLEAN MODE) AND p.lang='".Exoof_Lang::getLang()."'";

        return self::getDb()->fetch($sql, null, $count, $offset);
    }



    static function purgeOrphans() {
        $table1 = self::prefixize(self::TABLE_NAME);
        $table2 = self::prefixize('pages');

        self::getDb()->query("DELETE {$table1}
                                FROM  {$table1} LEFT JOIN  {$table2}
                                ON  {$table1}.page_id= {$table2}.page_id
                                WHERE  {$table2}.page_id IS NULL");
    }


    static public function fetchAll($where=null, $count=null, $offset=null, $orderBy='news_id', $orderDest=null) {
        $table1  = self::prefixize(self::TABLE_NAME);
        $sql    = "SELECT * FROM ".$table1;

        $where['lang'] = Exoof_Lang::getLang();

        return self::getDb()->fetch($sql, $where, $count, $offset, $orderBy, $orderDest);
    }

    static public function fetchWithBlock($where=null, $count=null, $offset=null, $orderBy='news_date', $orderDest='DESC') {
        $table1  = self::prefixize(self::TABLE_NAME);
//        $table2  = self::prefixize(NewsBlock::TABLE_NAME);
        $table3  = self::prefixize(Pages::TABLE_NAME);
        $sql    = "SELECT * FROM ".$table1." AS t1 ".
//                  " INNER JOIN {$table2} USING(nb_id)".
                  " INNER JOIN {$table3} USING(page_id)" ;

        $where['t1.lang'] = Exoof_Lang::getLang();

        return self::getDb()->fetch($sql, $where, $count, $offset, $orderBy, $orderDest);
    }

}
?>
