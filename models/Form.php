<?php
/**
 * Feedback Form module
 *
 * @author Eugene
 * @version 1.0 / Last Updated: 18 October 2010 / Project:DEMO / Author: Eugene Belyaev
 */
class Form extends Exoof_Db_PageTable {

    const TABLE_NAME   = 'forms';
    public $_tableName = 'forms';
    protected $_nameColumn= 'form_title';

    public $fields = array();

    /**
     * overrided
     */
    protected function _fill($array) {
        parent::_fill($array);
        if ($this->isLoaded())
          $this->fields = unserialize($this->form_fields_data);
    }

    public function  save($updateRelated = true) {
        $ret = parent::save($updateRelated);
        if ($ret)
            $this->fields = unserialize($this->form_fields_data);
        return $ret;
    }

    static function search($string, $count=null, $offset=null) {
        $table1 = self::prefixize(self::TABLE_NAME);
        $table2 = self::prefixize('pages');

        $sql = "SELECT p.*,CONCAT(t.form_text_before,t.form_text_after) as text FROM " . $table1 . " AS t INNER JOIN " . $table2 . " AS p USING(page_id)
                WHERE MATCH(p.page_title,p.page_meta_description,p.page_meta_keywords,t.form_text_before,t.form_text_after) AGAINST('" . $string . "' IN BOOLEAN MODE) AND p.lang='".Exoof_Lang::getLang()."'";

        return self::getDb()->fetch($sql, null, $count, $offset);
    }



    static function purgeOrphans() {
        $table1 = self::prefixize(self::TABLE_NAME);
        $table2 = self::prefixize('pages');

        self::getDb()->query("DELETE {$table1}
                                FROM  {$table1} LEFT JOIN  {$table2}
                                ON  {$table1}.page_id= {$table2}.page_id
                                WHERE  {$table2}.page_id IS NULL");
    }

    static public function fetchAll($where=null, $count=null, $offset=null, $orderBy='form_id', $orderDest=null) {
        $table1  = self::prefixize(self::TABLE_NAME);
        $sql    = "SELECT * FROM ".$table1;

        $where['lang'] = Exoof_Lang::getLang();

        return self::getDb()->fetch($sql, $where, $count, $offset, $orderBy, $orderDest);
    }

    static public function fetchWithBlock($where=null, $count=null, $offset=null, $orderBy='news_date', $orderDest='DESC') {
        $table1  = self::prefixize(self::TABLE_NAME);
//        $table2  = self::prefixize(NewsBlock::TABLE_NAME);
        $table3  = self::prefixize(Pages::TABLE_NAME);
        $sql    = "SELECT * FROM ".$table1." AS t1 ".
//                  " INNER JOIN {$table2} USING(nb_id)".
                  " INNER JOIN {$table3} USING(page_id)" ;

        $where['t1.lang'] = Exoof_Lang::getLang();

        return self::getDb()->fetch($sql, $where, $count, $offset, $orderBy, $orderDest);
    }

}
?>
