<?php
/**
 * PageType model
 *
 * @author eugene
 */
class PageType extends Exoof_Db_Table {

    const TABLE_NAME      = 'page_types';
    protected $_tableName = 'page_types';


    static public function fetchTypes() {
        $table  = self::prefixize(self::TABLE_NAME);
        $sql    = "SELECT ptype_id,ptype_name FROM ".$table;//." WHERE ptype_id<4";

        return self::getDb()->fetchAssoc($sql);
    }
}
?>
