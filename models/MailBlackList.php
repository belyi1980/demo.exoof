<?php
/**
 * MailBlackList
 *
 * @author eugene
 */
class MailBlackList extends Exoof_Db_Table  {

    const TABLE_NAME   = 'mail_blacklist';
    public $_tableName = 'mail_blacklist';

    public function save() {

    	if (!$this->date_added)
    	   $this->date_added = time();
        return parent::save();
    }
}