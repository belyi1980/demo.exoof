<?php
/**
 * Exoof Admin Controller
 *
 * @author eugene
 * @version 1.2 / Last Updated : 5 November 2010 / Project: DEMO / Author: Eugene Belyaev
 */
class Exoof_Admin_Controller extends Exoof_Controller {

    public function  __construct($controller, $action, $ctrlPath, $view = null) {
        parent::__construct($controller, $action, $ctrlPath, $view);
        $this->controllersPath = $this->config->paths->adminControllers;
    }

    public function _($string) {
        return Exoof_Factory::factory('Translate')->translate($string);
    }

    protected function buildList($title, $columns, $model_callback) {

        //if load data requested
        //if ($this->getRequest()->getParam('loadData',0))
        $where = null;
        if (isset($model_callback[1]) && is_array($model_callback[1]) && sizeof($model_callback[1])) {
            $where = $model_callback[1];
            $model_callback = $model_callback[0];
        }
        elseif (isset($model_callback[2]) && is_array($model_callback[2]) && sizeof($model_callback[2])) {
            $where = $model_callback[2];
            $model_callback = array_slice($model_callback, 0, 2);
        }

        $rows = call_user_func($model_callback,$where,$this->getParam('count',100),$this->getParam('offset',0));
        $list['rows'] = $rows;

        //buil table header requested
        $list['count']   = $this->getParam('count',100);
        $list['offset']  = $this->getParam('offset',0);
        $list['title']   = $title;
        $list['columns'] = $columns;
        return $list;
    }

}