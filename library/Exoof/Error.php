<?php
/**
 * Error handler class
 * @author Eugene Belyaev <work@belyaev.biz>
 * @version 1.2 / Last Updated: 30 November 2010 / Project: DEMO / Authhor: Eugene Belyaev /
 */

class Exoof_Error {

    private $_errTypes = array(
       E_ERROR          => "FATAL",
       E_USER_ERROR     => "FATAL",
       E_USER_WARNING   => "WARNING",
       E_NOTICE         => "NOTICE",
       E_PARSE          => "PARSE",
       E_CORE_WARNING   => "WARNING",
       E_CORE_ERROR     => "CORE",
       E_DEPRECATED     => "DEPRECATED"
    );
    /**
     * Constructor
     * @return Exoof_Error
     */
    public function __construct() {
        $this->ERROR_LEVEL = error_reporting();
        $this->DISPLAY_ERRORS = (Exoof_Factory::factory('Config')->debug->displayErrors == 'no') ? false : true;
        $this->DISPLAY_BACKTRACE = (Exoof_Factory::factory('Config')->debug->displayBacktrace == 'no') ? false : true;

        $this->MAIL_ERRORS    = (Exoof_Factory::factory('Config')->debug->mailErrors == 'no') ? false : true;
        $this->MAIL_RECEIVER  = Exoof_Factory::factory('Config')->debug->mailTo;
        $this->MAIL_BACKTRACE = $this->DISPLAY_BACKTRACE;

        $this->LOG_ERRORS     = (Exoof_Factory::factory('Config')->debug->logErrors == 'no') ? false : true;

//		error_reporting($this->ERROR_LEVEL);

        set_error_handler(array($this, "exoofErrorHandler"));
        register_shutdown_function(array($this,"exoofFatalHandler"));
    }

    public function _displayError($no, $type, $str, $line, $file, $context=null) {
        ob_start();

        echo "<div style='background:#fbffc0;font:normal 12px monospace,Terminus,Lucida Console,Verdana,Tahoma;color:brown;border:1px solid brown;padding:2px;margin:2px;'>";
        echo "<b>$type</b>: <tt>$str</tt> in line <b>$line</b> of file <b>$file</b><br />\n";

        $error_html = ob_get_clean();

        ob_start();
        if ($this->DISPLAY_BACKTRACE) {
            echo "<br><fieldset style='border:1px solid brown;'><legend><b>Backtrace</b></legend>";
            echo $this->_backtrace(true);
            echo "</fieldset>\n";
        }
        $backtrace_html = ob_get_clean();

        if ($this->DISPLAY_BACKTRACE)
            $error_html .= $backtrace_html;

        $error_html .= "</div>\n";

        echo $error_html;
    }

    public function _mailError($no, $type, $str, $line, $file, $context=null) {
        ob_start();

        echo "<div style='background:#fbffc0;font:normal 12px monospace,Terminus,Lucida Console,Verdana,Tahoma;color:brown;border:1px solid brown;padding:2px;margin:2px;'>";
        echo "<b>$type</b>: <tt>$str</tt> in line <b>$line</b> of file <b>$file</b><br />\n";

        $error_html = ob_get_clean();

        ob_start();
        if ($this->MAIL_BACKTRACE) {
            echo "<br><fieldset style='border:1px solid brown;'><legend><b>Backtrace</b></legend>";
            echo $this->_backtrace(true);
            echo "</fieldset>\n";
        }
        $backtrace_html = ob_get_clean();

        if ($this->MAIL_BACKTRACE)
            $error_html .= $backtrace_html;

        $error_html .= "</div>\n";

        $email = $this->MAIL_RECEIVER;
        if (!empty($email)) {
            $site = $_SERVER['HTTP_HOST'];
            $domain = str_replace("www.", "", $site);
            $from = strtoupper($domain) . " error handler <noreply@{$domain}>";

            $url = ((isset($_SERVER['HTTPS'])) ? 'https' : 'http') . "://" . $site . $_SERVER['REQUEST_URI'];

            $subject = strtoupper($site) . " : {$type} ERROR on `{$url}`";

            $msg = "<div style='color:red;text-align:center;padding:5px;'>Something wrong happens on the " . strtoupper($site) . "<br>Please analyze below backtrace and correct the error as soon as possible</div>";
            $msg .= $error_html;
            $msg .= "<br><hr><pre>";
            $msg .= "\nServer Host : " . $site;
            $msg .= "\nServer IP : " . $_SERVER['SERVER_ADDR'];
            $msg .= "\nTime : " . date("d.m.Y H:i:s", time());
            $msg .= "\nUser IP : " . $_SERVER['REMOTE_ADDR'];
            $msg .= "\nUser Agent : " . $_SERVER['HTTP_USER_AGENT'];
            $msg .= "\nURL Loaded : {$url}";
            $msg .= "\nReferer URL : " . $_SERVER['HTTP_REFERER'];
            $msg .= "\nRequest Method : " . $_SERVER['REQUEST_METHOD'];
            $msg .= "\nServer Software : " . $_SERVER['SERVER_SOFTWARE'];

            ob_start();
            print_r($_POST);
            $post_txt = ob_get_clean();

            ob_start();
            print_r($_GET);
            $get_txt = ob_get_clean();

            $msg .= "\n<hr>POST DATA : " . $post_txt;
            $msg .= "\n<hr>GET DATA : " . $get_txt;

            $headers = "From: {$from}\nContent-type: text/html";

            @mail($email, $subject, $msg, $headers);
        }
    }

    public function _logError($no, $type, $str, $line, $file) {
        //$path = realpath(dirname($_SERVER['SCRIPT_FILENAME']))."/".Exoof_Factory::factory('Config')->paths->logsPath;

        //$log_filename = $path."/error.log";
        //$time = date("[d.m.Y H:i:s]", time());

        $message = "%s ERROR http://%s%s : %s in line %d of file %s";
        $params  = array(
            $type,
            $_SERVER['HTTP_HOST'],
            $_SERVER['REQUEST_URI'],
            $str,
            $line,
            $file
        );
        Exoof_Tools_Logger::log("error",$message, $params);
    }

    public function _backtrace($cut_first_3 = false) {
        $output = "<div style='text-align: left; font-weight:bold; font-family: monospace,Terminus,Lucida Console;'>\n";
        $backtrace = debug_backtrace();
        if ($cut_first_3)
            $backtrace = array_slice($backtrace, 3);
        $tabs = (count($backtrace));
        if ($backtrace[0]['function'] == exoofFatalHandler)
            $backtrace = array_slice ($backtrace, 1);
        foreach ($backtrace as $bt) {
            $tabs--;
            $args = '';
            if (count($bt['args']))
                foreach ($bt['args'] as $a) {
                    if (!empty($args)) {
                        $args .= ', ';
                    }
                    switch (gettype($a)) {
                        case 'integer':
                        case 'double':
                            $args .= $a;
                            break;
                        case 'string':
                            $a = htmlspecialchars(substr($a, 0, 64)) . ((strlen($a) > 64) ? '...' : '');
                            $args .= "\"$a\"";
                            break;
                        case 'array':
                            $args .= 'Array(' . count($a) . ')';
                            break;
                        case 'object':
                            $args .= 'Object(' . get_class($a) . ')';
                            break;
                        case 'resource':
                            $args .= 'Resource(' . strstr($a, '#') . ')';
                            break;
                        case 'boolean':
                            $args .= $a ? 'True' : 'False';
                            break;
                        case 'NULL':
                            $args .= 'Null';
                            break;
                        default:
                            $args .= 'Unknown';
                    }
                }
            $nbsps = "<b>[" . ($tabs + 1) . "]</b> " . str_repeat("&nbsp;", $tabs * 2);
            $output .= $nbsps . "<b>{$bt['class']}{$bt['type']}{$bt['function']}($args)</b>";
            $output .= "&nbsp;&nbsp;<u>{$bt['file']} ,line {$bt['line']}</u><br />\n";
        }

        $output .= "</div>\n";
        return $output;
    }

    public function exoofErrorHandler($errno, $errstr, $errfile, $errline, $errcontext) {
        if (!error_reporting())
            return;

        $exit = false;
        if ($errno & $this->ERROR_LEVEL) {
            switch ($errno) {
                case E_ERROR:
                case E_USER_ERROR:
                    $exit    = true;
                    break;
            }

            $errType = (isset($this->_errTypes[$errno]))?$this->_errTypes[$errno]:"UNKNOWN";

            if ($this->DISPLAY_ERRORS)
                $this->_displayError($errno, $errType, $errstr, $errline, $errfile);
            if ($this->MAIL_ERRORS)
                $this->_mailError($errno, $errType, $errstr, $errline, $errfile);
            if ($this->LOG_ERRORS)
                $this->_logError($errno, $errType, $errstr, $errline, $errfile);

            if ($exit === true)
                exit(1);

        }
    }

    public function exoofFatalHandler() {
        $error = error_get_last();
        if ($error != null)
            $this->exoofErrorHandler($error['type'], $error['message'], $error['file'], $error['line'], $errcontext);
    }


}