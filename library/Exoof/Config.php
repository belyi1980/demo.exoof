<?php
/**
 * Exoof Config object
 *
 * @author eugene
 */

class Exoof_Config {

    /*
     * @type array
     */
    private $config = array();

    public function __construct(array $config) {
        $this->config = $config;
        foreach($config as $key=>$value)
        {

            if (is_array($value))
                $this->config[$key] = new self($value);
             else
                $this->config[$key] = $value;
        }
    }

    /*
     * @return mixed
     */
    public function  __get($name) {
        if (array_key_exists($name, $this->config))
            return $this->config[$name];
        else
            return null;
    }

    public function  __toString() {
        return print_r($this->config,true);
    }
}
?>
