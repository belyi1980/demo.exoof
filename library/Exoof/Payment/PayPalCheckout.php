<?
define("PAY_STATUS_NEW",1);
define("PAY_STATUS_PROCESSING",2);
define("PAY_STATUS_CHARGED",4);
define("PAY_STATUS_FAILED",5);
define("PAY_STATUS_CLOSED",99);
/**
 * PayPal Checkout 
 * @author Eugene Belyaev
 * @version 1.0
 *
 * @example 
 * 
 *		$GC = new PayPalCheckout($this->ado,"sb_payment_transactions","my@paypal.com","USD",true);
 *		$products = array();
			
		$HTML = $cart = $GC->generateCart('5.99','Super Product','http://my.com/payment.php');
		$GC->createOrder();
 */
class Exoof_Payment_PayPalCheckout
{
	public $db_table;

        /**
         * Databe object
         * @var Exoof_Db
         */
	public $db;
	public $merchant_id;
	public $trans_key;
	public $trans_comments;
	public $log = false;
	public $trans_amount;
	public $cart_xml;
	public $currency;
	public $testMode = false;

        private $okURL = 'payment/success';
        private $cancelURL = 'payment/fail';
        private $notifyURL = 'payment/notify';
	
	public function __construct($merchant_id, $currency="USD",$testMode=false)
	{
		$this->db		=  Exoof_Factory::factory('Db');
		$this->db_table  	= 'exf_payment_transactions';
		$this->merchant_id	= strtolower($merchant_id);
		$this->currency		= $currency;
		$this->testMode		= $testMode;		
	}

	public function generateCart($trans_amount, $trans_description, $callbackURL=null)
	{
		if ($this->testMode)
			$processURL = "https://www.sandbox.paypal.com/cgi-bin/webscr";//"http://www.eliteweaver.co.uk/testing/ipntest.php";
		else
			$processURL = "https://www.paypal.com/cgi-bin/webscr";
		$this->trans_key    = sha1(rand(1000000,9999999).time());
		$this->trans_amount = floatval($trans_amount);	
		$this->trans_comments = $trans_description;
		ob_start();

                $baseHref  = (isset($_SERVER['HTTPS']))?'https://':'http://';
                $baseHref .= $_SERVER['SERVER_NAME'];
                $baseHref .= ($_SERVER['SERVER_PORT']!='80' && $_SERVER['SERVER_PORT']!='443')?':'.$_SERVER['SERVER_PORT']:null;
                if (strlen(dirname($_SERVER['PHP_SELF']))>1)
                    $baseHref .= dirname($_SERVER['PHP_SELF']);
                $baseHref .= '/';
                $baseHref .= Exoof_Lang::getLangUrl();
		?>
                    <form method="post" class="right" action="<?=$processURL?>">
                    <input type="hidden" name="cmd" value="_xclick"/>
                    <input type="hidden" name="business" value="<?=$this->merchant_id?>"/>
                    <input type="hidden" name="item_name" value="<?=$this->trans_comments?>"/>
                    <input type="hidden" name="item_number" value="<?=$this->trans_key?>"/>
                    <input type="hidden" name="amount" value="<?=$this->trans_amount?>"/>
                    <input type="hidden" name="return" value="<?=$baseHref.$this->okURL?>"/>
                    <input type="hidden" name="cancel_return" value="<?=$baseHref.$this->cancelURL?>"/>
                    <input type="hidden" name="notify_url" value="<?=$baseHref.$this->notifyURL?>?transStatus=<?=PAY_STATUS_NEW?>"/>
                    <input type="hidden" name="rm" value="2"/>
                    <input type="hidden" name="currency_code" value="<?=$this->currency?>" />
                    <input type="hidden" name="no_shipping" value="1"/>
		<?
		return ob_get_clean();
				
	}
		
	public function createOrder($arr=null)
	{
		$trans['trans_key']	 = $this->trans_key;
		$trans['trans_amount'] 	= $this->trans_amount;
		$trans['trans_comments'] = $this->trans_comments;
		$trans['trans_currency'] = $this->currency;
		$trans['trans_date']	= time();
		$trans['trans_type']	= 'PAYPAL';
		if (is_array($arr) && count($arr))
			$trans = array_merge($trans, $arr);
		
                $this->db->insert($this->db_table, $trans);
                return $this->db->lastInsertId();
	}
	
	public function processAnswer($callback_function=null)
	{
		//--- LOG
		$f = fopen("./logs/payment.log","a+");
		fputs($f,"\n>>>>>>>> PAYPAL TRANSACTION ".date("m.d.Y H:i:s")." <<<<<<<<<<<");
		fputs($f,"\n========= POST =========\n");
		foreach ($_POST as $key=>$value) 
			fputs($f, "{$key} = {$value}\n");
		fputs($f,"\n========= GET =========\n");	
		foreach ($_GET as $key=>$value) 
			fputs($f, "{$key} = {$value}\n");
			
		fclose($f);
		//-------
		
		//$this->ado->Execute("LOCK TABLES {$this->db_table} WRITE;");
		$status = $this->_registerNewOrder();
		$order  = $this->getOrder();

		$f = fopen("./logs/payment.log","a+");
		fputs($f,"\n-1.0----STATUS: {$order['trans_status']}-{$status}\n-1.0s----\n");
		fclose($f);

		if ($status==PAY_STATUS_PROCESSING || $order['trans_status']=='PROCESSING' || $order['trans_status']=='')
		{
		$f = fopen("./logs/payment.log","a+");
		fputs($f,"\n-1.1----STATUS: {$order['trans_status']}-{$status}\n-1.1----\n");
		fclose($f);
				if ($_POST['payment_status']=='Completed')
					return $this->_updateOrder(PAY_STATUS_CHARGED);
				elseif ($_POST['payment_status']=='Pending')
					return PAY_STATUS_PROCESSING;
				else
					return $this->_updateOrder(PAY_STATUS_FAILED);	
		}
		elseif ($order['trans_status']=='CLOSED')
			return PAY_STATUS_CLOSED;
		elseif ($order['trans_status']=='CHARGED')
			return PAY_STATUS_CHARGED;
		elseif ($order['trans_status']=='FAILEd')
			return PAY_STATUS_FAILED;
	}
	
	public function _registerNewOrder()
	{
		$trans['trans_key']            = $_POST['item_number'];
		$trans['trans_status']         = 'PROCESSING';
		$trans['trans_merchant_order'] = $_POST['txn_id'];		
		
		if ($this->testMode)
			$processURL = "https://www.sandbox.paypal.com/cgi-bin/webscr";//"http://www.eliteweaver.co.uk/testing/ipntest.php";
		else
			$processURL = "https://www.paypal.com/cgi-bin/webscr";		
		
 		if ($_POST['receiver_email'] != $this->merchant_id || $_POST["txn_type"] != "web_accept")
      		die("1:You should not be here ...");		
		
		//---- post back to PayPal system to validate
		  $postdata="";
		  foreach ($_POST as $key=>$value) $postdata.=$key."=".urlencode($value)."&";
		  $postdata .= "cmd=_notify-validate"; 
		  $curl = curl_init($processURL);
		  curl_setopt ($curl, CURLOPT_HEADER, 0); 
		  curl_setopt ($curl, CURLOPT_POST, 1);
		  curl_setopt ($curl, CURLOPT_POSTFIELDS, $postdata);
		  curl_setopt ($curl, CURLOPT_SSL_VERIFYPEER, 0); 
		  curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
		  curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 1);
		  $response = trim(strtoupper(curl_exec ($curl)));
		  curl_close ($curl);
		  if ($this->testMode!=false)
			{
		  		if ($response != "VERIFIED") 
					die("2:You should not do that ...");  
			}
		//----------------------------------------------		

		$shop_info 	= array();
		$buyer_info = array();
		
		foreach ($_POST as $key=>$value) 
		{
			if (strstr($key,"txn_") || strstr($key,"payment_") || strstr($key,"mc_"))
				$shop_info[$key] = $value;
			elseif (strstr($key,"payer_") || strstr($key,"address_") || strstr($key,"_name"))
				$buyer_info[$key] = $value;
		}
		
		$trans['shopping_info'] = serialize($shop_info);
		$trans['buyer_info'] 	= serialize($buyer_info);

                $where = " trans_key='".$trans['trans_key']."' AND trans_status=''";
		$updated = $this->db->update($this->db_table,$trans, $where);
		
		$f = fopen("./logs/payment.log","a+");
		fputs($f,"\n-1----UPDATED: {$updated} rows\n-1----\n");
                fputs($f,"\n-2----UPDATE INFO: ".print_r($where,true)."\n-1----\n");
		fclose($f);
		
		if ($updated)
			return PAY_STATUS_PROCESSING;
		else 
			return false;
	}
	
	function _updateOrder($transStatus=null)
	{
		$trans['trans_key']	= $_POST['item_number'];
		
		if ($transStatus==null)
			$transStatus = $_GET['transStatus'];
		
		if ($transStatus==PAY_STATUS_CHARGED)
			$trans['trans_status']  = 'CHARGED';
		elseif ($transStatus==PAY_STATUS_FAILED)
			$trans['trans_status']  = 'FAILED';

                $where = "trans_key='".$trans['trans_key']."' AND trans_status='PROCESSING'";
		$updated = $this->db->update($this->db_table,$trans, $where);

		$f = fopen("./logs/payment.log","a+");
		fputs($f,"\n-2---- UPDATED: {$updated} rows\n-2----\n");
		fclose($f);
		
		if ($updated)
		{
			if ($trans['trans_status']=='FAILED')
				return PAY_STATUS_FAILED;
			elseif ($trans['trans_status']=='CHARGED')
				return PAY_STATUS_CHARGED;
			else 
				return false;
		}
		else 
			return false;
	}
	
	static public function checkIfMyPayment()
	{
		if (isset($_POST['item_number']))
		{
			//$this->amount = $_POST['payment_gross'];
			return true;
		}
		else
			return false;
	}
	
	
	function closeOrder()
	{
		$trans['trans_key']	    = $_POST['item_number'];
		$trans['trans_status']      = 'CLOSED';

                $where[] = "trans_key='".$trans['trans_key']."'";
		$updated = $this->db->update($this->db_table,$trans, $where);

		$f = fopen("./logs/payment.log","a+");
		fputs($f,"\n-3----UPDATED: {$updated} rows\n-3----\n");
		fclose($f);

		if ($updated)
			return PAY_STATUS_CLOSED;
	}
	
	function getOrder()
	{
		$trans['trans_key']	= $_POST['item_number'];
		$f = fopen("./logs/payment.log","a+");
		fputs($f,"\n-!!!----EXTRACTING ORDER #{$trans['trans_key']} \n-!!!----\n");
		fclose($f);
                try {
                    $order = $this->db->fetchRow("SELECT * FROM {$this->db_table} WHERE trans_key=?",array($trans['trans_key']));
                    $f = fopen("./logs/payment.log","a+");
                    fputs($f,"\n-!!!----EXTRACTED TRANSACTION: {$order['trans_id']} \n-!!!!----\n");
                    fclose($f);

                    return $order;
                } catch (Exception $e) {
                    $f = fopen("./logs/payment.log","a+");
                    fputs($f,"\n-ERR----[ {$e->getMessage()} \n-ERR----\n");
                    fclose($f);
                }

	}
	
	function _callUserCallBack($callback, $status)
	{
		if (!$callback)
			return $status;
			
		if (is_array($callback) && is_object($callback[0]) && @method_exists($callback[0],$callback[1]))
			return call_user_method($callback[1],$callback[0],$status);
		elseif (function_exists($callback))
			return call_user_func($callback,$status);
		else 
			return false;
	}
	
	function _hmacSHA1($data, $merchant_key) {
			
		    $blocksize = 64;
		    $hashfunc = 'sha1';
		
		    if (strlen($merchant_key) > $blocksize) {
		        $merchant_key = pack('H*', $hashfunc($merchant_key));
		    }
		
		    $merchant_key = str_pad($merchant_key, $blocksize, chr(0x00));
		    $ipad = str_repeat(chr(0x36), $blocksize);
		    $opad = str_repeat(chr(0x5c), $blocksize);
		    $hmac = pack(
		                    'H*', $hashfunc(
		                            ($merchant_key^$opad).pack(
		                                    'H*', $hashfunc(
		                                            ($merchant_key^$ipad).$data
		                                    )
		                            )
		                    )
		                );
		    return $hmac;
		}
}

?>