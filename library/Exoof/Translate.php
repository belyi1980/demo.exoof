<?php
/**
 * Translate Class
 *
 * @author eugene
 * @version 1.4 Last Updated: 10 September 2010 elearning
 */
class Exoof_Translate {

    /**
     * @var Exoof_Translate
     */
    private static $instance;

    /**
     * @var string Path to language folders
     */
    private $_pathToLangs = './lang';

    /**
     * @var array Array of available languages
     */
    private $_langs = array();

    /**
     * @var array Array of files for every availabel language
     */
    private $_files = array();

    /**
     * @var array Extracted language data
     */
    private $_data = array();

    /**
     *
     * @var array Untranslated data
     */
    private $_untranslatedData = array();

    /**
     * @var string Current lang. Default is 'auto' - autodetect
     */
    private $_currentLang = 'auto';

    /**
     * @var boolean Is Translation enabled or not
     */
    private $_translationEnabled = true;

    /**
     * @var boolean Is Untranslated phrases should be logged or not
     */
    private $_logUntranslated = false;

    /**
     * Constructor
     */
    private function __construct() {
    }

    /**
     * Initialize Translation process
     * @param string $pathToLangs
     * @param boolean $enableTranslation
     */
    public function init($pathToLangs=null, $enableTranslation=true) {
        $this->_translationEnabled = $enableTranslation;
        if ($pathToLangs && file_exists($pathToLangs))
            $this->_pathToLangs = $pathToLangs;
        $this->_pathToLangs = realpath($this->_pathToLangs);
        $this->_scanLangs();
    }

    /**
     * Returns Exoof_Translate object
     * @return Exoof_Translate
     */
    public static function getInstance() {
        if (!is_object(self::$instance) || !(self::$instance instanceof Exoof_Translate))
            self::$instance = new Exoof_Translate();
        return self::$instance;
    }


    /**
     * Sets current language
     * @param string $lang 2-chars language code or 'auto' to autodetect
     */
    public function setCurrentLang($lang, $override=false) {
        if (!$override) {
            $selang = Exoof_Session::get('__alang');
            if (in_array($selang, $this->_langs)) {
                    $this->_currentLang = $selang;
                    return;
            }
        }
        
        if ($lang == 'auto' || in_array($lang, $this->_langs))
            $this->_currentLang = $lang;
        else
            $this->_detectLanguage();
        Exoof_Session::set('__alang', $this->getCurrentLang());
    }

    public function setLogUntranslated($log = true) {
        $this->_logUntranslated = $log?true:false;
    }

    /**
     * Returns currently used languages among availables
     * @return string 2-chars language code
     */
    public function getCurrentLang() {
        if ($this->_currentLang == 'auto')
            $this->_detectLanguage();

        return $this->_currentLang;
    }

    /**
     * Returns array of available languages
     * @return array Array of available languages
     */
    public function getLangs() {
        return $this->_langs;
    }

    /**
     * Translates string according to current language
     * @param string $string String to translate
     * @return string Translated string
     */
    public function translate($string) {
        if (empty($string))
            return;
        $string = trim($string);
        if (!isset($this->_data[$this->_currentLang]['file_processed']))
            $this->_extractData();
        //Translation exists
        if (array_key_exists($string, $this->_data[$this->_currentLang]['data']))
            return $this->_data[$this->_currentLang]['data'][$string];
        //Translation doesn`t exist
        else {
            if ($this->_logUntranslated) {
                //log for
                if (!isset($this->_untranslatedData[$this->_currentLang]['data']))
                    $this->_untranslatedData[$this->_currentLang]['data'] = array();

                $string = strip_tags(stripslashes($string));
                $this->_untranslatedData[$this->_currentLang]['data'][$string] = $string;
            }

            return $string;
        }
    }

    /**
     * Return content of the file according to the current lang
     * @param string $fileName Filename from 'lang/<en>/' folder
     * @return string Content of the file
     */
    public function loadFile($fileName) {
        $string = null;
        $lang = $this->getCurrentLang();

        $path = realpath($this->_pathToLangs.'/'.$lang).'/';
        if (file_exists($path.$fileName))
            $string = file_get_contents($path.$fileName);
        return $string;
    }

    /**
     * Returns array of user browser-based available languages
     * @return array Array of 2-chars language codes
     */
    public function getUserLanguages() {
        $ul = explode(',',strtolower($_SERVER['HTTP_ACCEPT_LANGUAGE']));
        for($l=0,$ml=sizeof($ul) ; $l < $ml ; $l++) {
            $langs[] = substr($ul[$l],0,2);
        }
        return array_unique($langs);
    }

    /**
     * Scans lang folder for language subfolders
     */
    private function _scanLangs() {
        $dir = array_slice(scandir($this->_pathToLangs),2);
        if (sizeof($dir))
            foreach($dir as $sdir) {
                if (is_dir($this->_pathToLangs.'/'.$sdir) && strlen($sdir)==2) {
                    $this->_langs[] = $sdir;
                }
            }
    }

    /**
     * @return boolean Returns TRUE on finding available language among user`s and
     */
    private function _detectLanguage() {
        $langs = $this->getUserLanguages();

        $selang = Exoof_Session::get('__alang');

        for($l=0,$ml=sizeof($langs) ; $l < $ml ; $l++) {
            if ($selang != null)
                if (in_array($selang, $this->_langs)) {
                    $this->_currentLang = $selang;
                    return true;
                }
            if (in_array($langs[$l], $this->_langs)) {
                $this->_currentLang = $langs[$l];
                Exoof_Session::set('__alang', $this->_currentLang);
                return true;
            }
        }
        $this->_currentLang = @$this->_langs[0];
        Exoof_Session::set('__alang', $this->_currentLang);
        return false;
    }

    /**
     * Extracts data from language INI-files
     */
    private function _extractData() {

        if ($this->_currentLang == 'auto')
            $this->_detectLanguage();

        $fData = array();

        if ($this->isCacheEnabled()) {
            $cachedData = $this->_cacheEngine->fetch(__CLASS__.'_fData');
            if ($cachedData != false && isset($cachedData[$this->_currentLang])) {
                $this->_data = $cachedData;
                return;
            }
        }

        $this->_files[$this->_currentLang] = array_slice(scandir($this->_pathToLangs.'/'.$this->_currentLang),2);
        for($f=0,$mf=sizeof($this->_files[$this->_currentLang]); $f < $mf ; $f++) {
            if (substr($this->_files[$this->_currentLang][$f],-4) == '.ini')
                $fData += parse_ini_file($this->_pathToLangs.'/'.$this->_currentLang.'/'.$this->_files[$this->_currentLang][$f],false);
        }
        $this->_data[$this->_currentLang]['file_processed'] = true;
        $this->_data[$this->_currentLang]['data'] = $fData;

        if  ($this->isCacheEnabled())
            $this->_cacheEngine->store(__CLASS__.'_fData', $this->_data, $this->_cacheExpire);
    }

    public function  __destruct() {
        if ($this->_logUntranslated) {
            if (isset($this->_untranslatedData[$this->_currentLang]['data']) && sizeof($this->_untranslatedData[$this->_currentLang]['data'])) {
//                $file = '_auto_'.date('d_m_Y').'.ini';
                $file = '_auto_translate.ini';
                $lang = $this->getCurrentLang();
                $path = realpath($this->_pathToLangs.'/'.$lang).'/';

                $data = "";
                foreach ($this->_untranslatedData[$this->_currentLang]['data'] as $string) {
                  if (in_array(strtolower($string), array('null', 'yes', 'no', 'true', 'false', 'on', 'off', 'none')))
                          continue;
                    if (preg_match("/[\/?{}|&~![()^\"]/", $string)) {
                        //$key    = preg_replace("/[\/?{}|&~![()^\"]/", "", $string);
                        //$string = preg_replace("/([^\w\s])/", "\\\""."\\1"."\\\"", $string);
                        $data .= ";{$string} = \"{$string}\" ; Special characters detected. Change manually !\n";
                    }
                    elseif (preg_match("/\w/", $string))
                        $data .= "{$string} = \"{$string}\"\n";
                }
                if (strlen($data)) {
                    $data = "\n; --- auto added ".date('d/m/Y H:i:s')." ---\n\n".$data;
                    @chmod($path.$file, 0666);
                    $saved = @file_put_contents($path.$file,$data,FILE_APPEND);
                    //if (!$saved)
                      //  die("Please, set write permissions to `{$path}`");
                }
            }
        }
    }

    //========================== CACHE ====================================
    /**
     * @var Exoof_Cache_Abstract Cache object
     */
    private $_cacheEngine;

    /**
     * @var int Cache expire TTL. Default is 0 - cache disabled
     */
    private $_cacheExpire = 0;

    /**
     * Sets Cache Engine Object for internal purposes
     * @param Exoof_Cache_Abstract $engine Cache Engine Object
     * @param int $expire Cache expire TTL. Default is 0 - cache disabled
     */
    public function setCache(Exoof_Cache_Abstract $engine, $expire=0) {
        $this->_cacheEngine = $engine;
        $this->_cacheExpire = $expire;
    }

    /**
     * @return boolean Returns TRUE if cache can be used and FALSE if not
     */
    private function isCacheEnabled() {
        return ($this->_cacheExpire > 0 && is_object($this->_cacheEngine))?true:false;
    }
}