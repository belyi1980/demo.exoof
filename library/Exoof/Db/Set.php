<?php
/**
 * Db Set
 *
 * @author eugene
 */
class Exoof_Db_Set {

    /**
    * @var string Name of the table
    */
    protected $_tableName;

    /**
     * @var Exoof_Db PDO connected object
     */
    protected $_pdo;

    public function __construct($tableName) {
        $this->_tableName = $tableName;
        $this->_pdo       = Exoof_Factory::factory('Db');
    }

    public function fetchAll($whereArr=array(), $count=null, $offset=null, $orderBy=null, $orderDest=null) {
        $sql = "SELECT * FROM ".$this->_tableName;
        $params = array();

        if (sizeof($whereArr)) {
            foreach ($whereArr as $field=>$value)
                $where[] = $field.' = ?';
            $sql .= " WHERE ".implode(' AND ',$where);
            $params = array_values($whereArr);
        }

        if ($offset && $count)
            $sql .= " LIMIT ".$offset.','.$count;
        elseif ($count)
            $sql .= " LIMIT ".$count;

        if ($orderBy && $orderDest)
            $sql .= " ORDER BY ".$orderBy.' '.$orderDest;
        elseif ($orderBy)
            $sql .= " ORDER BY ".$orderBy;
        return $this->_pdo->fetchAll($sql, $params);
    }

}
?>
