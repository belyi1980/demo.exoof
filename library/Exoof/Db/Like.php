<?php
/**
 * DirectField class
 *
 * @author eugene
 */
class Exoof_Db_Like {

    private $value;

    public function  __construct($value) {
        $this->value = $value;
    }

    public function  __toString() {
        return $this->value;
    }

}