<?php
/**
 * @author eugene
 */
class Exoof_Db_Between {

    private $value1;
    private $value2;

    public function  __construct($value1, $value2) {
        $this->value1 = $value1;
        $this->value2 = $value2;
    }

    public function  val1() {
        return $this->value1;
    }

    public function  val2() {
        return $this->value2;
    }


}