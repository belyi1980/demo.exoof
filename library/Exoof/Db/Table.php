<?php
/**
 * Exoof Db Table
 *
 * @author eugene
 * @abstract
 * @version 1.5 / Last Updated : 3 November / Project: MA / Author: Eugene Belyaev
 */
abstract class Exoof_Db_Table {

    const TABLE_NAME = null;
    /**
    * @var string Name of the table
    */
    protected $_tableName;

    /**
     * @var Exoof_Db PDO connected object
     */
    protected $_pdo;

    /**
     * @var array Array of table fields/columns
     */
    protected $_fields = array();

    /**
     * @var array Associative array of table values as loaded from DB
     */
    protected $_values = array();

    /**
     * @var array Associative array of modified table values to update table
     */
    protected $_modified_values = array();

    /**
     * @var mixed Name of the field of PRIMARY KEY (can be array)
     */
    protected $_primaryKey;

    /**
     * @var boolean Flag of record existance
     */
    private $_existRecord = false;

    /**
     * @var boolean Flag: Is object loaded with values or not
     */
    private $_isLoaded = false;

    /**
     * Constructor
     * @param mixed $primaryKey Primary key to find the record and to load into the object
     * @version 1.0a
     */
    public function __construct($primaryKey=null) {
        $this->_pdo = Exoof_Factory::factory('Db');
        $this->_tableName = $this->_pdo->getPrefix().$this->_tableName;

        $metaInfo = $this->_pdo->getTableMeta($this->_tableName);
        if (sizeof($metaInfo))
            foreach($metaInfo as $field=>$info) {
                $this->_fields[] = $field;
                if (isset($info['primary']) && $info['primary']==1)
                    $this->_primaryKey = $field;
            }

        //$this->rowSet = new Exoof_Db_Set($this->_tableName);

        if (is_array($primaryKey))
            throw new Exception("Multiple primary keys are not supported in current version of ".__CLASS__);
        if ($primaryKey)
            $this->load($primaryKey);
    }

    /**
     * Loads row data into object by primary key value
     * @param string $primaryKey Value of the primary key
     * @version 1.0a
     */
    public function load($primaryKey) {
        /* if ($primaryKey && is_array($primaryKey) && is_array($this->_primaryKey)) { //both are arrays - RIGHT

        } elseif ($primaryKey && is_array($primaryKey) && !is_array($this->_primaryKey)) { //one of them is not array - WRONG
            throw new Exception("Primary key for the `{$this->_tableName}` is not multiple");
        } elseif ($primaryKey && !is_array($primaryKey) && is_array($this->_primaryKey)) { //one of them is not array - WRONG
            throw new Exception("Primary key for the `{$this->_tableName}` is multiple. You should use associative array of values");
        } else*/
        if ($primaryKey && !is_array($primaryKey) && !is_array($this->_primaryKey)) { //single pk - RIGHT
            $result = $this->_pdo->fetchRow("SELECT ".implode(' , ',$this->_fields)." FROM ".$this->_tableName." WHERE ".$this->_primaryKey."=?",array($primaryKey));
            if (is_array($result)) {
                $this->_fill($result);
            }
        }
    }

    protected function _fill($array) {
        $this->fillFromArray($array);
        $this->_values = $this->_modified_values;
        $this->_modified_values = array();
        $this->_existRecord = true;
        $this->_isLoaded = true;
    }

    public function  __get($name) {
        if (in_array($name, $this->_fields)) {
            if (array_key_exists($name, $this->_modified_values))
                return $this->_modified_values[$name];
            elseif (array_key_exists($name, $this->_values))
                return $this->_values[$name];
            else
                return null;
        }
        else
            throw new Exception("Table `{$this->_tableName}` doesn`t contain column `{$name}`");
    }

    public function  __set($name,  $value) {
        if (in_array($name, $this->_fields)) {
            $this->_modified_values[$name] = $value;
        }
        else
            throw new Exception("Table `{$this->_tableName}` doesn`t contain column `{$name}`");
    }

    /**
     * Return object`s data as array
     * @return array Array represenation of object`s data
     * @version 1.0a
     */
    public function toArray() {
        return array_merge($this->_values, $this->_modified_values);
    }

    /**
     * Fill object`s data from array
     * @param array $array Array representation of object`s data
     * @version 1.0a
     */
    public function fillFromArray($array) {
        foreach ($array as $field => $value) {
            if (!is_numeric($field) && in_array($field, $this->_fields))
                $this->_modified_values[$field] = $value;
        }
    }

    public function isLoaded() {
        return $this->_isLoaded;
    }


    /**
     * Deletes row from a table
     * @return boolean Returns TRUE on success or FALSE on failure
     */
    public function delete() {
       if ($this->_existRecord) {
           try {
                return ($this->_pdo->delete($this->_tableName, array($this->_primaryKey => $this->_values[$this->_primaryKey])))?true:false;
           } catch (PDOException $e) {
                return false;
            }
       }
    }

    /**
     * Saves new object in DB or updates existing one.
     * @return boolean Returns TRUE on success.
     * @version 1.0a
     */
    public function save() {
        if ($this->_existRecord) {
            $updated = $this->update();
            if ($updated) {
                $this->_values = array_merge($this->_values, $this->_modified_values);
                $this->_modified_values = array();
                return true;
            }
        }
        else {
            $lastId = $this->insert();
            if ($lastId) {
                $this->_values = array_merge($this->_values, $this->_modified_values);
                $this->_modified_values = array();
                if (!$this->_values[$this->_primaryKey])
                    $this->_values[$this->_primaryKey] = $lastId;
                return $lastId;
            }
        }
    }

    /**
     * Updates existing row in DB
     * @return int Number of affected rows on success or NULL on failure
     * @version 1.0a
     */
    public function update() {
        return $this->_pdo->update($this->_tableName, $this->_modified_values,array($this->_primaryKey => $this->_values[$this->_primaryKey]));
    }

    /**
     * Inserts new row in DB
     * @return int Number of affected rows on success or NULL on failure
     * @version 1.0a
     */
    public function insert() {
        if (in_array('lang', $this->_fields)) {
            if (empty($this->lang))
                $this->lang = Exoof_Lang::getLang();
        }
        $return =  $this->_pdo->insert($this->_tableName, $this->_modified_values);
        if ($return)
            $this->_values[$this->_primaryKey] = $return;
        return $return;
    }

    /**
     * Finds and loads row into object by SQL-query
     * @param string $sql SQL query
     * @param array $params Array of binded params
     * @return boolean Returns TRUE on success and FALSE on failure
     */
    public function find($sql, $params) {
            $result = $this->_pdo->fetchRow($sql, $params);
            if ($result !== false && sizeof($result)) {
                $this->_fill($result);
                return true;
            } else
                return false;
    }

    /**
     * Finds and loads row into object by $field=$value
     * @param string $field Field to search by
     * @param mixed $value Value of the field to search by
     * @return boolean Returns TRUE on success and FALSE on failure
     */
    public function findBy($field, $value) {
        if (in_array($field, $this->_fields)) {
            $result = $this->_pdo->fetchRow("SELECT * FROM ".$this->_tableName." WHERE ".$field."=?", array($value));
            if ($result !== false && sizeof($result)) {
                $this->_fill($result);
                return true;
            } else
                return false;
        }
        else
            throw new Exception("Table `{$this->_tableName}` doesn`t contain column `{$name}`");
    }

    /**
     * Magic function. Do not use !
     * @param string $name
     * @param mixed $arguments
     * @deprecated
     * @ignore
     * @version 1.0a
     */
    public function  __call($name,  $arguments) {

        if (preg_match("/^findBy(.*)$/",$name, $arr)) {
            $searchBy = $arr[1];
            $searchField = null;
            foreach ($this->_fields as $field) {
                $searchByField = str_replace(' ','',ucwords(strtolower(str_replace('_', ' ', $field)))); //makes from 'rec_date_added' -> 'RecDateAdded' to compare
                if ($searchBy == $searchByField) {
                    $searchField = $field;
                    break;
                }
            }
            if ($searchField) {
                return $this->findBy($searchField, $arguments[0]);
            }
        }
        return false;
    }

    /**
     * @static
     * @return Exoof_Db DB Object
     */
    static public function getDb() {
        return Exoof_Factory::factory('Db');
    }

    /**
     * Add prefix to the table
     * @static
     * @param string $table Table name
     * @return string Table name with prefix
     */
    static public function prefixize($table) {
       $prefix = self::getDb()->getPrefix();
       $mp     = sizeof($prefix);
       if (substr($table,0,$mp) != $prefix)
        return $prefix.$table;
       else
        return $table;
    }

}