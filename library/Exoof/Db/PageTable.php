<?php
/**
 * Description of PageTable
 *
 * @author Eugene Belyaev
 * @version 1.1 / Last Updated: 27 October 2010 / Project: VEULearning / Author: Eugene Belyaev /
 */
class Exoof_Db_PageTable extends Exoof_Db_Table {

    protected $_nameColumn;

    public function save($updateRelated=true) {
        $this->lang = Exoof_Lang::getLang();
        $updated = parent::save();
        if ($updated && $updateRelated && $this->page_id) {
            $page = new Pages($this->page_id);
            if ($page->isLoaded()) {
                $column = $this->_nameColumn;
                $page->page_title = $this->$column;
                $page->page_is_linked = 1;
                $page->save();
            }
        }
        return $updated;
    }

    public function delete() {
        $page_id = $this->page_id;
        $deleted = parent::delete();
        if ($deleted) {
            $p = new Pages($page_id);
            $p->page_is_linked = 0;
            $p->save();
        }
        return $deleted;
    }

    static public function search() {
        throw new Exception("Method `".__CLASS__."::search` should be overrided");
    }
}