<?php
/**
 * DirectField class
 *
 * @author eugene
 * @version 1.1
 */
class Exoof_Db_DirectField {

    private $value;
    private $operator;

    public function  __construct($operator=null,$value) {
        $this->value = $value;
        $this->operator = $operator;
    }

    public function  __toString() {
        return (string)$this->value;
    }

    public function getOperator() {
        return $this->operator;
    }

}