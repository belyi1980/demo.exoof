<?php
/**
 * Logger class
 *
 * For creating log files
 * @author Eugene Belyaev <eugeny.belyi@veusourcing.com>
 * @version 1.0 / Last Updated: 30 November / Project: DEMO / Author: Eugene Belyaev
 */

class Exoof_Tools_Logger {

    /**
     * Write log message to a file
     * @param string $filename Filename, extension will be changed
     * @param string $message Formatted log message (use sprintf syntax)
     * @param array $params Params for formatted message
     * @return boolean True or False
     * @static
     */
    static public function log($filename, $message, $params=array()) {
        $logPath        = realpath(Exoof_Factory::factory('Config')->paths->logsPath).'/';
        $logFileName    = basename($filename).strftime("_%d-%m-%Y").".log";

        $message0 = strftime("[%T]");
        $message1 = "\t[".$_SERVER['REMOTE_ADDR']."]\t";
        $message2 = vsprintf($message, $params);
        $bytes = @file_put_contents($logPath.$logFileName, $message0.$message1.$message2."\n", FILE_APPEND);
        return ($bytes > 0)?true:false;
    }

}
