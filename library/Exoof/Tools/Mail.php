<?php

/**
 * Sending Mail Class
 *
 * @author eugene
 * @version 1.2 Last Updated : 8 July 2010
 */
class Exoof_Tools_Mail {
    /*
     * Sends site autoformatted mail
     *
     * @author Eugene Belyaev
     * @static
     * @param mixed to Recipient e-mail and name (array) or just e-mail
     * @param string subjectLangKey Key to localized string (lang ini-files)
     * @param string textLangKey Key to localized string (lang ini-files)
     * @param mixed replaces Array with aliases to replace in mail text
     * @return void
     */
    /*
      public static function sendAutoMail($to, $subjectLangKey, $textLangKey, $replaces=null) {

      $translator = Zend_Registry::get('Zend_Translate');

      $host = str_replace('www.','',$_SERVER['HTTP_HOST']);

      $subject = $translator->translate($subjectLangKey);

      $mailText = $translator->translate($textLangKey);
      if (sizeof($replaces) && is_array($replaces))
      $mailText = str_replace(array_keys($replaces),array_values($replaces),$mailText);

      self::sendMail($to, $subject, $mailText);
      } */

    /*
     * Sends site autoformatted mail
     *
     * @author Eugene Belyaev
     * @static
     * @param mixed to Recipient e-mail and name (array) or just e-mail
     * @param string subject Subject Text
     * @param string text Message Text
     * @param array keys Array of dynamic fields
     * @return void
     */

    public static function sendMail($toVar, $subject, $text, $keys=array(), $from=array()) {
        $headers = array();
        if (is_array($toVar)) {
            $toFinal = '';
            foreach ($toVar as $val) {
                if (preg_match("/^.*@+.*.?([a-z]{0,6})$/", $val))
                    $to = $val;
                else
                    $toFinal .= $val . ' ';
            }
            $toFinal .= "<" . $to . ">";
        } else
            $to = $toFinal = $toVar;

        $obj = new MailBlackList($to);
        if ($obj->isLoaded()) {
            throw new Exoof_Exception_Security('E-mail is in the blacklist !');
        }

        $translator = Exoof_Translate::getInstance();
        $config = Exoof_Factory::factory('Config');

        $host = str_replace('www.', '', $_SERVER['HTTP_HOST']);

        $subjectF = strtoupper($host) . ': ';
        $subjectF .= str_replace(array('\n'), array(chr(10)), $subject);

        $signature = $translator->loadFile('mail_signature.txt');
        if (empty($signature)) {
            $signature = "\n\n" . str_repeat('_', 80) . "\n";
            $signature .= ' ' . strtoupper($host) . ' ';
        }

        $text .= $signature;
        $mailText = str_replace(array('\n', '\t'), array(chr(10), chr(9)), $text);
        $keys['__to__'] = $to;
        $keys['__host__'] = strtoupper($host);
        $keys['__link__'] = Exoof_View::getInstance()->baseHref() . $translator->getCurrentLang() . '/';
        $keys['__crypt__'] = Exoof_Tools_Crypt::Encrypt($to);
        $akeys = array_map(array('self', 'chKeys'), array_keys($keys));
        $mailText = str_replace($akeys, array_values($keys), $mailText);

        if (is_array($from) && sizeof($from)) {
            $headers['From'] = '';
            $fromEmail = '';
            foreach ($from as $val) {
                if (preg_match("/^.*@+.*.?([a-z]{0,6})$/", $val))
                    $fromEmail = $val;
                else
                    $headers['From'] .= $val . ' ';
            }
            $headers['From'] .= "<" . $fromEmail . ">";
        }
        elseif (!is_array($from) && strlen($from)) {
            $headers['From'] = "<" . $from . ">";
        } else {
            $fromName = (isset($config->mail->fromName)) ? $config->mail->fromName : strtoupper($host);
            $headers['From'] = $fromName;
            $headers['From'] .= '<' . ((isset($config->mail->fromEmail)) ? $config->mail->fromEmail : $_SERVER['SERVER_ADMIN']);
            $headers['From'] .= ( (substr($headers['From'], -1, 1) == '@') ? $host : '') . '>';
        }

        $hash = strtoupper(sha1(md5(time())));
        $headers['Content-Type'] = "multipart/alternative; boundary={$hash}";

        $headersTextArr = array();
        foreach ($headers as $header => $hval)
            $headersTextArr[] = $header . ": " . $hval;

        // Text Version
        $bodyText = "--" . $hash . "\n";
        $bodyText .= "Content-Type: text/plain; charset=\"utf-8\"\n\n";
        $bodyText .= strip_tags($mailText);
        $bodyText .= "\n\n--" . $hash . "\n";

        //HTML Version
        $bodyText .= "Content-Type: text/html; charset=\"utf-8\"\n";
        $bodyText .= "Content-Transfer-Encoding: quoted-printable\n\n";
        $bodyText .= nl2br(str_replace('\t', '<dd>', $mailText));
        $bodyText .= "\n\n--" . $hash . "--\n";
        return mail($toFinal, $subjectF, $bodyText, implode("\n", $headersTextArr));
    }

    static public function chKeys($element) {
        return '%' . $element . '%';
    }

}
?>