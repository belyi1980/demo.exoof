<?php
class Exoof_Tools_Crypt {
/**
 * XOR encrypts a given string with a given key phrase.
 *
 * @param    string    $InputString    Input string
 * @param    string    $KeyPhrase      Key phrase
 * @return    string    Encrypted string   
 */
    static public function Encryption($InputString, $KeyPhrase){

        $KeyPhraseLength = strlen($KeyPhrase);

        // Loop trough input string
        for ($i = 0; $i < strlen($InputString); $i++){
            // Get key phrase character position
            $rPos = $i % $KeyPhraseLength;
            // Magic happens here:
            $r = ord($InputString[$i]) ^ ord($KeyPhrase[$rPos]);

            // Replace characters
            $InputString[$i] = chr($r);
        }

        return $InputString;
    }

    // Helper functions, using base64 to
    // create readable encrypted texts:

    static public function Encrypt($InputString, $KeyPhrase='MySimPleCode'){
        $InputString = self::Encryption($InputString, $KeyPhrase);
        $InputString = base64_encode($InputString);
        return $InputString;
    }

    static public function Decrypt($InputString, $KeyPhrase='MySimPleCode'){
        $InputString = base64_decode($InputString);
        $InputString = self::Encryption($InputString, $KeyPhrase);
        return $InputString;
    }
}
?>
