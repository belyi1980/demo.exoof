<?
define("THUMB_FIXED",0);
define("THUMB_PROPORTIONAL",1);
define("THUMB_CUT_CENTER",2);
define("THUMB_CUT_TOP_CENTER",3);
define("THUMB_CUT_TOP_LEFT",4);
define("THUMB_4_3",5);
define("THUMB_16_9",6);

/**
 * Images upload and resize method
 */
class Exoof_Tools_Image {
/**
 * Returns image extension
 *
 * @param string $filename FileName
 * @return string Extension
 */
    static public function getImageExtension($filename) {
        if (!file_exists($filename) || !filesize($filename))
            return false;

        $f = getimagesize($filename);
        if ($f[2]>0 && $f[2]<4) {
            switch ($f[2]) {
                case 1 : $ext = 'gif'; break;
                case 2 : $ext = 'jpg'; break;
                case 3 : $ext = 'png'; break;
                case 4 : $ext = 'swf'; break;
                case 6 : $ext = 'bmp'; break;
            }
            return $ext;
        }
        else
            return false;
    }

    function makeUploadPhoto($var_name, $dir, $makeThumb=true, $thumbOptions=THUMB_PROPORTIONAL ,$sizeW=150, $sizeH=120, $file_name=null, $file_tmp=null) {
        if (file_exists($file_tmp))
            $FILE = $file_tmp;
        else
            $FILE = $_FILES[$var_name]['tmp_name'];

        if (!$file_name)
            $file_name = substr(strtoupper(md5(time())),0,23);

        if (@filesize($FILE)) {
            $ext = self::getImageExtension($FILE);
        }
        else
            return false;

        if (!$ext)
            return false;
        else {
            $new_file = $file_name.".".$ext;

            $result = @move_uploaded_file($FILE, realpath($dir)."/".$new_file);
            @chmod(realpath($dir)."/".$new_file, 0644);
            if (!$makeThumb)
                return $new_file;

            if ($result) {

                $thumbnail = self::makeThumbnail(realpath($dir)."/".$new_file,$dir,$sizeW, $sizeH, $thumbOptions);
                return array($new_file,basename($thumbnail));
            }
            else
                return false;
        }
    }

    /**
     * Generates thumbnail from image
     *
     * @param string $file Path to image
     * @param string $cache_dir Directory to save thumbnail
     * @param int $nW Maximum width of thumbnail image
     * @param int $nH Maximum height of thumbnail image
     * @param defined $thumbOptions THUMB_FIXED, THUMB_PROPORTIONAL, THUMB_CUT_CENTER, THUMB_CUT_TOP_CENTER, THUMB_CUT_TOP_LEFT
     * @return string Path to generated thumbnail image
     */
    function makeThumbnail($file, $cache_dir=EXOOF_UPLOAD_DIR, $nW=152, $nH=114, $thumbOptions=THUMB_PROPORTIONAL) {
        $img_arr = getimagesize($file);
        $ext		 = self::getImageExtension($file);
        $file_name       = basename($file);
        $new_file_name   = 'thumb_'.$nW.'x'.$nH.'_t'.$thumbOptions.'_'.$file_name;
        $th_file	 = $cache_dir.$new_file_name;

        $mW=$img_arr[0];$mH=$img_arr[1];
        if ($mW<$nW) $nH=$mH;
        if ($mH<$nH) $nH=$mH;

        @ini_set("memory_limit","24M");

        switch($ext) {
            case 'jpg' : $im = imagecreatefromjpeg($file); break;
            case 'gif' : $im = imagecreatefromgif($file); break;
            case 'png' : $im = imagecreatefrompng($file); break;
        }

        if ($im) {
            $w = imagesx($im);
            $h = imagesy($im);

            //нужно проверить какая из сторон пропорционально больше максимально допустимых nW и nH
            $xW = $w/$nW; // соотношение X
            $xY = $h/$nH; // соотношение Y

            $xD  = ($xW > $xY)?$xW:$xY;//коэффициент1 - уменьшаем по той стороне, которая "не влезет" в наши максим. координаты

            $xD1 = ($xW > $xY)?$xY:$xW;//коэффициент2 - уменьшаем по той стороне, которая влезает в наши максим. координаты. Другая при этом будет вылезать и ее надо будет обрезать
            $wS = round($w/$xD1);
            $hS = round($h/$xD1);

            $im2 = imagecreatetruecolor($wS, $hS);
            if(($ext == 'gif') || ($ext=='png')) { // TRANSPARENCY PATCH
                  imagealphablending($im2, false);imagesavealpha($im2,true);$transparent = imagecolorallocatealpha($im2, 255, 255, 255, 127);
                  imagefilledrectangle($im2, 0, 0, $wS, $hS, $transparent); }

            imagecopyresampled($im2, $im, 0, 0, 0, 0, $wS, $hS, $w, $h);

            switch($thumbOptions) {
                case THUMB_FIXED:
                    $w1 = $nW;
                    $h1 = $nH;

                    $im1 = @imagecreatetruecolor($w1, $h1);

                    if(($ext == 'gif') || ($ext=='png')) { // TRANSPARENCY PATCH
                          imagealphablending($im1, false);imagesavealpha($im1,true);$transparent = imagecolorallocatealpha($im1, 255, 255, 255, 127);
                          imagefilledrectangle($im1, 0, 0, $w1, $h1, $transparent); }

                    imagecopyresampled($im1, $im, 0, 0, 0, 0, $w1, $h1, $w, $h);
                    break;

                case THUMB_CUT_CENTER:
                    $cutX    = round(($wS - $nW)/2);
                    $cutY    = round(($hS - $nH)/2);
                    $im1 = imagecreatetruecolor($nW, $nH);
                    imagecopyresampled($im1, $im2, 0, 0, $cutX, $cutY, $nW, $nH, $nW, $nH);
                    break;

                case THUMB_CUT_TOP_CENTER:
                    $cutX    = round(($wS - $nW)/2);
                    $cutY    = 0;
                    $im1 = imagecreatetruecolor($nW, $nH);
                    imagecopyresampled($im1, $im2, 0, 0, $cutX, $cutY, $nW, $nH, $nW, $nH);
                    break;

                case THUMB_CUT_TOP_LEFT:
                    $cutX    = 0;
                    $cutY    = 0;
                    $im1 = imagecreatetruecolor($nW, $nH);
                    imagecopyresampled($im1, $im2, 0, 0, $cutX, $cutY, $nW, $nH, $nW, $nH);
                    break;

                case THUMB_4_3:
                    if ($w >= $h) {
                        $w1 = $nW;
                        $h1 = $nW * 3/4;
                    } else {
                        $h1 = $nW;
                        $w1 = $nW * 3/4;
                    }

                    $im1 = imagecreatetruecolor($w1, $h1);
                    if(($ext == 'gif') || ($ext=='png')) { // TRANSPARENCY PATCH
                          imagealphablending($im1, false);imagesavealpha($im1,true);$transparent = imagecolorallocatealpha($im1, 255, 255, 255, 127);
                          imagefilledrectangle($im1, 0, 0, $w1, $h1, $transparent); }

                    imagecopyresampled($im1, $im, 0, 0, 0, 0, $w1, $h1, $w, $h);
                    break;

                case THUMB_16_9:
                    if ($w >= $h) {
                        $w1 = $nW;
                        $h1 = $nW * 9/16;
                    } else {
                        $h1 = $nW;
                        $w1 = $nW * 9/16;
                    }

                    $im1 = imagecreatetruecolor($w1, $h1);
                    if(($ext == 'gif') || ($ext=='png')) { // TRANSPARENCY PATCH
                          imagealphablending($im1, false);imagesavealpha($im1,true);$transparent = imagecolorallocatealpha($im1, 255, 255, 255, 127);
                          imagefilledrectangle($im1, 0, 0, $w1, $h1, $transparent); }

                    imagecopyresampled($im1, $im, 0, 0, 0, 0, $w1, $h1, $w, $h);
                    break;

                default:
                case THUMB_PROPORTIONAL:
                    $w1 = round($w/$xD);
                    $h1 = round($h/$xD);

                    $im1 = @imagecreatetruecolor($w1, $h1);
                    if(($ext == 'gif') || ($ext=='png')) { // TRANSPARENCY PATCH
                          imagealphablending($im1, false);imagesavealpha($im1,true);$transparent = imagecolorallocatealpha($im1, 255, 255, 255, 127);
                          imagefilledrectangle($im1, 0, 0, $w1, $h1, $transparent); }

                    imagecopyresampled($im1, $im, 0, 0, 0, 0, $w1, $h1, $w, $h);
                    break;

            }

        }
        switch($ext) {
            case 'jpg': imagejpeg($im1,$th_file,100); break;
            case 'gif': imagegif($im1,$th_file); break;
            case 'png':
                    @imagealphablending( $im1, false );
                    imagesavealpha($im1, true);
                    imagepng($im1,$th_file,0,null);
                break;
        }
        @imagedestroy($im);
        @imagedestroy($im1);
        @imagedestroy($im2);
        return $th_file;

    }

    /**
     * returns EXIF data
     *
     * @param string $file FileName
     * @return array
     */
    function getEXIFData($file) {
        return exif_read_data($file);
    }
}
?>