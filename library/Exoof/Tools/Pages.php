<?php
class Exoof_Tools_Pages {

    private $pages_arr;
    private $pages_arr_new;
    private $mp;
    private $activePath;

    public function __construct() {
        $this->activePath = Exoof_Lang::getLangUrl().Exoof_Registry::get('activePath');//Zend_Registry::get('URL');
    }

    public function getAllParents() {

                $cache = Exoof_Factory::factory('Cache')->engine;

                @set_time_limit(90);//1.5min
                $this->pages_arr = null;//$cache->fetch('ExoofStructure');
                if (!$this->pages_arr) {

                    $db     = Exoof_Factory::factory('Db');
                    //$db->setFetchMode(Zend_Db::FETCH_ASSOC);
                    //$prefix = Zend_Registry::get('db_prefix');
                    $tab1 = $db->getPrefix().'pages';
                    $tab2 = $db->getPrefix().'page_types';
                     //fetch($tableOrSql, $whereArr=array(), $count=null, $offset=null, $orderBy=null, $orderDest=null) {
                    $this->pages_arr = $db->fetchAll(
                        "SELECT t1.page_id,t1.lang,t1.parent_page_id,t1.sort_code,t1.page_path,t1.page_url,t1.page_title,t2.ptype_name 
                            FROM {$tab1} AS t1
                            INNER JOIN {$tab2} as t2
                                USING(ptype_id)
                            WHERE lang=?
                            ORDER BY sort_code ASC",
                        array(Exoof_Lang::getLang()));

                    $cache->store('ExoofStructure',$this->pages_arr,(int)Exoof_Factory::factory('Config')->cache->structureCacheExpire);
                }
                $this->mp   = sizeof($this->pages_arr);
                for($p=0;$p<$this->mp;$p++) {
                    if ($this->pages_arr[$p]['parent_page_id']) {
                        continue;
                    }
                    else
                    {
                        $key   = $this->pages_arr[$p]['page_id'];

                        //$i = sizeof($this->pages_arr_new);
                        //$i = ($i>0)?$i-1:0;
                        $this->pages_arr_new[$key]['label'] = stripslashes($this->pages_arr[$p]['page_title']);
                        $this->pages_arr_new[$key]['uri']   = $this->makePath($this->pages_arr[$p]['page_path'],$this->pages_arr[$p]['page_url']);
                        $this->pages_arr_new[$key]['ptype']  = $this->pages_arr[$p]['ptype_name'];
                        $this->pages_arr_new[$key]['page_id']  = $key;
                        $this->pages_arr_new[$key]['level']  = sizeof(explode('-',$this->pages_arr[$p]['sort_code']));
                        $this->pages_arr_new[$key]['psort']  = $this->pages_arr[$p]['sort_code'];
                        if ($this->pages_arr_new[$key]['uri'] == $this->activePath || ($this->pages_arr_new[$key]['uri']=='') || strpos($this->activePath,$this->pages_arr_new[$key]['uri'])===0)
                            $this->pages_arr_new[$key]['active'] = true;

                        $this->findParents($key, $this->pages_arr_new[$key]);
                    }
        }
        return $this->getArray();
    }

    protected function findParents($page_id , &$pages_new) {
        for($p=0;$p<$this->mp;$p++) {
            if ($this->pages_arr[$p]['parent_page_id'] == $page_id) {
                $key   = $this->pages_arr[$p]['page_id'];
                $title = $this->pages_arr[$p]['page_title'];

                //                $i = sizeof($this->pages_arr_new);
                //$i = ($i>0)?$i-1:0;

                $pages_new['pages'][$key]['page_id']  = $key;
                $pages_new['pages'][$key]['label'] = stripslashes($this->pages_arr[$p]['page_title']);
                $pages_new['pages'][$key]['uri'] = $this->makePath($this->pages_arr[$p]['page_path'],$this->pages_arr[$p]['page_url']);
                $pages_new['pages'][$key]['ptype'] = $this->pages_arr[$p]['ptype_name'];
                $pages_new['pages'][$key]['psort'] = $this->pages_arr[$p]['sort_code'];
                $pages_new['pages'][$key]['level']  = sizeof(explode('-',$this->pages_arr[$p]['sort_code']));
                //((!empty($this->pages_arr[$p]['page_path']))?'/':'').$this->pages_arr[$p]['page_path']."/".$this->pages_arr[$p]['page_url'];
                if ($pages_new['pages'][$key]['uri'] == $this->activePath || strpos($this->activePath,$pages_new['pages'][$key]['uri'])===0 || $pages_new['pages'][$key]['uri']=='')
                    $pages_new['pages'][$key]['active'] = true;

                $this->findParents($key, &$pages_new['pages'][$key]);
            }
        }
    }	

    public function getArray() {
        static $arr = null;
        if ($arr == null)
            $arr = $this->pages_arr_new;
        return $arr;
    }

    private function makePath($path, $page) {


        //Remove LANG prefix from path if not multilanguage site
        /*if ($config->multilang->multiple!='yes' && preg_match('/^[a-z]{2}\/+/',$path))
            $path = preg_replace('/^([a-z]{2}\/+)/','',$path);
        if ($config->multilang->multiple!='yes' && preg_match('/^[a-z]{2}\/+/',$page))
            $page = preg_replace('/^([a-z]{2})/','',$page);
        */
        if (strlen($path))
            return Exoof_Lang::getLangUrl().$path.$page;
        elseif (strlen($page))
            return Exoof_Lang::getLangUrl().$page;
        else
            return Exoof_Lang::getLangUrl().'';
    }
} 
?>
