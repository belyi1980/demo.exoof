<?php
/**
 * Description of NotFound
 *
 * @author eugene
 */
class Exoof_Exception_NotFound extends Exception {
    

    public function __construct ($message='Page `%page%` not found', $code=0) {

        $header   = Exoof_Factory::factory('Translate')->translate('Navigation Error');
        $message1 = $header. ': '.Exoof_Factory::factory('Translate')->translate($message);
        $message1 = str_replace('%page%',@$_GET['__path'], $message1);

        parent::__construct($message1, $code);
    }
}
?>
