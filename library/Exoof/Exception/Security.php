<?php
/**
 * Security Exception
 *
 * @author eugene
 */
class Exoof_Exception_Security extends Exception {
    

    public function __construct ($message=null, $code=0) {

        $header   = Exoof_Factory::factory('Translate')->translate('Security Error');
        $message1 = $header. ': '.Exoof_Factory::factory('Translate')->translate($message);

        parent::__construct($message1, $code);
    }
}
?>
