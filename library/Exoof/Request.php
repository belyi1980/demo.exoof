<?php
/**
 * HTTP Request Class
 *
 * @author eugene
 * @version 1.6 / Last Updated: 22 November 2010 / Project : DEMO / Author: Eugene Belyaev
 */
class Exoof_Request {

    private $_params = array();
    private $_urlParams = array();

    public function __construct($addArray) {
        $pathParams = explode('/',trim((isset($_GET['__path'])?$_GET['__path']:''),'/'));
        if (Exoof_Lang::getLangUrl() == null)
            $this->_urlParams = array_slice($pathParams, 2);
        else
            $this->_urlParams = array_slice($pathParams, 3);

        $this->_params = array_merge($_GET, $_POST, $_REQUEST);
        if (is_array($addArray))
            $this->_params = array_merge($this->_params, $addArray);
    }

    public function  __get($name) {
        return $this->getParam($name, null);
    }

    public function getParam($key, $default=null) {
        if ($this->isParamExists($key))
            return $this->_params[$key];
        elseif ($default != null)
            return $default;
        else
            return null;
    }

    public function getDateParam($fieldName) {
        $dateP = $this->getParam($fieldName);

        return @mktime(0,0,0,$dateP['month'],$dateP['day'],$dateP['year']);
    }

    public function getUrlParam($position = 0) {
        if (isset($this->_urlParams[$position]))
            return $this->_urlParams[$position];
        elseif ($position == -1) {
            $pathParams = explode('/',trim((isset($_GET['__path'])?$_GET['__path']:''),'/'));
            return array_pop($pathParams);
        }
         else
             return null;
    }

    public function isParamExists($key) {
        if (array_key_exists($key, $this->_params))
            return true;
        else
            return false;
    }

    public function addParam($key, $value) {
        $this->_params[$key] = $value;
    }

    public function getAllParams() {
        return $this->_params;
    }

    public function isPost() {
        return ($_SERVER['REQUEST_METHOD']=='POST')?true:false;
    }

    public function isFileUploaded($paramName, $idx=null) {
        if ($idx)
            $full = $_FILES[$paramName]['tmp_name'][$idx];
        else
            $full = $_FILES[$paramName]['tmp_name'];
        return (@filesize($full)?true:false);
    }

    public function isCaptchaValid($fieldName='captcha') {
        if ($this->getParam($fieldName) == Exoof_Session::get('CaptchaWord'))
            return true;
        else
            return false;
    }

    public function isAjaxRequest() {
        $nativeAjax     = (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH']=="XMLHttpRequest");
        $iframeUpload   = $this->isParamExists('__isAjaxUsed');
        return ($nativeAjax || $iframeUpload);
    }

    public function getFile($paramName, $idx=null) {
        if ($idx)
            return
                array(
                    'name' => @$_FILES[$paramName]['name'][$idx],
                    'tmp_name' => @$_FILES[$paramName]['tmp_name'][$idx],
                    'size' => @$_FILES[$paramName]['size'][$idx],
                    'type' => @$_FILES[$paramName]['type'][$idx],
                    'error' => @$_FILES[$paramName]['errors'][$idx]
                    );
        else
            return $_FILES[$paramName];
    }

}