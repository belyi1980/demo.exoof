<?php
/**
 * Exoof_Bootstrap
 *
 * @author Eugene Belyaev
 * @version 1.17 / Last Updated : 17 December 2010 / Project: Kings / Author: Eugene Belyaev
 */
class Exoof_Bootstrap {

    /**
     * @var Bootstrap
     */
    private static $instance = null;
    /**
     * @var Exoof_Config
     */
    private $config;
    /**
     * @var Exoof_Db
     */
    private $db;
    /**
     * @var Exoof_Translate
     */
    private $translation;
    /**
     * @var array Array of controllers/actions to run
     */
    private $controllers;
    /**
     * @var boolean Flag: admin requested or not
     */
    private $_adminRequested = false;
    /**
     * @var array Array of request URL params
     */
    private $_params = array();

    /*
     * Constructor is locked due to Singleton pattern
     */
    private function __construct() {
        //throw new Exception('Can`t create instance of '.__CLASS__.'.Use '.__CLASS__.'::getInstance()');
    }

    /* Returns Bootstrap object
     *
     * @return Exoof_Bootstrap
     */

    public static function getInstance() {
        if (!is_object(self::$instance) || !(self::$instance instanceof Exoof_Bootstrap))
            self::$instance = new Exoof_Bootstrap();
        return self::$instance;
    }

    public function init($config) {
        //init paths
        set_include_path(
                realpath($config['paths']['libraryPath']) . PATH_SEPARATOR .
                realpath($config['paths']['modelPath']) . PATH_SEPARATOR .
                get_include_path()
        );
        spl_autoload_register(array($this, 'autoload'));
        $this->config = new Exoof_Config($config);
        Exoof_Factory::factory('Config', $this->config);

        //debug($this->config);exit;
        if ($this->config->session->session_autostart == 'yes')
            Exoof_Session::start();

        $this->_initExceptionHandler();
        $this->_initErrorHandler();
        $this->_initCache();
        $this->_initDb();
        $this->_initTranslation();
        $this->_initRoute();
        //@TODO $this->_initErrorHandler();
    }

    private function _initCache() {
        $cache = Exoof_Cache::getInstance();
        Exoof_Factory::factory('Cache', $cache);
    }

    private function _initDb() {
        if ($this->config->database->type == 'sqlite')
            $db = new Exoof_Db_Sqlite($this->config->database->dbname, $this->config->database->prefix);
        else
            $db = new Exoof_Db($this->config->database->type,
                            $this->config->database->host,
                            $this->config->database->username,
                            $this->config->database->password,
                            $this->config->database->dbname,
                            $this->config->database->charset,
                            $this->config->database->prefix);

        $cache = Exoof_Factory::factory('Cache');
        $db->setCache($cache->engine, (int) $this->config->cache->dbCacheExpire);

        Exoof_Factory::factory('Db', $db);
    }

    private function _initTranslation() {

        $this->translation = Exoof_Translate::getInstance();
        $this->translation->init($this->config->paths->langsPath, ($this->config->multilang->multiple == 'yes'));
        $this->translation->setLogUntranslated($this->config->multilang->logUntranslated == 'yes');
        $this->translation->setCurrentLang($this->config->multilang->default);
        $cache = Exoof_Factory::factory('Cache');
        $this->translation->setCache($cache->engine, (int) $this->config->cache->translateCacheExpire);

        //TODO Remove factory dependence
        Exoof_Factory::factory('Translate', $this->translation);
    }

    private function _initExceptionHandler() {
        if ($this->config->debug->handleErrors == 'yes')
            set_exception_handler(array($this, 'handleException'));
    }

    private function _initErrorHandler() {
        @ini_set('display_errors', 'On');
        if ($this->config->debug->handleErrors == 'yes')
                $errH = new Exoof_Error();
    }

    public function handleException(Exception $exception) {
        $_GET['__path'] = Exoof_Lang::getLangUrl() . 'error/';
        Exoof_Factory::factory('Exception', $exception);
        $this->_initRoute();
        try {
            $this->run();
        } catch (Exception $e) {
            echo "Error Handling Error : " . $e->getMessage();
        }
    }

    private function _initRoute() {
        $requestUrl = trim((isset($_GET['__path']) ? $_GET['__path'] : ''), '/');
        $this->_params = explode('/', $requestUrl);
        $requestUrl = implode('/', $this->_params);

        //checking if admin loading request
        // ADMIN REQUEST
        if (isset($this->_params[0]) && $this->_params[0] == $this->config->admin->adminURL) {
            $this->_adminRequested = true;
            Exoof_Factory::factory('AdminLoaded', true);
            restore_exception_handler();
            $this->_validateAdministrator();
            Exoof_Tools_Logger::log("admin", "URL: `%s` , METHOD: `%s`",array($requestUrl,$_SERVER['REQUEST_METHOD']));
            $this->controllers[] = array(
                'controller' => (string) (isset($this->_params[1]) && !empty($this->_params[1])) ? $this->_params[1] : 'index',
                'action' => (string) (isset($this->_params[2]) && !empty($this->_params[2])) ? $this->_params[2] : 'index',
                'container' => 'index',
                'layout' => 'main.phtml'
            );
            //check admin permissions
            $admin = Exoof_Session::get('admin');
            $permissions = @array_map('strtolower',@array_keys(unserialize($admin['adm_sections'])));
            $admRequest = strtolower(implode('/',array_slice($this->_params,1)));
            if (!$admin['adm_is_superuser'] && $this->controllers[0]['controller'] != 'index') {
                if (sizeof($permissions)) {
                    $access = false;
                    foreach ($permissions as $url) {
                        if (strstr($admRequest,$url)) {
                            $access = true;
                            break;
                        }
                    }
                    if (!$access) {
                        Exoof_Tools_Logger::log("admin", "Admin `%s` permission denied for `%s`",array($admin['adm_login'], $admRequest));
                        die('<h1>Access denied</h1>');
                    }
                    
                }
            }
            array_shift($this->_params);
            @session_start();
            $_SESSION['adminAuthorized'] = true;
            //FRONTEND REQUEST
        } else {

            //checking for LANG in request url
            if ($this->config->multilang->multiple == 'yes') {
                $langDetected = false;
                $allLangs = $this->translation->getLangs();

                //SITUATION 1: lang prefix should exists
                if ($this->config->multilang->defaultUrlPrefix == 'yes') {
                    if (sizeof($allLangs))
                        foreach ($allLangs as $lang) {
                            if (@$this->_params[0] == $lang) {//if first url chunk is lang
                                $this->translation->setCurrentLang($lang, true); //it means to switch to $lang language
                                array_shift($this->_params); //shift lang url chunk
                                $langDetected = true;
                                $requestUrl = implode('/', $this->_params);
                                break;
                            }
                        }

                    if (!$langDetected) { //if lang is not detected or doesn`t exist translations - redirect to default lang
                        $baseHref = (isset($_SERVER['HTTPS'])) ? 'https://' : 'http://';
                        $baseHref .= $_SERVER['SERVER_NAME'];
                        $baseHref .= ( $_SERVER['SERVER_PORT'] != '80' && $_SERVER['SERVER_PORT'] != '443') ? ':' . $_SERVER['SERVER_PORT'] : null;
                        if (strlen(dirname($_SERVER['PHP_SELF'])) > 1)
                            $baseHref .= dirname($_SERVER['PHP_SELF']);
                        $baseHref .= '/';

                        header('Location: ' . $baseHref . $this->translation->getCurrentLang() . '/'); //Exoof_Lang::getLangUrl());
                        exit;
                    }
                } else {
                    //SITUATION 2: lang prefix can be empty
                    if ($this->config->multilang->default != 'auto') { //
                        $this->translation->setCurrentLang($this->config->multilang->default, true);
                        $langDetected = true;
                    } else {
                        die("Don`t use default='auto' and defaultUrlPrefix= 'no'");
                        //$this->translation->setCurrentLang($this->config->multilang->default);
                        //throw new Exoof_Exception_NotFound($requestUrl);
                    }
                }



                if (!$langDetected && strlen($requestUrl) > 1) { //if no lang chunk in url - it means that page doesn`t exist
                    if ($this->config->multilang->default != 'auto') {
                        $this->translation->setCurrentLang($this->config->multilang->default);
                        $langDetected = true;
                    } else {

                        $this->translation->setCurrentLang($this->config->multilang->default);
                        throw new Exoof_Exception_NotFound($requestUrl);
                    }
                }
            }

            $route = simplexml_load_file('./config/site.xml');
            $routeSet = null;

            // Loading rewrite rules
            $rewrited = false;
            foreach ($route->rule as $rule) {
                $mask = "/" . addcslashes($rule['mask'], "/") . "/";
                if (preg_match($mask, $requestUrl, $matches)) {
                    $requestUrl = trim(preg_replace($mask, $rule['dest'], $matches[0]), '/');
//                        debug($requestUrl);
                    $rewrited = true;
                    break;
                }
            }

            if ($rewrited) { //resort params
                $ru = explode('/', $requestUrl);
                $revParams = array_merge($ru, array_values(array_diff($this->_params, $ru)));
                $this->_params = $revParams;
            }

            //- 1 - Trying to load route by URL from config
            foreach ($route->item as $item) {
                //print_r($item);
                if (isset($item['url']) && empty($item['url']) && empty($requestUrl)) { // empty request
                    //found empty request and empty route entry point
                    $routeSet = $item;
                    break;
                } elseif (isset($item['url']) && !empty($item['url']) && preg_match("/^" . addcslashes($item['url'], '/') . "/", $requestUrl)) {
                    //found matched route
                    $routeSet = $item;
                    break;
                }
            }

            //- 2 - Trying to load route by page type
            if (!$routeSet) { //checking for page types
                $obj = new Pages();

                $_params = explode('/', $requestUrl); //can`t use $this->_params here due to 'rewrite rules'. Must check rewrited url
                $url = (@$_params[count($_params) - 1]) ? $_params[count($_params) - 1] : '';

                $path = implode('/', array_slice($_params, 0, -1));

                if (strlen($url))
                    $url = trim($url, '/') . '/';
                if (strlen($path))
                    $path = trim($path, '/') . '/';

                if ($obj->findByFullURLAndLang($url, $path, $this->translation->getCurrentLang()) && $obj->isLoaded()) {
                    $ptype = new PageType($obj->ptype_id);
                    if ($ptype->isLoaded()) {
                        foreach ($route->item as $item) {
                            if (isset($item['type']) && $item['type'] == $ptype->ptype_name) {
                                $routeSet = $item;
                                $this->page = $obj;
                                break;
                            }
                        }
                    }
                }
            }

            //- 3 - Trying to load route by params
            if (!$routeSet) {
                $this->controllers[] = array(
                    'controller' => (string) (isset($this->_params[0]) && !empty($this->_params[0])) ? $this->_params[0] : 'index',
                    'action' => (string) (isset($this->_params[1]) && !empty($this->_params[1])) ? $this->_params[1] : 'index',
                    'container' => 'index',
                    'layout' => 'main.phtml'
                );
            } else {
                $this->controllers = array();
                foreach ($routeSet->controller as $controller) {
                    $this->controllers[] = array(
                        'controller' => (string) $controller['name'],
                        'action' => (string) (isset($controller['action']) ? $controller['action'] : '@auto@'), //TODO
                        'container' => (string) (isset($controller['container']) ? $controller['container'] : $controller['name']),
                        'layout' => (string) (isset($routeSet['layout']) ? $routeSet['layout'] : 'main.phtml')
                    );
                }
            }
        }
    }

    private function _validateAdministrator() {
        $loggedIn = false;
        if (Exoof_Session::get('adminAuthorized') == true || isset($_POST['login'])) {
            if (isset($_POST['login'])) {
                $admin = new Administrator();
                Exoof_Tools_Logger::log("admin", "Admin `%s` is trying to log in",$_POST['login']);
                $admin->findByLoginAndPassw($_POST['login'], $_POST['password']);
                if ($admin->isLoaded()) {
                    Exoof_Tools_Logger::log("admin", " ******** Admin `%s` logged in",$_POST['login']);
                    Exoof_Session::set('adminAuthorized', true);
                    Exoof_Session::set('admin', $admin->toArray());
                    $_SESSION['adminAuthorized'] = true; //flag for TinyMCE File Browser
                    $loggedIn = true;
                    return true;
                }
            } elseif (Exoof_Session::get('admin') != null)
                $loggedIn = true;
        }
        if (!$loggedIn) {
            Exoof_Tools_Logger::log("admin", "Admin login screen");
            $view = Exoof_View::getInstance();
            $view->setRenderScriptsPath($this->config->paths->adminControllers . '/../views/');
            $view->setRenderScript('index/login.phtml');
            $view->render();
            exit;
            //header('WWW-Authenticate: Basic realm="'.$this->config->labels->title1.' '.$this->config->labels->title2.'"');
            //header('HTTP/1.0 401 Unauthorized');
            //echo 'Access denied';
            //exit;
        }
    }

    public function run() {
        $requestUrl = implode('/', $this->_params) . '/';
        Exoof_Registry::set('activePath', $requestUrl);

        $request = array();
        $mp = sizeof($this->_params);
        if ($mp > 2) {
            for ($i = 2; $i < $mp; $i = $i + 2) {
                if (isset($this->_params[$i + 1]))
                    $request[$this->_params[$i]] = $this->_params[$i + 1];
                else
                    $request[$this->_params[$i]] = null;
            }
        }
        $eRequest = new Exoof_Request($request);
        if (isset($this->page))
            $eRequest->addParam('page', $this->page);
        Exoof_Factory::factory('Request', $eRequest);

        $view = Exoof_View::getInstance();

        $noRenderLayout = false;
        //controllers

        $mc = sizeof($this->controllers);
        if ($eRequest->isAjaxRequest()) {
            $mc = 1; //only 1 controller for AJAX requests
        }
        for ($c = 0; $c < $mc; $c++) {
            $controller = ((!empty($this->controllers[$c]['controller'])) ? $this->controllers[$c]['controller'] : 'index');
            $controllerClass = ucfirst($controller) . 'Controller';

            $action = strtolower(!empty($this->controllers[$c]['action']) ? $this->controllers[$c]['action'] : 'index');
            if ($action == '@auto@' && isset($this->_params[1])) //take action from URL
                $action = strtolower($this->_params[1]);
            elseif ($action == '@auto@' && !isset($this->_params[1]))
                $action = 'index';
            $actionMethod = $action . 'Action';

            if (!$this->_adminRequested)
                $file = $this->config->paths->appControllers . '/' . $controllerClass . '.php';
            else
                $file = $this->config->paths->adminControllers . '/' . $controllerClass . '.php';

            if (!file_exists($file)) {
                if ($this->config->debug->handleErrors != 'yes')
                    throw new Exoof_Exception_NotFound('File <i>`' . $file . '` doesn`t exist !</i>');
                else
                    throw new Exoof_Exception_NotFound();
            }

            try {
                include_once($file);
                $ctrl = new $controllerClass($controller, $action, dirname($file), $view);
                $ctrl->init();
                $ctrl->setLayout($this->controllers[$c]['layout']);
            } catch (Exception $e) {
                die('Can not create or use object of type `' . $controllerClass . '`');
            }

            if (method_exists($ctrl, $actionMethod)) {
                //try {
                $ctrl->$actionMethod();
                //} catch (Exception $e) {
                //    die('Can not call method `'.$controllerClass.'->'.$actionMethod.'`');
                //}
            } else {
                if ($this->config->debug->handleErrors != 'yes')
                    throw new Exoof_Exception_NotFound('Method  <i>`' . $controllerClass . ' &rarr; ' . $actionMethod . '()`</i> doesn`t exist !');
                else
                    throw new Exoof_Exception_NotFound();
            }
            ob_start();
            if ($ctrl->isRendered() == false)
                $ctrl->render();
            $controllerOutput = ob_get_clean();

            if (!Exoof_Factory::factory('http_code'))
                @header("HTTP/1.0 200 Ok");
            if (!$ctrl->isLayoutDisabled())
                $ctrl->view->container($this->controllers[$c]['container'], $controllerOutput);
            else {
                echo $controllerOutput;
                $noRenderLayout = true;
            }
        }
        if ($noRenderLayout === false) {
            $view->setRenderScriptsPath(dirname(dirname($file)) . '/layouts/');
            $view->setRenderScript($ctrl->getLayout());
            $view->render();
        }
    }

    public function autoload($class) {
        if ($class == 'BreadCrumbStack') // SUGAR CRM BUG
            return;
        if (strstr($class, 'Controller') && !preg_match('/^Exoof_/',$class)) {
            if (!$this->_adminRequested)
                @require_once($this->config->paths->appControllers.'/'.$class.'.php');
            else
                @require_once($this->config->paths->adminControllers.'/'.$class.'.php');
        } else {
            $classFile = str_replace('_', '/', $class) . '.php';
            include_once $classFile;
        }
    }

}

function debug($obj) {
    echo "<fiedset style='padding:0px;font:normal 11px Lucida Console,Courier New,Terminal;'><legend><b>" . ucwords(gettype($obj)) . "</b></legend>";
    echo "<pre>";
    print_r($obj);
    echo "</pre></fieldset>";
}
