<?php

class Exoof_Cache_eAccelerator extends Exoof_Cache_Abstract {

    public function fetch($key) {
        return eaccelerator_get($key);
    }

    public function store($key,$data,$ttl) {
        return eaccelerator_put($key,$data,$ttl);
    }

    public function delete($key) {
        return eaccelerator_rm($key);
    }

    public function clear() {
        return eaccelerator_clear();
    }

}