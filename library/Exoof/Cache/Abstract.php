<?php

/**
 * Cache abstract class
 */
abstract class Exoof_Cache_Abstract {

    /**
     * Fetches data from the cache by key
     * @param string $key Key
     */
    abstract function fetch($key);

    /**
     * Puts data into the cache
     * @param string $key Key of the cache record
     * @param mixed $data Data to cache
     * @param int $ttl Expire time (in seconds)
     */
    abstract function store($key,$data,$ttl);

    /**
     * Delete record in the cache
     * @param string $key Key of the cache record to delete data
     */
    abstract function delete($key);

    /**
     * Clears all cache data
     */
    abstract function clear();
}