<?php

class Exoof_Cache_APC extends Exoof_Cache_Abstract {

    public function fetch($key) {
        return apc_fetch($key);
    }

    public function store($key,$data,$ttl) {
        $ttl = (!$ttl)?-1:$ttl;
        return apc_store($key,$data,$ttl);
    }

    public function delete($key) {
        return apc_delete($key);
    }

    public function clear() {
//        debug(apc_cache_info('user'));
        return apc_clear_cache('user');
    }
}