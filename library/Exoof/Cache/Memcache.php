<?php

class Exoof_Cache_MemCache extends Exoof_Cache_Abstract {

// Memcache object
    public $connection;

    public function __construct() {
        $this->connection = new MemCache;
    }

    public function store($key, $data, $ttl) {
        return $this->connection->set($key,$data,0,$ttl);
    }

    public function fetch($key) {
        return $this->connection->get($key);
    }

    public function delete($key) {
        return $this->connection->delete($key);
    }

    public function addServer($host,$port = 11211, $weight = 10) {
        $this->connection->addServer($host,$port,true,$weight);
    }

}