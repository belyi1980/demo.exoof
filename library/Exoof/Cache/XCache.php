<?php

class Exoof_Cache_XCache extends Exoof_Cache_Abstract {

    public function fetch($key) {
        return xcache_get($key);
    }

    public function store($key,$data,$ttl) {
        return xcache_set($key,$data,$ttl);
    }

    public function delete($key) {
        return xcache_unset($key);
    }

    public function clear() {
        return xcache_clear_cache();
    }
}