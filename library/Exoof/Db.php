<?php
/**
 * Db (use Singleton pattern)
 *
 * @author eugene
 * @version 1.6 / Last Updated: 27 October 2010 / Project: VEULearning / Author: Eugene Belyaev
 */
class Exoof_Db extends PDO {

    /**
     * @var boolean
     */
    public $isConnected = false;

    /**
     *
     * @var string Tables prefix
     */
    private $_prefix;

    /**
     *
     * @var int PDO Fetch Mode
     * @see http://php.net/manual/en/pdostatement.fetch.php#pdostatement.fetch.parameters
     */
    private $_fetchMode = PDO::FETCH_ASSOC;

    protected $_pdoBindTypes = array(
            'char' => PDO::PARAM_STR,
            'int' => PDO::PARAM_INT,
            'bool' => PDO::PARAM_BOOL,
            'date' => PDO::PARAM_STR,
            'time' => PDO::PARAM_INT,
            'text' => PDO::PARAM_STR,
            'blob' => PDO::PARAM_LOB,
            'binary' => PDO::PARAM_LOB
    );

    /*
     * Connects to database
     * @param string $serverType Type of connection:mysql,sqlite...
     * @param string $host Database host
     * @param string $user Database user name
     * @param string $password Database user password
     * @param string $db Database name to connect to
     * @param string $charset Connection charset
     * @param string $prefix Tables prefix
     * @return void
    */
    public function __construct($serverType,$host,$user,$password,$db,$charset='utf8',$prefix='exf_') {
        $dsn = $serverType.':dbname='.$db.';host='.$host;

        $this->_prefix = $prefix;

        try {
            parent::__construct($dsn, $user, $password);
            $this->isConnected = true;
            try {
                $this->query("SET NAMES ?",array($charset));
            } catch(PDOException $e1) {
                //Maybe OLD MySQL version, so continue;
            }
        }
        catch (PDOException $e) {
            echo 'Connection failed: ' . $e->getMessage();
        }
        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * Returns tables prefix
     * @return string
     */
    public function getPrefix() {
        return $this->_prefix;
    }

    /**
     * Sets Fetch Mode for further queries
     * @param int $modes PDO::FETCH_* mode
     * @see http://php.net/manual/en/pdostatement.fetch.php#pdostatement.fetch.parameters
     * @return void
     */
    public function setFetchMode($modes) {
        $this->_fetchMode = $mode;
    }

    /**
     * Run SQL query
     * @param string $sql SQL Query
     * @param array $params Array with parameters to SQL query
     * @return boolean Returns TRUE on success or FALSE on failure.
     */
    public function query($sql, $params=array()) {
        $stmt = $this->_prepare($sql);
        $stmt->execute($params);
        return $stmt;
    }

    /**
     * Returns array of results
     * @param string $tableOrSql SQL Query or Table Name
     * @param array $params Array with parameters to SQL query
     * @return array Array of results according with fetch mode on success and FALSE on failure
     */
    public function fetchAll($tableOrSql, $params=array()) {
        $sql  = $this->_prepareSelectQuery($tableOrSql);
        $stmt = $this->_prepare($sql);
        $stmt->execute($params);
        return $stmt->fetchAll();
    }

    /**
     * Returns single row result
     * @param string $tableOrSql SQL Query or Table Name
     * @param array $params Array with parameters to SQL query
     * @return array Single row result according with fetch mode on success and FALSE on failure
     */
    public function fetchRow($tableOrSql, $params=array()) {
        $sql = $this->_prepareSelectQuery($tableOrSql);
        $stmt = $this->_prepare($sql);
        $stmt->execute($params);
        return $stmt->fetch();
    }

    /**
     * Returns singlle column result
     * @param string $tableOrSql SQL Query or Table Name
     * @param array $params Array with parameters to SQL query
     * @return array Single column result according with fetch mode on success and FALSE on failure
     */
    public function fetchColumn($tableOrSql, $params=array()) {
        $sql = $this->_prepareSelectQuery($tableOrSql);
        $stmt = $this->_prepare($sql);
        $stmt->execute($params);
        return $stmt->fetchColumn();
    }

    /**
     * Returns single row result as object
     * @param string $tableOrSql SQL Query or Table Name
     * @param array $params Array with parameters to SQL query
     * @return Object Single row result on success and FALSE on failure
     */
    public function fetchObject($tableOrSql, $params=array()) {
        $sql = $this->_prepareSelectQuery($tableOrSql);
        $stmt = $this->_prepare($sql);
        $stmt->execute($params);
        return $stmt->fetchObject();
    }

    /**
     * Returns associative array result
     * @param string $sql SQL Query
     * @param array $params Array with parameters to SQL query
     * @return array Associative array ([col1]=>col2)
     */
    public function fetchAssoc($sql, $params=array()) {
        $stmt = $this->_prepare($sql);
        $stmt->execute($params);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $assocResult = array();
        if (sizeof($result)) {
            foreach ($result as $row) {
                $assocResult[array_shift($row)] = array_shift($row);
            }
        }
        return $assocResult;
    }

    /**
     * Returns results as objects
     * @param string $tableOrSql SQL Query or Table Name
     * @param array $params Array with parameters to SQL query
     * @return array Array of objects on success and FALSE on failure
     */
    public function fetchObjects($tableOrSql, $params=array()) {
        $sql = $this->_prepareSelectQuery($tableOrSql);
        $stmt = $this->_prepare($sql);
        $stmt->execute($params);
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public function fetchCount($tableOrSql, $whereArr=array()) {
        if (preg_match("/^ ?select (.*?) from/i",$tableOrSql,$arr)) {
            $sql = str_replace($arr[1],' COUNT(*) ',$tableOrSql);
        }
        else
            $sql = "SELECT COUNT(*) FROM ".$tableOrSql;

        $params   = array();
        $whereSql = $this->parseWhere($whereArr);
        if ($whereSql) {
                if (is_array($whereArr))
                    $params = array_values($whereArr);
            $sql .= (preg_match("/ ?where ?/i",$tableOrSql))?$whereSql:" WHERE ".$whereSql;
        }
        return $this->fetchColumn($sql, $params);
    }

    /**
     * Fetches data from the table according to conditions
     * @param string $tableOrSql Table name or SQL query (without LIMIT, ORDER, WHERE etc.)
     * @param array $whereArr Associative array of where conditions
     * @param int $count Number of records to be extracted
     * @param int $offset Starting offset
     * @param string $orderBy Field to order by
     * @param string $orderDest Order destination (ASC, DESC)
     * @return array Array of results
     */
    public function fetch($tableOrSql, $whereArr=array(), $count=null, $offset=null, $orderBy=null, $orderDest=null) {
        $sql = $this->_prepareSelectQuery($tableOrSql);
        $params = array();

        $whereSql = $this->parseWhere($whereArr);
        if ($whereSql) {
            if (is_array($whereArr))
                $params = array_values($whereArr);
            $sql .= (preg_match("/ +where +/i",$tableOrSql))?$whereSql:" WHERE ".$whereSql;
        }

        $orderDest = (in_array(strtoupper($orderDest),array('ASC','DESC')))?$orderDest:null;
        if ($orderBy && $orderDest)
            $sql .= " ORDER BY ".$orderBy.' '.$orderDest;
        elseif ($orderBy)
            $sql .= " ORDER BY ".$orderBy;
        if ($offset && $count)
            $sql .= " LIMIT ".$offset.','.$count;
        elseif ($count)
            $sql .= " LIMIT ".$count;
        //debug($sql);
        //debug($params);
        return $this->fetchAll($sql, $params);
    }


    /**
     * Inserts array of values into db table
     * @param string $table Name of the table
     * @param string $array Associative array (field=>value)
     * @return int Last inserted ID on success or NULL on failure
     * @example To use SQL functions use directField static method. Sample: 'file_date_added' => Exoof_Db::directField('UNIX_TIMESTAMP()')
     */
    public function insert($table, $array) {
        $sqlValues = array();
        $bindValues   = array();
        foreach ($array as $field=>$value) {
            if (is_object($value) && get_class($value)=='Exoof_Db_DirectField')
                $sqlValues[] = (string)$value;
            else {
                $bindValues[$field] = $value;
                $sqlValues[]        = ':'.$field;
            }
        }

        $sql  = "INSERT INTO `{$table}`(".implode(' , ' , array_keys($array)).')';
        $sql .= ' VALUES( '.implode(' , ' , $sqlValues).' )';

        $stmt       = $this->prepare($sql);
        $fieldsMeta = $this->getTableMeta($table);
//        print_r($sql);
        foreach ($bindValues as $field => $value) {
            switch($fieldsMeta[$field]['pdoType']) {
                case PDO::PARAM_INT:
                    $defValue = null;
                    break;
                case PDO::PARAM_STR:
                default:
                    $defValue = '';
                    break;

            }
            $stmt->bindValue(':'.$field, ($value!==null)?$value:$defValue, $fieldsMeta[$field]['pdoType']);//bindParam works wrong !!!
        }
        try {
            $executed = $stmt->execute();
        } catch (PDOException $e) {
            throw new PDOException($e->getMessage().'<hr/>'.$sql);
        }
        if ($executed)
            return ($this->lastInsertId()?$this->lastInsertId():$stmt->rowCount());
        else
            return null;
    }

    /**
     * Delete rows in a table
     * @param string $table Name of the table
     * @param mixed $whereArr Associative array of where conditions OR sql where statement
     * @return int Number of affected rows on success or NULL on failure
     */
    public function delete($table, $whereArr=null) {
        $sql  = "DELETE FROM `{$table}`";

        $params = null;
        $whereSql = $this->parseWhere($whereArr);
        if ($whereSql) {
            $sql .= (preg_match("/ +where +/i",$table))?$whereSql:" WHERE ".$whereSql;
            $params = array_values($whereArr);
        }

        //debug($sql);exit;
        $stmt = $this->query($sql, $params);
        if ($stmt &&  $stmt->rowCount())
            return $stmt->rowCount();
        else
            return null;
    }

    protected function parseWhere($whereArr, $bindType='?') {
        $sql = null;
        if ($whereArr && is_array($whereArr)) {
            if (sizeof($whereArr)) {
                foreach ($whereArr as $field=>$value) {
                    if (is_object($value) && get_class($value)=='Exoof_Db_Like')
                        $where[] = $field." LIKE (".(($bindType=='?')?'?':':'.$field).')';
//                        $where[] = 'UPPER('.$field.') LIKE UPPER('.(($bindType=='?')?'?':':'.$field).')';
                    elseif(is_object($value) && get_class($value)=='Exoof_Db_DirectField')
                        $where[] = $field.' '.$value->getOperator().' ?';
                    else
                        $where[] = $field.' = '.(($bindType=='?')?'?':':'.$field);
                }
                $sql .= ' '.implode(' AND ',$where);
                //$params = array_values($whereArr);
            }
        } elseif ($whereArr && !is_array($whereArr)) {
            $sql .= ' '.$whereArr;
        }
        return $sql;
    }

    /*protected function parseParameters($bindParams) {
        $bindValues   = array();
        foreach ($bindParams as $field=>$value) {
            if (is_object($value) && get_class($value)=='Exoof_Db_DirectField')
                $sqlValues[] = (string)$value;
            else {
                $bindValues[$field] = $value;
                $sqlValues[]        = ':'.$field;
            }
        }
    }*/

    /**
     * Updates table with array of values
     * @param string $table Name of the table
     * @param string $array Associative array (field=>value)
     * @param mixed $where Where condition statement
     * @return int Number of affected rows on success or NULL on failure
     * @example To use SQL functions use directField static method. Sample: 'file_date_added' => Exoof_Db::directField('UNIX_TIMESTAMP()')
     */
    public function update($table, $array, $where=null) {
        $sqlValues = array();
        $bindValues   = array();

        foreach ($array as $field=>$value) {
            if (is_object($value) && get_class($value)=='Exoof_Db_DirectField')
                $sqlValues[$field] = $field.'='.(string)$value;
            else {
                $bindValues[$field] = $value;
                $sqlValues[$field] = $field.'=:'.$field;
            }
        }

//        debug($bindValues);
        if (is_array($where) && sizeof($where))
            $bindValues = array_merge($bindValues, $where);

//        debug($bindValues);exit;
        $sql  = "UPDATE `{$table}` SET ".implode(' , ' , $sqlValues);


        $whereSql = $this->parseWhere($where,':');
        if ($whereSql) {
            $sql .= (preg_match("/ +where +/i",$table))?$whereSql:" WHERE ".$whereSql;
        }

        $stmt       = $this->prepare($sql);
        $fieldsMeta = $this->getTableMeta($table);
        //print_r($sql);print_r($bindValues);exit;
        foreach ($bindValues as $field => $value) {
            switch($fieldsMeta[$field]['pdoType']) {
                case PDO::PARAM_INT:
                    $defValue = null;
                    break;
                case PDO::PARAM_STR:
                default:
                    $defValue = '';
                    break;

            }
            $stmt->bindValue(':'.$field, ($value!==null)?$value:$defValue, $fieldsMeta[$field]['pdoType']);
        }

        try {
            $executed = $stmt->execute();
        } catch (PDOException $e) {
            throw new PDOException($e->getMessage().'<hr/>'.$sql);
        }
        if ($executed)
            return $stmt->rowCount();
        else
            return null;
    }

    public function getError() {
        return implode(' - ',$this->errorInfo());
    }

    /**
     * Extracts Table META information
     * @param string $table Name of the table
     * @return array Array of fields with meta info
     */
    public function getTableMeta($table) {
        static $metaInfoCache;
        if (!$metaInfoCache)
            $metaInfoCache = array();

        if (!array_key_exists($table, $metaInfoCache)) {
            if ($this->isCacheEnabled()) {
                $_cachedMeta = $this->_cacheEngine->fetch($table);
                if (is_array($_cachedMeta)) {
                    $metaInfoCache[$table] = $_cachedMeta;
                }
            }

            if (!isset($metaInfoCache[$table])) {//if was not cached
                $columns = $this->fetchAll('SHOW COLUMNS FROM '.$table);
                foreach($columns as $key => $col) {
                    $_fieldMeta[$col['Field']] = $this->parseColumnType($col);
                }
                $metaInfoCache[$table] = $_fieldMeta;
                if ($this->isCacheEnabled())
                    $this->_cacheEngine->store($table, $_fieldMeta, $this->_cacheExpire);
            }
        }

        return $metaInfoCache[$table];
    }

    /**
     * Parse PDO-produced column type
     * @param array $col Array with column meta information
     * @return array  Array with changed column meta information
     */
    protected function parseColumnType($col) {
        $colType = $col['Type'];
        $colInfo = array();
        $colParts = explode(" ", $colType);
        if($fparen = strpos($colParts[0], "(")) {
            $colInfo['type'] = substr($colParts[0], 0, $fparen);
            $colInfo['pdoType'] = '';
            $colInfo['length']  = str_replace(")", "", substr($colParts[0], $fparen+1));
            $colInfo['attributes'] = isset($colParts[1]) ? $colParts[1] : NULL;
        }
        else {
            $colInfo['type'] = $colParts[0];
        }

        // PDO Bind types
        $pdoType = '';
        foreach($this->_pdoBindTypes as $pKey => $pType) {
            if(strpos(' '.strtolower($colInfo['type']).' ', $pKey)) {
                $colInfo['pdoType'] = $pType;
                break;
            } else {
                $colInfo['pdoType'] = PDO::PARAM_STR;
            }
        }
        if($col['Key'] == "PRI") {
            $colInfo['primary'] = true;
        }

        return $colInfo;
    }

    /**
     *
     * @param string $tableOrSql SQL Query or Table Name
     */
    protected function _prepareSelectQuery($tableOrSql) {
        if (preg_match("/^ ?select/i",$tableOrSql) || preg_match("/^ ?show ?columns/i", $tableOrSql))
            $sql = $tableOrSql;// SQL query
        else
            $sql = "SELECT * FROM ".$tableOrSql;
        return $sql;
    }

    /**
     * Prepares SQL query
     * @param string $sql SQL query
     * @return PDOStatement PDO Statement object
     */
    protected function _prepare($sql) {
        $stmt = $this->prepare($sql);
        $stmt->setFetchMode($this->_fetchMode);
        return $stmt;
    }

    static public function directField($operator=null, $value) {
        return new Exoof_Db_DirectField($operator, $value);
    }

    static public function likeCondition($constr) {
        return new Exoof_Db_Like($constr);
    }

    //========================== CACHE ====================================
    /**
     * @var Exoof_Cache_Abstract Cache object
     */
    private $_cacheEngine;

    /**
     * @var int Cache expire TTL. Default is 0 - cache disabled
     */
    private $_cacheExpire = 0;

    /**
     * Sets Cache Engine Object for internal purposes
     * @param Exoof_Cache_Abstract $engine Cache Engine Object
     * @param int $expire Cache expire TTL. Default is 0 - cache disabled
     */
    public function setCache(Exoof_Cache_Abstract $engine, $expire=0) {
        $this->_cacheEngine = $engine;
        $this->_cacheExpire = $expire;
    }

    /**
     * @return boolean Returns TRUE if cache can be used and FALSE if not
     */
    private function isCacheEnabled() {
        return ($this->_cacheExpire > 0 && is_object($this->_cacheEngine))?true:false;
    }
}