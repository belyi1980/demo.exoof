<?php
/**
 * Exoof View class
 *
 * @author eugene
 * @version 1.2 Last Updated : 27 May 2010
 */
class Exoof_View {

    /**
     * @var Exoof_View
     */
    private static $instance;

    protected $_script;

    /*
     * @var array Array of assigned vars
     */
    protected $assignedVars = array();

    /*
     * @var string Directory to render controller view scripts
     */
    private $_renderScriptsDir;

    /*
     * @var string View script to render
     */
    private $_renderScript;

    /**
     * Constructor is locked due to Singleton pattern
     */
    protected function __construct() {

    }

    /**
     * Returns Exoof_View object
     * @return Exoof_View
     */
    public static function getInstance() {
        if (!is_object(self::$instance) || !(self::$instance instanceof Exoof_View))
            self::$instance = new Exoof_View();
        return self::$instance;
    }
    /*
     * Renders view script accroding to controller/action
     */
    public function render() {

        if (file_exists($this->_renderScriptsDir.$this->_renderScript))
            include $this->_renderScriptsDir.$this->_renderScript;
        else
            throw new Exception('View script `'.$this->_renderScriptsDir.$this->_renderScript.'` doesn`t exist !');
    }

    /*
     * Assign variable to View
     * @param string $name Name of the assigned variable
     * @param mixed $value Variable value
     * @return void
     */
    public function assign($name, $value) {
       $this->assignedVars[$name] = $value;
    }

    public function  __set($name,  $value) {
       $this->assign($name, $value) ;
    }

    public function getVars() {
        return $this->assignedVars;
    }
    
    public function  __get($name) {
        if (array_key_exists($name, $this->assignedVars))
            return $this->assignedVars[$name];
        else
            return null;
    }

    public function  __call($helper,  $arguments=null) {
        static $helperObjectsStorage;

        if (!$helperObjectsStorage) // to store created objects
            $helperObjectsStorage = array();

        $helper      = ucfirst($helper);
        $helperClass = 'Exoof_View_Helper_'.$helper;
        $helperMethod= $helper;

        if (array_key_exists($helperClass, $helperObjectsStorage))
            $helperObject = $helperObjectsStorage[$helperClass];
        else
            try {
                $helperObject= new $helperClass();
            } catch(Exception $e) {
                throw new Exception("Can`t find helper `".$helper."`");
            }

            try {
                //echo ("calling `".$helperClass."&rarr;".$helperMethod."(".implode(',',$arguments).")`");
                $return = call_user_func(array($helperObject,$helperMethod), @$arguments[0], @$arguments[1], @$arguments[2], @$arguments[3], @$arguments[4], @$arguments[5], @$arguments[6], @$arguments[7], @$arguments[8], @$arguments[9], @$arguments[10]);
            } catch (Exception $e) {
                throw new Exception("Can`t call `".$helperClass."&rarr;".$helperMethod."(".implode(',',$arguments).")`");
            }

        if (!property_exists($helperObject, 'cacheable') || $helperObject->cacheable == true)
            $helperObjectsStorage[$helperClass] = $helperObject;

        return $return;
    }

    public function setRenderScriptsPath($path) {
        $this->_renderScriptsDir = $path;
        set_include_path(
            realpath($path) . PATH_SEPARATOR .
            get_include_path()
        );
    }

    public function setRenderScript($renderScript) {
        if (substr($renderScript,-6) != '.phtml')
            $renderScript .= '.phtml';
        $this->_renderScript = strtolower($renderScript);
    }
}
?>
