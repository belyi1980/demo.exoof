<?php
/**
 * Lang View Helper
 *
 * @author eugene
 * @version 1.1  Updated:9 April 2010
 */
class Exoof_View_Helper_Url {

    private $url;
    private $params;

    public function url() {
        $args = array_unique(func_get_args());
        //$args = array_merge(array(Exoof_Lang::getLangUrl()),$args);
        if (sizeof($args)  && (sizeof($args)==1 && $args[0]!=''))
            $this->url = implode('/',$args);
        else
            $this->url = $_SERVER['REQUEST_URI'];
        return $this;
    }

    public function addParams($arr) {
        if (sizeof($arr))
            foreach($arr as $param => $value) {
                $this->url = preg_replace("/(".$param."=.*)/","",$this->url);
                $params[] = $param."=".$value;
            }

        $this->url = preg_replace("/\?$/", "", $this->url);
        if (sizeof($params) && !strstr($this->url,"?"))
                $this->url .= "?";
        elseif(sizeof($params))
                $this->url .= "&";
        $this->url .= join("&",$params);
        return $this;
    }

    public function __toString() {
        return str_replace('//','/',$this->url);
    }
}