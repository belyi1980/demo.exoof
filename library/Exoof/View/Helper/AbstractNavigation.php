<?php
/**
 * Abstract Navigation Helper
 *
 * @author eugene
 */
class Exoof_View_Helper_AbstractNavigation {

    public $cacheable = false;

    protected $_structure = array();
    protected $_path = array();

    public function __construct() {
        $struc = new Exoof_Tools_Pages();
        $this->_structure = $struc->getAllParents();
    }

    protected function activeLevel() {
        $this->_findActiveLevel($this->_structure);
        return $this;
    }

    private function _findActiveLevel($arrSlice) {
        if (is_array($arrSlice))
            foreach ($arrSlice as $level) {
                if (isset($level['active']) && $level['active']==1) {
                    $this->_path[] = $this->_getAttributesOnly($level);
                //                $this->_activeLevel = $level;
                }
                if (isset($level['pages']) && sizeof($level['pages']))
                    $this->_findActiveLevel($level['pages']);
            }
    }

    private function _getAttributesOnly($arr) {
        $attrs = array();
        if (is_array($arr)) {
            foreach ($arr as $key => $val) {
                if (!is_array($val))
                    $attrs[$key] = $val;
            }
        }
        return $attrs;
    }
}
