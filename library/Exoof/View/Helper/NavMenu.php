<?php
/**
 * Tree-based Menu Helper
 *
 * @author eugene
 * @version 1.2 / Last Updated : 19 October 2010 / Project : Kings / Author : Alex Koleda
 */
class Exoof_View_Helper_NavMenu extends Exoof_View_Helper_AbstractNavigation {

    private $_output = '';
    private $_startLevel = null;
    private $_activeLevel = null;
    private $_endLevel = null;
    private $_activeOnly = false;

    public function navMenu() {
        return $this;
    }

    public function setStartLevel($level=-1) { // -1 means start level is the level of last active element
        $this->_startLevel = (int)$level;
        return $this;
    }

    public function setEndLevel($level=null) {
        $this->_endLevel = (int)$level;
        return $this;
    }

    public function setActiveOnly($active=true) {
        $this->_activeOnly = $active;
        return $this;
    }

    private function _sortLevels($arrSlice) {
        if ($this->_startLevel > 1)
            if (sizeof($arrSlice))
                foreach ($arrSlice as $idx=>$arrSlice2) {
                    if (isset($arrSlice2['pages'])) {
                        if (($arrSlice2['level']+1)==$this->_startLevel) {
                            $this->_structure = $arrSlice2['pages'];
                            break;
                        }
                        $this->_sortLevels($arrSlice2['pages']);
                    }
                }
    }

    private function _sortActive($arrSlice) {
            if (sizeof($arrSlice)) {
                $arrSliceNew = array();
                $wasActive = false;
                foreach ($arrSlice as $idx=>$arrSlice2) {
                    if (isset($arrSlice2['active']) && $arrSlice2['active']==1) {
                        $wasActive = true;
                        if ($this->_startLevel==-1)
                                $this->_activeLevel = $arrSlice2['level'];
                        if (isset($arrSlice2['pages']))
                            $arrSlice2['pages'] = $this->_sortActive($arrSlice2['pages']);
                        $arrSliceNew[$idx] = $arrSlice2;
                    } else
                        $arrSliceNewSafe[$idx] = $arrSlice2;
                }
                if (!$wasActive)
                    $arrSliceNew += $arrSliceNewSafe;
            }
            return $arrSliceNew;
    }

    private function _renderMenu($arrSlice, $parActive=false, $sub=true) {

        $this->_output .= sprintf("<ul class='%s%s'>\n",($sub?'navSubMenu':'navMenu'),($parActive)?' active':'');
        if (sizeof($arrSlice))
        foreach ($arrSlice as $level) {
            $active = (isset($level['active'])&&$level['active']==1);
            $label  = htmlspecialchars($level['label']);
            $this->_output  .= sprintf("<li%s><a href='%s' title='%s'>%s</a>", $active?" class='active'":'',$level['uri'],$label, $label);
            if (isset($level['pages']) && is_array($level['pages'])) {
                if (!$this->_endLevel || ($level['level']+1)<=$this->_endLevel)
                    $this->_renderMenu($level['pages'], $active);
            }
            $this->_output .= "</li>\n";
        }
        $this->_output  .= "</ul>\n";
    }

    public function  __toString() {
//        debug($this->_structure);
        if ($this->_activeOnly) {
            $this->_structure = $this->_sortActive($this->_structure);
            if ($this->_startLevel == -1)
                     $this->_startLevel = $this->_activeLevel;
        }
//        debug($this->_structure);
        $this->_sortLevels($this->_structure);
        $this->_renderMenu($this->_structure,true,false);
        return $this->_output;
    }
}
