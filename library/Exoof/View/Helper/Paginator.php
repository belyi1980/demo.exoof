<?php
/**
 * Description of Paginator
 *
 * @author eugene
 * @version 1.2 Last Update : 4 June 2010
 */
class Exoof_View_Helper_Paginator {
    //put your code here

    public function paginator($total, $perpage=null, $link=null, $ajaxOptions=null) {
        if (!$total)
            return;
        $perpage = ($perpage==null)?20:$perpage;
        $id   = uniqid('paginator');
        //$out  = '<script type="text/javascript" src="scripts/jquery.pagination.js"></script><link rel="stylesheet" href="styles/pagination.css" type="text/css" />';
        $out = '<div class=clear id="'.$id.'"></div>';
        $current = (isset($_GET['_p']))?intval($_GET['_p']):0;

        $link = ($link)?$link:$_SERVER['REQUEST_URI'];
        if (preg_match('/^\?/',$link)) {
            $url = parse_url($_SERVER['REQUEST_URI']);
            $link = $url['path'].$link;
        }

        $lparts = parse_url($link);
        $link  = $lparts['path'];
        if (isset($lparts['query'])) {
            $query = preg_replace("/(&?_p=[0-9]*)/", "", $lparts['query']);
            $query = trim($query,'&');
            $link  .= (!empty($query))?'?':'';
            $link  .= $query;
        }
        $add  = (strstr($link,'?'))?'&_p=':'?_p=';
//        $add .= '__id__';

        $step  = 4;
        $pages = ceil($total / $perpage);
        if ($pages == 1)
            return;

        $hf_pages = floor($pages/2);
        $hf_step  = ceil($step/2);

        $out .= '<script>
                $("#'.$id.'").pagination('.$total.', {
                current_page : '.$current.',
                items_per_page : '.$perpage.',
                link_to : "'.$link.$add.'__id__",
                callback: function(p ,cont){ return true; }
                });
                </script>';
        /*
        if ($pages == 1)
            return;

        if ($current != 1)
            $out .= "<a href='".$link.$add.($current-1)."'>&larr;</a>&nbsp;&nbsp;";
        for($p=1; $p <= $pages; $p++) {
            if ($pages > 20) {
                //if ($p == $step+1 || $p == ($hf_pages + $step) || $p==($current+$step))
                if ($p == $step+1 || $p==($current+$step))
                {
                    $out .= '<div class=dots> &#8230; </div>';
                }
                if (!  ($current-$hf_step <= $p && $p <= $current+$hf_step) ) {
                //if ( ($p > $step && $p < $pages-$step) && ($p < ($hf_pages - $hf_step) || $p > ($hf_pages + $hf_step)) )
                if ( ($p > $step && $p < $pages-$step))
                    continue;
                }

            }
            if ($p != $current)
                $out .= "<a href='".$link.$add.$p."'>{$p}</a> ";
            else
                $out .= "<a href='".$link.$add.$p."' class='current'>{$p}</a> ";
        }
        if ($current != $pages)
            $out .= "&nbsp;&nbsp;<a href='".$link.$add.($current+1)."'>&rarr;</a> ";

        $out .= "</div>";
        if (isset($ajaxOptions['id']))
        {
            $out  .= "<script>$('#".$id." a').click(function(){\$('#".$ajaxOptions['id']."').load($(this).attr('href'));return false;});</script>";
        }*/

        return $out;
    }

}
?>
