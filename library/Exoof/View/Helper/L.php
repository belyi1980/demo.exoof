<?php
/**
 * Lang View Helper
 *
 * @author eugene
 */
class Exoof_View_Helper_L {

    public function L($string) {
            return Exoof_Factory::factory('Translate')->translate($string);
    }
}
?>
