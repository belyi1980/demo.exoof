<?php
/**
 * @author Eugene Belyaev
 * @version 1.0 / Last Updated : 27 October 2010 / Project: VEULearning / Author: Eugene Belyaev
 */
class Exoof_View_Helper_FormEmail extends Exoof_View_Helper_AbstractFormField {
    //put your code here
    
    /**
     * Inits Form String object
     * @param string $name Name of the field
     * @param string $label Label of the field
     * @param string $value
     * @param array $options
     * @return Exoof_View_Helper_AbstractFormField
     */
    public function FormEmail($name, $label, $value=null, $options=array()) {
        return $this->_init($name, $label, $value, $options);
    }

    public function  __toString() {
        $attributes = parent::__toString();

        $format = '';
        if (!empty($this->_label))
            $format .= sprintf("<label %s for='%s' id='label_%s'>%s</label>",
                    ($this->_required)?"required='true'":'',
                    $this->_id,
                    $this->_id,
                    $this->_label);

        $format .= sprintf("<input type='text' name='%s' id='%s' role='email' value='%s' %s title='%s' %s />",
                        $this->_name,
                        $this->_id,
                        htmlspecialchars(stripslashes($this->_value),ENT_QUOTES),
                        ($this->_required)?"required='true'":'',
                        $this->_title,
                        $attributes
                        );
        return $format;
    }

}
?>
