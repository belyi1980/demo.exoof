<?php
/**
 * Date View Helper
 *
 * @author eugene
 */
class Exoof_View_Helper_DateTime {

    public function dateTime($time) {
        $lang = Exoof_Translate::getInstance()->getCurrentLang();
        @setlocale(LC_TIME, strtolower($lang).'_'.strtoupper($lang));
        return strftime('%c', $time);
    }
}
?>
