<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Container
 *
 * @author eugene
 */
class Exoof_View_Helper_Container {

    /**
     *
     * @var array Containers
     */
    private $containers = array();

    public function container($containerName, $containerContent=null) {
        if ($containerContent)
        {
            $this->containers[$containerName] = $containerContent;
        } else {
            if (array_key_exists($containerName, $this->containers))
                return $this->containers[$containerName];
            else
                return null;
        }
    }

}
?>
