<?php
/**
 * Description of FormString
 *
 * @author eugene
 * @version 1.1 Last Updated 7 July 2010
 */
class Exoof_View_Helper_FormCheckbox extends Exoof_View_Helper_AbstractFormField {
    //put your code here
    
    protected $_checked = false;

    /**
     * Inits Form String object
     * @param string $name Name of the field
     * @param string $label Label of the field
     * @param string $value
     * @param array $options
     * @return Exoof_View_Helper_AbstractFormField
     */
    public function FormCheckbox($name, $label, $value=null, $options=array()) {

        $postedValue = Exoof_Factory::factory('Request')->getParam($name);
        if ($postedValue == $value)
            $this->setChecked(true);

        return $this->_init($name, $label, $value, $options);
    }

    public function setChecked($checked=true) {
        $this->_checked = $checked;
        return $this;
    }

    public function  __toString() {
        $attributes = parent::__toString();

        $format = '';
        if (!empty($this->_label))
            $format .= sprintf("<label %s for='%s' id='label_%s'>%s</label>",
                    ($this->_required)?"required='true'":'',
                    $this->_id,
                    $this->_id,
                    $this->_label);

        $format .= sprintf("<input type='checkbox' %s name='%s' id='%s' value='%s' %s title='%s' %s />",
                        ($this->_checked?'checked':''),
                        $this->_name,
                        $this->_id,
                        htmlspecialchars(stripslashes($this->_value),ENT_QUOTES),
                        ($this->_required)?"required='true'":'',
                        $this->_title,
                        $attributes
                        );
        return $format;
    }

}