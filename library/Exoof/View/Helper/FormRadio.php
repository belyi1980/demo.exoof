<?php
/**
 * Description of FormString
 *
 * @author eugene
 */
class Exoof_View_Helper_FormRadio extends Exoof_View_Helper_AbstractFormField {
    //put your code here
    
    private $_checked = false;

    /**
     * Inits Form String object
     * @param string $name Name of the field
     * @param string $label Label of the field
     * @param string $value
     * @param array $options
     * @return Exoof_View_Helper_AbstractFormField
     */
    public function FormRadio($name, $label, $value=null, $options=array()) {
        return $this->_init($name, $label, $value, $options);
    }

    public function setChecked($checked=true) {
        $this->_checked = $checked;
        return $this;
    }

    public function  __toString() {
        $attributes = parent::__toString();

        $format = '';
        if (!empty($this->_label))
            $format .= sprintf("<label required='%s' for='%s' id='label_%s'>%s</label>",
                    ($this->_required)?'true':'false',
                    $this->_id,
                    $this->_id,
                    $this->_label);

        $format .= sprintf("<input type='radio' %s name='%s' id='%s' value='%s' required='%s' title='%s' %s />",
                        ($this->_checked?'checked':''),
                        $this->_name,
                        $this->_id,
                        htmlspecialchars(stripslashes($this->_value),ENT_QUOTES),
                        ($this->_required)?'true':'false',
                        $this->_title,
                        $attributes
                        );
        return $format;
    }

}
?>
