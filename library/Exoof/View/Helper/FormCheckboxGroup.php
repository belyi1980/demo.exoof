<?php
/**
 * @author Eugene Belyaev
 * @version 1.0 / Last Updated: 29 October 2010 / Project: DEMO / Author: Eugene Belyaev
 */
class Exoof_View_Helper_FormCheckboxGroup extends Exoof_View_Helper_AbstractFormField {
    
    protected $_checked = false;

    /**
     * Inits Form Checkbox Group object
     * @param string $name Name of the field
     * @param string $label Label of the field
     * @param string $value
     * @param array $options
     * @return Exoof_View_Helper_AbstractFormField
     */
    public function FormCheckbox($name, $label, $options=array()) {

//        $postedValue = Exoof_Factory::factory('Request')->getParam($name);
//        if ($postedValue == $value)
//            $this->setChecked(true);

        return $this->_init($name, $label, null, $options);
    }

    public function setChecked($checked=true) {
        $this->_checked = $checked;
        return $this;
    }

    public function  __toString() {
        $attributes = parent::__toString();

        $format = '';
        if (!empty($this->_label))
            $format .= sprintf("<label %s for='%s' id='label_%s'>%s</label>",
                    ($this->_required)?"required='true'":'',
                    $this->_id,
                    $this->_id,
                    $this->_label);

        $format .= sprintf("<input type='checkbox' %s name='%s' id='%s' value='%s' %s title='%s' %s />",
                        ($this->_checked?'checked':''),
                        $this->_name,
                        $this->_id,
                        htmlspecialchars(stripslashes($this->_value),ENT_QUOTES),
                        ($this->_required)?"required='true'":'',
                        $this->_title,
                        $attributes
                        );
        return $format;
    }

}