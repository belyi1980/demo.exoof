<?php
/**
 * Captcha
 *
 * @author eugene
 * @version 1.1 / Last Updated: 28 October 2010 / Project: VEULearning / Author: Eugene Belyaev
 */
class Exoof_View_Helper_FormCaptcha extends Exoof_View_Helper_AbstractFormField {
    //put your code here
    
    /**
     * Inits Form Captcha object
     * @param string $name Name of the field
     * @param string $label Label of the field
     * @param string $value
     * @param array $options
     * @return Exoof_View_Helper_AbstractFormField
     */
    public function FormCaptcha($name, $label, $options=array()) {
        return $this->_init($name, $label, null, $options);
    }

    public function  __toString() {
        $attributes = parent::__toString();

        $format = "<div class='captcha_box clear'>";
        if (!empty($this->_label))
                $format .= sprintf("<label required='%s' for='%s' id='label_%s'>%s</label>",
                        ($this->_required)?'true':'false',
                        $this->_id,
                        $this->_id,
                        $this->_label);
        
        $format .= sprintf("<div class='captcha_box_in'><div class='clear'><img src='getcaptcha.php?r' class='captcha' /><div class='reload' onclick='%s'></div></div><div class='bt_left'><input type='text' maxlength='5' name='%s' id='%s' value='%s' required='%s' role='numeric' chars='5' title='%s' %s /></div></div>",
                        htmlspecialchars(stripslashes("$(this).parent().find('img').attr('src','getcaptcha.php?r&'+Math.random())"),ENT_QUOTES),
                        $this->_name,
                        $this->_id,
                        "",
                        ($this->_required)?'true':'false',
                        $this->_title,
                        $attributes
                        );
        $format .= "</div>";
        return $format;
    }

}