<?php
/**
 * Description of FormString
 *
 * @author eugene
 */
class Exoof_View_Helper_FormHidden extends Exoof_View_Helper_AbstractFormField {
    //put your code here
    
    /**
     * Inits Form Hidden object
     * @param string $name Name of the field
     * @param string $label Label of the field
     * @param string $value
     * @param array $options
     * @return Exoof_View_Helper_AbstractFormField
     */
    public function FormHidden($name, $value=null) {
        return $this->_init($name, null, $value);
    }

    public function  __toString() {
        parent::__toString();
        return sprintf("<input type='hidden' name='%s' id='%s' value='%s' />",
                        $this->_name,
                        $this->_id,
                        htmlspecialchars(stripslashes($this->_value),ENT_QUOTES)
                        );
    }

}
?>
