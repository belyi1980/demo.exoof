<?php
/**
 * LangUrl View Helper
 *
 * @author eugene
 */
class Exoof_View_Helper_LangUrl {

    public function langUrl() {
        return Exoof_Lang::getLangUrl();
    }
}