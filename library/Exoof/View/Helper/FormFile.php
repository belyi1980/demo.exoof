<?php
/**
 * Description of FormFile Helper
 *
 * @author eugene
 * @version 1.2 Last Updated 28 September 2010 / MarcAndre
 */
class Exoof_View_Helper_FormFile extends Exoof_View_Helper_AbstractFormField {
    //put your code here

    /**
     * Inits Form String object
     * @param string $name Name of the field
     * @param string $label Label of the field
     * @param string $value
     * @param array $options
     * @return Exoof_View_Helper_AbstractFormField
     */
    public function FormFile($name, $label, $value=null, $options=array()) {
        return $this->_init($name, $label, $value, $options);
    }

    public function  __toString() {
        $attributes = parent::__toString();

        $format = '';
        if (!empty($this->_label))
            $format .= sprintf("<label %s for='%s' id='label_%s'>%s</label>",
                    ($this->_required)?"required='true'":'',
                    $this->_id,
                    $this->_id,
                    $this->_label);

        $format .= sprintf("<input type='file' name='%s' id='%s' %s title='%s' %s />",
                $this->_name,
                $this->_id,
                ($this->_required)?"required='true'":'',
                $this->_title,
                $attributes
        );
        if (!empty($this->_value)) {
                $addPath = (Exoof_Factory::factory('AdminLoaded')?'../':'').Exoof_Factory::factory('Config')->paths->uploadPath.'/';
                $format .= sprintf("&nbsp;<a href='%s'>%s</a>",$addPath.$this->_value,basename($this->_value));
        }
        return $format;
    }
}