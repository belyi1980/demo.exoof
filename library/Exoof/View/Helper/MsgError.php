<?php
/**
 * DialogButton helper
 *
 * @author eugene
 * @version 1.0
 * @uses Zend_View_Helper_Abstract
 */
class Exoof_View_Helper_MsgError {

    public function msgError($string) {
        if ($string)
        {
        	$msg = "<div class=\"ui-widget\">
					<div class=\"ui-state-error ui-corner-all\" style=\"padding: 0pt 0.7em; margin-top: 20px;\">
					<p>
					<span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: 0.3em;\"/>
					".$string."
					</p>
					</div>
					</div>";
            return $msg;
        }
        return null;
    }
}
