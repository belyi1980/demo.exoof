<?php
/**
 * Description of FormString
 *
 * @author eugene
 * @version 1.4 Last Updated : 7 July 2010
 */
class Exoof_View_Helper_FormSelect extends Exoof_View_Helper_AbstractFormField {
    //put your code here

    protected $selected = null;
    protected $options = array();
    protected $emptyOption = null;
    private $_autoTranslate = false;
    /**
     * Inits Form String object
     * @param string $name Name of the field
     * @param string $label Label of the field
     * @param string $value
     * @param array $options
     * @return Exoof_View_Helper_AbstractFormField
     */
    public function FormSelect($name, $label, $value=null, $options=array()) {
        if (is_array($value))
            $this->options = $value;
        return $this->_init($name, $label, $value, $options);
    }

    public function setOptions($options) {
        $this->options = $options;
        return $this;
    }

    public function setAutoTranslate($set = true) {
        $this->_autoTranslate = $set?true:false;
        return $this;
    }

    public function loadExLangOptions($langIniFile) {
        $lang   = Exoof_Translate::getInstance()->getCurrentLang();
        $cfg    = Exoof_Factory::factory('Config');
        $values = parse_ini_file($cfg->paths->langsPath.'/'.$lang.'/'.$langIniFile, false);
        $this->setOptions($values);
        return $this;
    }

    public function setSelected($selectedValue) {
        $this->selected = $selectedValue;
        return $this;
    }

    public function setDefaultValue($value) {
        return $this->setSelected($value);
    }

    public function setEmptyOption($string='-') {
        $this->emptyOption = $this->_translate->translate($string);
        return $this;
    }

    public function  __toString() {

        $attributes = parent::__toString();

        $format = '';
        if (!empty($this->_label))
            $format .= sprintf("<label %s for='%s' id='label_%s'>%s</label>",
                    ($this->_required)?"required='true'":'',
                    $this->_id,
                    $this->_id,
                    $this->_label);

        $format .= sprintf("<select name='%s' id='%s' %s title='%s' %s >",
                $this->_name,
                $this->_id,
                ($this->_required)?"required='true'":'',
                $this->_title,
                $attributes
        );

        if ($this->emptyOption!=null)
            $format .= sprintf("<option value='%s'>%s</option>",
                    '',
                    ($this->_autoTranslate)?Exoof_Translate::getInstance()->translate($this->emptyOption):$this->emptyOption
            );

        if (sizeof($this->options) && is_array($this->options))
            foreach ($this->options as $key => $val) {
                if (is_array($val)) {
                    $format .= sprintf("<optgroup label='%s'>",htmlspecialchars(stripslashes($key),ENT_QUOTES));
                    foreach ($val as $key2 => $val2) {

                        $val2 = ($this->_autoTranslate)?(Exoof_Translate::getInstance()->translate($val2)):$val2;
                        $format .= sprintf("<option value='%s' %s>%s</option>",
                                htmlspecialchars(stripslashes($key2),ENT_QUOTES),
                                ($key2 == $this->selected)?"selected='selected'":'',
                                htmlspecialchars(stripslashes($val2),ENT_QUOTES)
                        );
                    }
                    $format .= "</optgroup>";

                } else {
                    $val = ($this->_autoTranslate)?(Exoof_Translate::getInstance()->translate($val)):$val;
                    $format .= sprintf("<option value='%s' %s>%s</option>",
                            htmlspecialchars(stripslashes($key),ENT_QUOTES),
                            ($key == $this->selected)?"selected='selected'":'',
                            htmlspecialchars(stripslashes($val),ENT_QUOTES)
                    );
                }
            }
        return $format.'</select>';
    }

}
?>
