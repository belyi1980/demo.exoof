<?php
/**
 * Description of FormString
 *
 * @author eugene
 * @version 1.1 Last Updated 7 July 2010
 */
class Exoof_View_Helper_FormHTML extends Exoof_View_Helper_AbstractFormField {
    //put your code here

    protected $_attributes = array(
            'class'=>'ui-widget-content ui-corner-all',
            'cols'=>30,
            'rows'=>6,
            'wysiwyg'=>'true'
        );

    /**
     * Inits Form HTML object
     * @param string $name Name of the field
     * @param string $label Label of the field
     * @param string $value
     * @param array $options
     * @return Exoof_View_Helper_AbstractFormField
     */
    public function FormHTML($name, $label, $value=null, $options=array()) {
        return $this->_init($name, $label, $value, $options);
    }

    public function  __toString() {
        $attributes = parent::__toString();
        
        return sprintf("<label %s for='%s' id='label_%s'>%s</label><a href='javascript:void;' onclick='%s' class='small'>HTML/Text</a><br/><textarea name='%s' id='%s' %s title='%s' %s>%s</textarea>",
                        ($this->_required)?"required='true'":'',
                        $this->_id,
                        $this->_id,
                        $this->_label,
                        htmlspecialchars(stripslashes("$('#".$this->_id."').tinymce();return false;"),ENT_QUOTES),
                        $this->_name,
                        $this->_id,
                        ($this->_required)?"required='true'":'',
                        $this->_title,
                        $attributes,
                        htmlspecialchars(stripslashes($this->_value),ENT_QUOTES)
                        );
    }

}
?>
