<?php
/**
 * Description of Request
 *
 * @author eugene
 */
class Exoof_View_Helper_Request {
    
    public function request($varName, $default=null) {
        return Exoof_Factory::factory('Request')->getParam($varName, $default);
    }

}
?>
