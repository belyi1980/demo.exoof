<?php
/**
 * Form Submit
 *
 * @author eugene
 */
class Exoof_View_Helper_FormSubmit extends Exoof_View_Helper_AbstractFormField {
    //put your code here

    protected $_attributes = array(
            'class'=>'ui-state-default ui-corner-all',
        );
    /**
     * Inits Form String object
     * @param string $name Name of the field
     * @param string $label Label of the field
     * @param string $value
     * @param array $options
     * @return Exoof_View_Helper_AbstractFormField
     */
    public function FormSubmit($label, $options=array(), $name='_submit') {
        return $this->_init($name, $label, null, $options);
    }

    public function  __toString() {
        $attributes = parent::__toString();

        return sprintf("<label>&nbsp;</label><input type='submit' name='%s' id='%s' value='%s' title='%s' %s />",
                        $this->_name,
                        $this->_name,
                        $this->_label,
                        $this->_title,
                        $attributes
                        );
    }
}
?>
