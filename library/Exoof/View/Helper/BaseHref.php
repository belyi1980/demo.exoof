<?php
/**
 * baseHref View Helper
 *
 * @author eugene
 */
class Exoof_View_Helper_Basehref {

    public function Basehref() {
        $baseHref  = (isset($_SERVER['HTTPS']))?'https://':'http://';
        $baseHref .= $_SERVER['SERVER_NAME'];
        $baseHref .= ($_SERVER['SERVER_PORT']!='80' && $_SERVER['SERVER_PORT']!='443')?':'.$_SERVER['SERVER_PORT']:null;
        if (strlen(dirname($_SERVER['PHP_SELF']))>1)
            $baseHref .= dirname($_SERVER['PHP_SELF']);
        $baseHref .= '/';

        return $baseHref;
    }

}
?>
