<?php
/**
 * Option helper
 *
 * @author Eugene
 * @version 1.1 Last Updated : 29 July 2010
 */
class Exoof_View_Helper_Option {

    public function option($variable = null) {
        static $options;
        if (isset($options[$variable]))
            return stripslashes($options[$variable]);
        else {
             $opt = new Option();
             $found = $opt->search($variable);
             if (!$found) {
                 //AUTO CREATE
                 $opt->opt_date_added = time();
                 $opt->opt_variable   = $variable;
                 $opt->opt_title      = $variable;
                 $opt->opt_type       = 'text';
                 $opt->save();
             }
             if ($opt->opt_value) {
                $options[$variable] = $opt->opt_value;
                return stripslashes($options[$variable]);
            }
        }

        return '<!-- Undefined `'.$variable.'` -->';
    }
}
