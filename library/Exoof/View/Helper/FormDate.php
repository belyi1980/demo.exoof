<?php
/**
 * Description of FormString
 *
 * @author eugene
 * @version 1.1 Last Updated : 3 September 2010 Cougar
 */
class Exoof_View_Helper_FormDate extends Exoof_View_Helper_AbstractFormField {
    //put your code here
    protected $emptyOption = null;

    /**
     * Inits Form String object
     * @param string $name Name of the field
     * @param string $label Label of the field
     * @param string $value
     * @param array $options
     * @return Exoof_View_Helper_AbstractFormField
     */
    public function FormDate($name, $label, $value=null, $options=array()) {
        if (is_array($value))
            $this->options = $value;
        return $this->_init($name, $label, $value, $options);
    }

    public function setEmptyOption($string='-') {
        $this->emptyOption = $this->_translate->translate($string);
        return $this;
    }

    public function  __toString() {

        $valueExist = ($this->_value!=null)?true:false;
        $value = ($valueExist)?($this->_value):(time());
        $attributes = parent::__toString();

        $output = '';
        if (!empty($this->_label))
        $output  .= sprintf("<label required='%s' for='%s' id='label_%s'>%s</label>",
                ($this->_required)?'true':'false',
                $this->_id,
                $this->_id,
                $this->_label);

        $output .= "<div class='dateSelect'>";

        $output .= sprintf("<select name='%s[day]' id='%s_day' class='day' required='%s' title='%s'>",
                $this->_name,
                $this->_id,
                ($this->_required)?'true':'false',
                $this->_title
        );
        if ($this->emptyOption)
            $output .= "<option value=''>".$this->emptyOption."</option>";
        for($d=1 ; $d<=31 ; $d++)
            $output .= "<option value='{$d}' ".($valueExist && ($d==date('j',$value))?"selected":"").">".str_pad($d, 2, '0',STR_PAD_LEFT)."</option>";
        $output .= "</select>";

        $output .= sprintf("<select name='%s[month]' id='%s_month' class='month' required='%s' title='%s'>",
                $this->_name,
                $this->_id,
                ($this->_required)?'true':'false',
                $this->_title
        );
        if ($this->emptyOption)
            $output .= "<option value=''>".$this->emptyOption."</option>";
        for($m=1 ; $m<=12 ; $m++)
            $output .= "<option value='{$m}' ".($valueExist && ($m==date('n',$value))?"selected":"").">".str_pad($m, 2, '0',STR_PAD_LEFT)."</option>";
        $output .= "</select>";


        $output .= sprintf("<select name='%s[year]' id='%s_year' class='year' required='%s' title='%s'>",
                $this->_name,
                $this->_id,
                ($this->_required)?'true':'false',
                $this->_title
        );
        if ($this->emptyOption)
            $output .= "<option value=''>".$this->emptyOption."</option>";
        for($y=1920 ; $y<=(date('Y')+1) ; $y++)
            $output .= "<option value='{$y}' ".($valueExist && ($y==date('Y',$value))?"selected":"").">{$y}</option>";
        $output .= "</select>";

        /*if (sizeof($this->options) && is_array($this->options))
            foreach ($this->options as $key => $val) {
                if (is_array($val)) {
                    $output .= sprintf("<optgroup label='%s'>",htmlspecialchars(stripslashes($key),ENT_QUOTES));
                    foreach ($val as $key2 => $val2) {
                        $output .= sprintf("<option value='%s' %s>%s</option>",
                                htmlspecialchars(stripslashes($key2),ENT_QUOTES),
                                ($key2 == $this->selected)?"selected='selected'":'',
                                htmlspecialchars(stripslashes($val2),ENT_QUOTES)
                        );
                    }
                    $output .= "</optgroup>";

                } else {
                    $output .= sprintf("<option value='%s' %s>%s</option>",
                            htmlspecialchars(stripslashes($key),ENT_QUOTES),
                            ($key == $this->selected)?"selected='selected'":'',
                            htmlspecialchars(stripslashes($val),ENT_QUOTES)
                    );
                }
            }*/
        return $output."</div>";
    }

}
?>