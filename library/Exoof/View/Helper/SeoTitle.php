<?php
/**
 * seoTitle view helper
 *
 * @author eugene
 */
class Exoof_View_Helper_SeoTitle {
    
    private $_title = null;
//    public $cacheable = false;

    public function SeoTitle($title=null) {
        if ($title && !$this->_title) {
            $this->_title = $title;
            $return = $this->_title;
        } elseif ($title && $this->_title) {
            $prevTitle = $this->_title;
            $this->_title = $title;
            $return = $prevTitle;
        } else
            $return = $this->_title;

        return htmlspecialchars(stripslashes(strip_tags($return)));
    }

}
?>
