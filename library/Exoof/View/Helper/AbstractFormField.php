<?php
/**
 * formString Helper
 *
 * @author eugene
 * @version 1.0 Last Updated : 22 April 2010
 */
abstract class Exoof_View_Helper_AbstractFormField {

    public $cacheable = false;
    /**
     * @var boolean Is field required or not
     */
    protected $_required = false;

    /**
     * @var string Label of the field
     */
    protected $_label    = null;

    /**
     * @var string Name of the field
     */
    protected $_name     = null;

    /**
     * @var string Id of the field
     */
    protected $_id       = null;

    /**
     * @var mixed Default value of the field
     */
    protected $_value    = null;

    /**
     * @var string Title of the field (title html attribute)
     */
    protected $_title    = null;
    /**
     * Other HTML attributes
     * @var array Array of attributes
     * @example array('class' => 'cssClass', 'onclick'=>'alert(something)');
     */
    protected $_attributes = array('class'=>'ui-widget-content ui-corner-all');

    /**
     * Translation object
     * @var Exoof_Translate
     */
    protected $_translate;
    /**
     * Inits Form Field object
     * @param string $name Name of the field
     * @param string $label Label of the field
     * @param string $value
     * @param array $options
     * @return Exoof_View_Helper_AbstractFormField
     */
    protected function _init($name, $label, $value=null, $options=array()) {
        $isAdmin          = Exoof_Factory::factory('AdminLoaded')?true:false;
        if (!$isAdmin)
            $this->_attributes = array();
        $this->_translate = Exoof_Factory::factory('Translate');
        $this->setName($name)
             ->setLabel($label)
             ->setDefaultValue($value);

        $_attributes = array();
        if (sizeof($options))
            foreach ($options as $property => $value) {
                $rproperty = '_'.$property;
                if (property_exists($this, $rproperty))
                    $this->$rproperty = $value;
                else
                    $_attributes[$property] = $value;
            }
        $this->setAttributes($_attributes);
        return $this;
    }

    /**
     * Sets required flag
     * @param boolean $required
     * @return Exoof_View_Helper_AbstractFormField
     */
    public function setRequired($required=false) {
        $this->_required = $required;
        return $this;
    }


    /**
     * Sets disabled flag
     * @param boolean $disabled
     * @return Exoof_View_Helper_AbstractFormField
     */
    public function setDisabled($disabled=true) {
        if ($disabled)
            $this->_attributes['disabled'] = 'true';
        else
            unset($this->_attributes['disabled']);
        return $this;
    }


    /**
     * Sets readonly flag
     * @param boolean $readonly
     * @return Exoof_View_Helper_AbstractFormField
     */
    public function setReadOnly($readonly=true) {
        if ($readonly)
            $this->_attributes['readonly'] = 'true';
        else
            unset($this->_attributes['readonly']);
        return $this;
    }

    /**
     * Sets attributes
     * @param array $attributes
     * @return Exoof_View_Helper_AbstractFormField
     * @example array('class' => 'cssClass', 'onclick'=>'alert(something)');*
     */
    public function setAttributes($attributes=array()) {
        if (sizeof($attributes))
            foreach ($attributes as $key => $val) {
                $this->_attributes[$key] = $val;
            }
        
        return $this;
    }

    /**
     * Adds one pair attribute
     * @param string|array $attrName Key of the attribute or assoc. array of pair
     * @param any $attrValue Value of the attribute
     * @return Exoof_View_Helper_AbstractFormField
     * @example addAttribute('size',30) OR addAttribute(array('size'=>'30))
     */
    public function addAttribute($attrName, $attrValue=null) {
        if (!$attrValue && is_array($attrName)) {
            $attrValue = @array_pop($attrName);
            $attrName  = @array_shift($attrName);
        }

        $this->_attributes[$attrName] = $attrValue;
        return $this;
    }

    /**
     * Sets label
     * @param string $label
     * @return Exoof_View_Helper_AbstractFormField
     */
    public function setLabel($label) {
        if ($label != $this->_label)
            $this->_label = $this->_translate->translate($label);
        return $this;
    }

    /**
     * Sets id
     * @param string $id
     * @return Exoof_View_Helper_AbstractFormField
     */
    public function setId($id) {
        $this->_id = $id;
        return $this;
    }

    /**
     * Sets title
     * @param string $title
     * @return Exoof_View_Helper_AbstractFormField
     */
    public function setTitle($title) {
        if (!empty($this->_title) && $title != $this->_title)
            $this->_title = $this->_translate->translate($title);
        elseif($title == $this->_label)
            $this->_title = $this->_label;
        else
            $this->_title = $title;
        return $this;
    }

    /**
     * Sets name
     * @param string $name
     * @return Exoof_View_Helper_AbstractFormField
     */
    public function setName($name) {
        $this->_name = $name;
        return $this;
    }

    /**
     * Sets default value
     * @param mixed $value
     * @return Exoof_View_Helper_AbstractFormField
     */
    public function setDefaultValue($value) {
        $this->_value = $value;
        return $this;
    }

    /**
     * Renders field into HTML
     * @abstract
     */
    public function  __toString() {
        $postedValue = Exoof_Factory::factory('Request')->getParam($this->_name);
        if ($postedValue)
            $this->setDefaultValue($postedValue);
        if (!$this->_id)
            $this->setId($this->_name);
        if (!$this->_title)
            $this->setTitle($this->_label);

        $attributes = '';
        if (sizeof($this->_attributes))
            foreach ($this->_attributes as $attr => $value) {
                $attributes .= sprintf(" %s='%s'",$attr,htmlspecialchars(stripslashes($value),ENT_QUOTES));
            }
        return $attributes;
    }
}
?>
