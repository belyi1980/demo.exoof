<?php
/**
 * DialogButton helper
 *
 * @author Eugene Belyaev
 * @version 1.1 / Last Updated : 20 October 2010 / Project: photoconcurs / Author: Eugene Belyaev
 */
class Exoof_View_Helper_DialogButton {

    public function dialogbutton($action, $title, $icon) {
        $title = Exoof_Translate::getInstance()->translate($title);
        if ($action)
        {
       	   $var = "<a href='".$action."' title='".$title."' class='btn-icon ui-state-default ui-corner-all' role='button'>".
                    "<span class='ui-icon ui-icon-".$icon."'></span>".(strlen($title)?$title.'&nbsp;':'')."</a>";
           return $var;
        } 
        return null;
    }
}
