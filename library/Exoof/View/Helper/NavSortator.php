<?php
/**
 * NavSortator - displays box for choosing sort field
 *
 * @author eugene
 */
class Exoof_View_Helper_NavSortator {

   private $_autoTranslate = false;

   private $_options = array();

   private $_link = null;

   private $_ajaxOptions = array();

   public function setAutoTranslate($set = true) {
        $this->_autoTranslate = $set?true:false;
        return $this;
    }

   public function navSortator($options=null, $link=null, $ajaxOptions=null) {
       $this->_link = $link;
       $this->_ajaxOptions = $ajaxOptions;
       $this->_options = $options;
        return $this;
   }

   public function addItem($url, $label) {
       $this->_options[$url] = $label;
       return $this;
   }

   public function  __toString() {
        $id  = uniqid('sortator');
        $out = "<div class='navSortator clear' id='".$id."'>";
        $current = (isset($_GET['_s']))?($_GET['_s']):null;
        $dest    = (isset($_GET['_d']))?($_GET['_d']):null;

        $link = ($this->_link)?$this->_link:$_SERVER['REQUEST_URI'];
        if (preg_match('/^\?/',$link)) {
            $url = parse_url($_SERVER['REQUEST_URI']);
            $link = $url['path'].$link;
        }

        $lparts = parse_url($link);
        $link  = $lparts['path'];
        if (isset($lparts['query'])) {
            $query = preg_replace("/(&?_s=[^&]*)/", "", $lparts['query']);
            $query = trim($query,'&');
            $link  .= (!empty($query))?'?':'';
            $link  .= $query;
        }
        $add  = (strstr($link,'?'))?'&_s=':'?_s=';

        if (sizeof($this->_options) && is_array($this->_options))
            foreach($this->_options as $sfield => $slabel) {
                if ($this->_autoTranslate == true)
                        $slabel = Exoof_Translate::getInstance()->translate($slabel);
                $out .= "<a href='".$link.$add.($sfield)."' ".(($sfield==$current)?"class='active'":'').">".$slabel."</a>&nbsp;";
            }

        $link2  = $lparts['path'];
        if (isset($lparts['query'])) {
            $query = preg_replace("/(&?_d=[ud]*)/", "", $lparts['query']);
            $query = trim($query,'&');
            $link2  .= (!empty($query))?'?':'';
            $link2  .= $query;
        }
        $add2  = (strstr($link2,'?'))?'&_d=':'?_d=';
        switch($dest) {
            default:
            case 'u' :
                $class   = 'up';
                $newDest = 'd';
                break;
            case 'd' :
                $class   = 'down';
                $newDest = 'u';
                break;
            /*default:
                $class   = 'updown';
                $newDest = 'u';
                break;*/
        }
        $out .= "<a href='".$link2.$add2.$newDest."' class='".$class."'></a>&nbsp;";
        $out .= "</div>";
        if (isset($this->_ajaxOptions['id']))
        {
            $out  .= "<script>$('#".$id." a').click(function(){\$('#".$this->_ajaxOptions['id']."').load($(this).attr('href'));return false;});</script>";
        }

        return $out;
    }
    //put your code here
}
?>
