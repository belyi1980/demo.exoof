<?php
/**
 * Normalize helper
 *
 * @author eugene
 * @version 1.0 
 */
class Exoof_View_Helper_Normalize {

    public function normalize($value = null, $mode=1) {
        if ($value)
        {
            if ( ($mode & NORM_STRIP_SLASHES)>0)
                $value = stripslashes($value);
            if ( ($mode & NORM_ENTITIES)>0)
                $value = htmlentities($value);
            
            return $value;
        } 
        return null;
    }
}
