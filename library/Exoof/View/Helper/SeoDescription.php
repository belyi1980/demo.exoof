<?php
/**
 * @author eugene
 * @version 1.1 / Last Updated: 2 November 2010 / Project: JD / Author: Eugene Belyaev
 */
class Exoof_View_Helper_SeoDescription {
    
    private $_description = null;
//    public $cacheable = false;

    public function Seodescription($description=null) {
        if ($description && !$this->_description) {
            $this->_description = $description;
            $return =  $this->_description;
        } elseif ($description && $this->_description) {
            $prevdescription = $this->_description;
            $this->_description = $description;
            $return =  $prevdescription;
        } else
            $return =  $this->_description;

        return substr(htmlspecialchars(str_replace("\n",'',stripslashes(strip_tags($return)))),0,156);
    }

}
?>
