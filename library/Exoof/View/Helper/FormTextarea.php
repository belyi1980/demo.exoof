<?php
/**
 * Description of FormString
 *
 * @author eugene
 */
class Exoof_View_Helper_FormTextarea extends Exoof_View_Helper_AbstractFormField {
    //put your code here

    protected $_attributes = array(
            'class'=>'ui-widget-content ui-corner-all',
            'cols'=>30,
            'rows'=>6
        );

    /**
     * Inits Form String object
     * @param string $name Name of the field
     * @param string $label Label of the field
     * @param string $value
     * @param array $options
     * @return Exoof_View_Helper_AbstractFormField
     */
    public function FormTextarea($name, $label, $value=null, $options=array()) {
        return $this->_init($name, $label, $value, $options);
    }

    public function  __toString() {
        $attributes = parent::__toString();
        $format = '';
        if (!empty($this->_label))
            $format .= sprintf("<label %s for='%s' id='label_%s'>%s</label>",
                    ($this->_required)?"required='true'":'',
                    $this->_id,
                    $this->_id,
                    $this->_label);

        
        $format .= sprintf("<textarea name='%s' id='%s' %s title='%s' %s>%s</textarea>",
                        $this->_name,
                        $this->_id,
                        ($this->_required)?"required='true'":'',
                        $this->_title,
                        $attributes,
                        htmlspecialchars(stripslashes($this->_value),ENT_QUOTES)
                        );

        return $format;
    }

}
?>
