<?php
/**
 * Partial Helper
 * @author Eugene
 * @version 1.2 Updated: 06 August 2010 Cougarmyth
 */
class Exoof_View_Helper_Partial extends Exoof_View {

    public function partial($template, $data=null) {
        $template .= (substr($template,-6,6)=='.phtml')?'':'.phtml';
        $this->assignedVars = Exoof_View::getInstance()->getVars();
        if (sizeof($data))
        foreach ($data as $key=>$value)  {
            if (!is_numeric($key))
                $this->assign($key, $value);
        }
        include $template;
    }

}
?>
