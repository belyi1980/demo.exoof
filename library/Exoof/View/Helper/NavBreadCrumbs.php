<?php
/**
 * Bread Crumbs Helper
 *
 * @author eugene
 * @version 1.2 / Last Updated: 28 October 2010 / Proejct: VEULearning / Author: Eugene Belyaev
 */
class Exoof_View_Helper_NavBreadCrumbs extends Exoof_View_Helper_AbstractNavigation {

//    private $_separator = '&rarr;';
    private $_showFirst = true;

    private $_autoTranslate = false;

    public function navBreadCrumbs() {
        $this->activeLevel();
        return $this;
    }

    public function setAutoTranslate($set = true) {
        $this->_autoTranslate = $set?true:false;
        return $this;
    }

//    public function setSeparator($separator = '&rarr;') {
//        $this->_separator = $separator;
//        return $this;
//    }

    public function hideFirst() {
        $this->_showFirst = false;
        return $this;
    }

    public function addItem($link,$label=null) {
        if (is_array($link) || $label==null) {
        $item['uri']   = key($link);
        $item['label'] = $link[$item['uri']];
        } else  {
            $item['uri']   = $link;
            $item['label'] = $label;
        }
        if ($this->_autoTranslate) {
            $item['label'] = Exoof_Translate::getInstance()->translate($label);
            $lurl = Exoof_Lang::getLangUrl();
            if ($lurl != null && !preg_match("|^".addslashes($item['uri'])."|",$item['uri']))
                $item['uri'] = $lurl.$item['uri'];
        }
        $this->_path[] = $item;
        return $this;
    }

    public function  __toString() {
        $output = "<ul class='navBreadCrumbs clear'>\n";
        $start = ($this->_showFirst)?0:1;
        for ($i=0,$mi=sizeof($this->_path);$i<$mi;$i++) {
            $label = htmlspecialchars($this->_path[$i]['label']);
            $output .= sprintf("<li%s><a href='%s' title='%s'>%s</a></li>\n", ($i == ($mi-1))?" class='last'":(!$i?" class='first'":''),$this->_path[$i]['uri'],$label, $label);
        }
        $output .= "</ul>\n";
        return $output;
    }

}