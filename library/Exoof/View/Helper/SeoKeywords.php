<?php
/**
 * seokeywords view helper
 *
 * @author eugene
 * @version 1.1 / Last Updated: 2 November 2010 / Project: JD / Author: Eugene Belyaev
 */
class Exoof_View_Helper_SeoKeywords {
    
    private $_keywords = null;
//    public $cacheable = false;

    public function SeoKeywords($keywords=null) {
        if ($keywords && !$this->_keywords) {
            $this->_keywords = $keywords;
            $return = $this->_keywords;
        } elseif ($keywords && $this->_keywords) {
            $prevkeywords = $this->_keywords;
            $this->_keywords = $keywords;
            $return = $prevkeywords;
        } else
            $return = $this->_keywords;

        return substr(htmlspecialchars(str_replace("\n",'',stripslashes(strip_tags($return)))),0,256);
    }

}
?>
