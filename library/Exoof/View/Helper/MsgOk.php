<?php
/**
 * DialogButton helper
 *
 * @author eugene
 * @version 1.0
 * @uses Zend_View_Helper_Abstract
 */
class Exoof_View_Helper_MsgOk {

    public function msgOk($string) {
        if ($string)
        {
        	$msg = "<div class=\"ui-widget\">
					<div class=\"ui-state-highlight ui-corner-all\" style=\"padding: 0pt 0.7em; margin-top: 20px;\">
					<p>
					<span class=\"ui-icon ui-icon-info\" style=\"float: left; margin-right: 0.3em;\"></span>
					".$string."
					</p>
					</div>
					</div>";
            return $msg;
        }
        return null;
    }
}
