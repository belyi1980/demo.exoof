<?php
/**
 * Description of FormString
 *
 * @author eugene
 */
class Exoof_View_Helper_FormPassword extends Exoof_View_Helper_AbstractFormField {
    //put your code here
    private $showShowPass = false;

    private $visible = false;
    
    /**
     * Inits Form Password object
     * @param string $name Name of the field
     * @param string $label Label of the field
     * @param string $value
     * @param array $options
     * @return Exoof_View_Helper_AbstractFormField
     */
    public function FormPassword($name, $label, $value=null, $options=array()) {
        return $this->_init($name, $label, $value, $options);
    }

    public function setShowPasswordLink($setFlag=true) {
        $this->showShowPass = ($setFlag)?true:false;
        return $this;
    }

    public function setVisible($visible=true) {
        $this->visible = $visible;
        return $this;
    }

    /**
     *
     * @param <type> $type
     * @return <type>
     * type=0 Strong password (upper+vowels+consonants+digits)
     * type=1 Normal password (vowels+consonants)
     * type=2 Digits only password
     */
    public function setDefaultGenerated($type=2) {
        if (!$this->_value)
                {
                    $vowels = 'aeui';
                    $consonants = 'bdghjmnpqrstvz';
                    $digits = '23456789';

                    if ($type == 2)
                        $vowels = $consonants = $digits;

                    $alt = 0;
                    for ($i = 0; $i < 6; $i++) {
                            if ($alt == 1) {
                                    $this->_value .= $vowels[(rand() % strlen($vowels))];
                                    $alt = 0;
                            } else {
                                    $this->_value .= $consonants[(rand() % strlen($consonants))];
                                    $alt = 1;
                            }
                    }
                    if ($type < 2)
                        $this->_value .= $digits[(rand() % strlen($digits))].$digits[(rand() % strlen($digits))];
                    if ($type == 0)
                        $this->_value  = ucfirst($this->_value);
                }
        return $this;
    }

    public function  __toString() {
        $attributes = parent::__toString();

        $showLink = ($this->showShowPass?"&nbsp;<span style='cursor:pointer;' onclick='alert($(\"#".$this->_id."\").val());'>&harr;</span>":'');

        return sprintf("<label %s for='%s' id='label_%s'>%s</label><input type='%s' name='%s' id='%s' value='%s' %s title='%s' %s/>".$showLink,
                        ($this->_required)?"required='true'":'',
                        $this->_id,
                        $this->_id,
                        $this->_label,
                        (($this->visible)?'text':'password'),
                        $this->_name,
                        $this->_id,
                        htmlspecialchars(stripslashes($this->_value),ENT_QUOTES),
                        ($this->_required)?"required='true'":'',
                        $this->_title,
                        $attributes
                        );
    }

}
?>
