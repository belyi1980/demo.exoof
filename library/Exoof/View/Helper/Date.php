<?php
/**
 * Date View Helper
 *
 * @author eugene
 */
class Exoof_View_Helper_Date {

    public function date($time) {
        $lang = Exoof_Translate::getInstance()->getCurrentLang();
        @setlocale(LC_TIME, strtolower($lang).'_'.strtoupper($lang));
        return strftime('%m/%d/%Y', $time);
    }
}
?>
