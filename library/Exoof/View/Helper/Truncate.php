<?php
/**
 * Truncate helper
 *
 * @author eugene
 * @version 1.0
 */
class Exoof_View_Helper_Truncate {

    public function truncate($val = null, $length=null, $trailing = null) {
        mb_internal_encoding('UTF-8');

        $length  = ($length)?$length:150;
        $trailing= ($trailing)?$trailing:"…";
        
        $length  -= mb_strlen($trailing);
        if (mb_strlen($val)> $length)
            return mb_substr($val,0,$length).$trailing;
        else
            return $val;
    }
}