<?php
class Exoof_View_Helper_IsLogged {

    public function isLogged() {
        $user = Exoof_Session::get('user');
        if (isset($user['u_id'])) {
            if ($user['u_exp_pay_date'] < time())
                return -1;
            else
                return $user['u_id'];
        } else
            return 0;
    }

}
?>
