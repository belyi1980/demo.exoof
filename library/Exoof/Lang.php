<?php
/**
 * Lang
 *
 * @author eugene
 */
class Exoof_Lang {

    static public function getLangs() {
        return Exoof_Factory::factory('Translate')->getLangs();
    }

    static public function getLang() {
        return Exoof_Factory::factory('Translate')->getCurrentLang();
    }


    static public function getLangUrl() {
        $config = Exoof_Factory::factory('Config');
        if ($config->multilang->multiple == 'yes') {
            $default = $config->multilang->default;
            $current = Exoof_Lang::getLang();
            if ($default != 'auto') {
                if ($current == $default && $config->multilang->defaultUrlPrefix != 'yes') //if default is RU and now is RU - empty lang prefix returns
                    return null;
                else
                    return $current.'/';
            } else
                return $current.'/';
        } else
            return null;
    }

}
?>
