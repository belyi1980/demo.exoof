<?php
/**
 * Exoof Registry
 *
 * @author eugene
 */

class Exoof_Registry {

    /**
     * @var Exoof_Factory
     */
    private static $instance;

    /**
     * @var array
     */
    private $_storage = array();

    /**
     * Constructor is locked due to Singleton pattern
     */
    private function __construct() {
        
    }

    /**
     * Returns Factory object
     * @return Exoof_Factory
     */
    public static function getInstance() {
        if (!is_object(self::$instance) || !(self::$instance instanceof Exoof_Registry))
            self::$instance = new Exoof_Registry();
        return self::$instance;
    }

    /*
     * Set variable`s value in registry
     *
     * @param string Name of the variable placed in the registry
     * @return boolean
     */
    public static function set($name, $value=null) {
        $self = self::getInstance();

        $self->_storage[$name] = $value;
        return true;
    }

    /**
     * Returns variable`s value if exists
     * @param string $name Name of the variable placed in the registry
     * @return mixed
     */
    public static function get($name) {
        $self = self::getInstance();

        if (array_key_exists($name, $self->_storage))
            return $self->_storage[$name];
    }

}