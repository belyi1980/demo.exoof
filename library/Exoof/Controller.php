<?php
/**
 * Exoof Controller
 *
 * @author eugene
 * @version 1.7 / Last Updated : 16 November 2010 / Project: Kings / Author: Eugene Belyaev
 */
class Exoof_Controller {
    /**
     * @var Exoof_Request Request Object
     */
    public $request;

    /**
     * @var Exoof_Config Config object
     */
    protected $config;

    /**
     * @var Exoof_Translate Translate Object
     */
    protected $translation;
    /**
     * @var Exoof_View View object
     */
    public $view = null;

    /**
     * @var boolean Flag - is controller has been rendered already or not
     */
    protected $isRendered = false;
    /**
     * @var string Layout to render view in
     */
    protected $_layout = 'main.phtml';

    protected $_ctrlInfo;
    /**
     * @var boolean Flag: layout disabled or not
     */
    private $_layoutDisabled = false;

    protected $controllersPath = null;

    public function __construct($controller, $action, $ctrlPath, $view=null, $overrideRender=true) {
        $this->request = Exoof_Factory::factory('Request');
        $this->config   = Exoof_Factory::factory('Config');
        $this->controllersPath = $this->config->paths->appControllers;
        $this->translation   = Exoof_Translate::getInstance();
        $this->view     = Exoof_View::getInstance();
        if ($overrideRender) {
            $this->view->setRenderScriptsPath(dirname($ctrlPath).'/views/');
            $this->view->setRenderScript($controller.'/'.$action.'.phtml');
        }
        $this->_ctrlInfo = array($controller, $action);
        if ($this->isAjaxRequest())
            $this->disableLayout();
    }

    public function init() {
    }

    public function render() {
        $this->view->render();
    }

    public function setRenderScript($viewScript) {
        $this->view->setRenderScript($viewScript);
    }

    public function disableLayout() {
        $this->_layoutDisabled = true;
    }

    public function enableLayout() {
        $this->_layoutDisabled = false;
    }

    public function isLayoutDisabled() {
        return $this->_layoutDisabled;
    }

    public function setLayout($layout) {
        if (substr($layout,-6,6) != '.phtml')
            $layout = $layout.'.phtml';
        $this->_layout = $layout;
    }

    public function getLayout() {
        return $this->_layout;
    }

    public function call($controller, $method='indexAction', $params=array()) {

        $controllerObj = $controller;
        if (!preg_match("/Controller$/",$controller))
            $controllerObj .= 'Controller';

        include $this->controllersPath.'/'.$controllerObj.'.php';
        $ctrl  = new $controllerObj($controller,$method,$this->controllersPath, false);
        $ctrl->init();
        $ctrl->disableLayout();

        if (method_exists($ctrl, $method)) {
            return call_user_func_array(array($ctrl, $method),$params);
        }
        else
            throw new Exception('Method `'.$controllerObj.' &rarr; '.$method.'()` doesn`t exist ');
    }

    public function forward($controller, $action='index', $render=false) {

        $actionMethod  = $action;
        if (!preg_match("/Action$/",$actionMethod))
            $actionMethod .= 'Action';

        if (!is_object($controller)) {
            $controllerObj = ucfirst($controller);
            if (!preg_match("/Controller$/",$controller))
                $controllerObj .= 'Controller';

            include_once $this->controllersPath.'/'.$controllerObj.'.php';
            $ctrl  = new $controllerObj($controller,$action,$this->controllersPath);
        } else {
            $ctrl = $controller;
            $controller = str_replace('Controller','',get_class($ctrl));
            $ctrl->setRenderScript($controller.'/'.$action.'.phtml');
        }


        $ctrl->init();
        $ctrl->setRenderScript($controller.'/'.$action.'.phtml');
        $ctrl->$actionMethod();
        if ($render) {
            $ctrl->disableLayout();
            $ctrl->render();
        }
        $this->isRendered = $render;
    }

    public function isRendered() {
        return $this->isRendered;
    }

    public function redirect($url="") {
        header('Location: '.$this->view->baseHref().$url);
        exit;
    }

    protected function getParam($key, $default=null) {
        return $this->request->getParam($key, $default);
    }

    protected function getAllParams() {
        return $this->request->getAllParams();
    }

    public function isAjaxRequest() {
        $nativeAjax     = (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH']=="XMLHttpRequest");
        $iframeUpload   = $this->request->isParamExists('__isAjaxUsed');
        return ($nativeAjax || $iframeUpload);
    }

    public function l($string=null) {
        return $this->translation->translate($string);
    }
}
?>
