<?php
/**
 * Exoof Cache Class
 *
 * @author eugene
 */

class Exoof_Cache {

    /**
     * @var Exoof_Cache
     */
    private static $instance;

    /**
     * @var string Name of the used engine
     */
    private $_engine;

    /**
     * @var Exoof_Cache_Abstract Exoof Cache Engine object
     */
    public $engine;

    /**
     * Constructor is locked due to Singleton pattern
     */
    private function __construct() {
        if (function_exists('eaccelerator_get'))
            $this->_engine = 'eAccelerator';
        elseif (function_exists('apc_store'))
            $this->_engine = 'APC';
        elseif (function_exists('xcache_set'))
            $this->_engine = 'XCache';
        else
            $this->_engine = 'File';
        $class = 'Exoof_Cache_'.$this->_engine;
        $this->engine = new $class();
    }

    /**
     * Returns Cache object
     * @return Exoof_Cache
     */
    public static function getInstance() {
        if (!is_object(self::$instance) || !(self::$instance instanceof Exoof_Cache))
            self::$instance = new Exoof_Cache();
        return self::$instance;
    }

    /**
     * @return string Name of used cache engine
     */
    public function getEngineName() {
        return $this->_engine;
    }

}
?>
