<?php
/**
 * @version 1.1 / Last Updated: 21 September 2010 / Project: MarcAndre / Author: Eugene Belyaev
 */
class Exoof_List_Thumb {
	
    public $isAction = false;
	
	public $field;
	public $title;
        public $size;
	public $sorting = false;
        private $addPath;
	
	public function __construct($field, $title=null, $addPath=null, $size=75, $sorting=false) {
		$this->field = $field;
                $this->size  = $size;
                $this->addPath = $addPath;
		$this->title = ($title)?$title:ucwords(str_replace('_',' ',$field));
		$this->sorting = ($sorting)?true:false;
	}
	
	public function getValue($row) {
		try {
                        $config = Exoof_Factory::factory('Config');
                        $uploadPath = $config->paths->uploadPath;
                        $imgCode  = "<a href='../".$uploadPath.'/'.$this->addPath.$row[$this->field]."' target='_blank'>";
                        $imgCode .= "<img src='../thumbs/{$this->size}/{$this->size}/1/{$uploadPath}/{$this->addPath}{$row[$this->field]}' border='0'/>";
                        $imgCode .= "</a>";
                        return $imgCode;
		}
		catch (Exception $e)
		{
			trigger_error($e->getMessage(),E_USER_WARNING);
		}
		return null;
	}
	
	public function enableSorting() {
        $this->sorting = true;		
	} 
	
	public function disableSorting() {
        $this->sorting = false;		
	} 
	
	
}
?>