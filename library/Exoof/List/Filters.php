<?php
class Exoof_List_Filters {

    private function __construct() {
        return false;
    }

    static public function normalizeDate($field, $title, $row) {
        return date("d/m/Y H:i:s",$row[$field]);
        //$zd  = new Zend_Date($row[$field], null, Exoof_Lang::getLang());
        //$ret =  $zd->get(Zend_Date::DATES);
        //unset($zd);
        return $ret;
    }

    static public function normalizeHtml($field, $title, $row) {
        return __truncate(htmlspecialchars(strip_tags(stripslashes(str_replace('&nbsp;','',$row[$field])))));
    }

    static public function YesNo($field, $title, $row) {
        return ($row[$field])?
                Exoof_Translate::getInstance()->translate('Yes')
                :
                Exoof_Translate::getInstance()->translate('No');
    }

    static public function slashPaddings($field, $title, $row, $path_field, $url_field, $is_linked) {
        $numSlashes = substr_count($row[$path_field].$row[$url_field],"/");

        $row[$field] = self::normalizeHtml($field,$title,$row);

        $alert = !$row[$is_linked]?"<span class='ui-icon ui-icon-alert'></span>":'';

        if ($numSlashes>=0 && strlen($row[$url_field]))
            return $alert.str_repeat('&#8212;',($numSlashes)*1).'&nbsp;'.$row[$field];
        else
            return $alert.$row[$field];
    }

    static public function dialogLink($field, $link, $title, $icon, $row) {
        if (is_array($link)) //if MVC-compatible link array(controller,action...)
        {
            if (is_array($field)) {
                $fields=$field;
            } else {
                $fields[]=$field;
            }
            foreach ($fields as $_field) {
                $link[] = $_field;
                $link[] = $row[$_field];
            }
            $link = join("/",$link);
            /* array('product','edit')
		   * link[] = 'product_id'
		   * link[] = '9'
		   *
		   * result : /product/edit/product_id/9
            */
        }
        else {
            $link_parts = parse_url($link);
            if (!empty($link_parts['query']))
                $link .= "&".$field."=".urlencode($row[$field]);
        }
        //return "<span onclick='return openDialog(\"".$link."\",\"".$title."\");' class='btn-link ui-corner-all ui-icon ui-widget-content ui-icon-".$icon."' title='".$title."'>".$title."</span>";
        return "<a href='".$link."' class='btn-link ui-corner-all ui-icon ui-widget-content ui-icon-".$icon."' role='button' title='".$title."'>".$title."</a>";
    }

    static public function confirmDialogLink($field, $link, $title, $icon, $row) {
        if (is_array($link)) //if MVC-compatible link array(controller,action...)
        {
            if (is_array($field)) {
                $fields=$field;
            } else {
                $fields[]=$field;
            }
            foreach ($fields as $_field) {
                $link[] = $_field;
                $link[] = $row[$_field];
            }
            $link = join("/",$link);
            /* array('product','edit')
		   * link[] = 'product_id'
		   * link[] = '9'
		   *
		   * result : /product/edit/product_id/9
            */
        }
        else {
            $link_parts = parse_url($link);
            if (!empty($link_parts['query']))
                $link .= "&".$field."=".urlencode($row[$field]);
        }
        //return "<span onclick='return openDialog(\"".$link."\",\"".$title."\");' class='btn-link ui-corner-all ui-icon ui-widget-content ui-icon-".$icon."' title='".$title."'>".$title."</span>";
        return "<a href='".$link."' class='btn-link ui-corner-all ui-icon ui-widget-content ui-icon-".$icon."' title='".$title."' confirm='".Exoof_Factory::factory('Translate')->translate('Are you sure')." ? '>".$title."</a>";
    }

    static public function popupLink($field, $link, $title, $icon, $row) {
        if (is_array($link)) //if MVC-compatible link array(controller,action...)
        {
            if (is_array($field)) {
                $fields=$field;
            } else {
                $fields[]=$field;
            }
            foreach ($fields as $_field) {
                $link[] = $_field;
                $link[] = $row[$_field];
            }
            $link = join("/",$link);
            /* array('product','edit')
		   * link[] = 'product_id'
		   * link[] = '9'
		   *
		   * result : /product/edit/product_id/9
            */
        }
        else {
            $link_parts = parse_url($link);
            if (!empty($link_parts['query']))
                $link .= "&".$field."=".urlencode($row[$field]);
        }
        //return "<span onclick='return openDialog(\"".$link."\",\"".$title."\");' class='btn-link ui-corner-all ui-icon ui-widget-content ui-icon-".$icon."' title='".$title."'>".$title."</span>";
        return "<a href='".$link."' target='_blank' onClick='popupWin = window.open(this.href,\"".$title."\", \"width=800,resizable=yes,scrollbars=yes,height=400,top=0\"); popupWin.focus(); return false;' class='btn-link ui-corner-all ui-icon ui-widget-content ui-icon-".$icon."' title='".$title."'>".$title."</a>";
    }

    static public function truncate($field,$title,$row) {
        return __truncate($row[$field]);
    }

}

function __truncate($val) {
    mb_internal_encoding('UTF-8');
    $length   = 50;
    $trailing = "...";

    $length  -= mb_strlen($trailing);
    if (mb_strlen($val)> $length) {
        // string exceeded length, truncate and add trailing dots
        return mb_substr($val,0,$length).$trailing;
    }
    else {
        // string was already short enough, return the string
        $res = $val;
    }
    return $res;
}
?>