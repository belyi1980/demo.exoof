<?php
/**
 * @author Eugene Belyaev
 * @version 1.0 / Last Updated: 19 December 2010 / Project: Kings / Author: Eugene Belyaev
 */
class Exoof_List_ActionDialog {

    public $isAction = true;
    
    public $field;
    public $title;
    public $link;
    public $icon;
    public $callbackFunction = null;

    
    public function __construct($field, $link=null, $title=null, $icon='pencil', $callbackFunction=null) {
    	if ($callbackFunction == null)
    	   $callbackFunction = array('Exoof_List_Filters','dialogLink');
        $this->field = $field;
        $this->link  = str_replace('Controller', '', $link);
        $this->icon  = $icon;
        $this->title = ($title)?$title:ucwords(str_replace('_',' ',$field));
        $this->callbackFunction = $callbackFunction;
    }
    
    public function getValue($row) {
        try {
             if ($this->callbackFunction != null)
              $value = call_user_func($this->callbackFunction, $this->field, $this->link, $this->title, $this->icon, $row);
             else
              $value = $row[$this->field];
              
              return $value;
        }
        catch (Exception $e)
        {
            trigger_error($e->getMessage(),E_USER_WARNING);
        }
        return null;
    }
}
?>