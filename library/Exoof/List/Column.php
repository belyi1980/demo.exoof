<?php
class Exoof_List_Column {
	
    public $isAction = false;
	
	public $field;
	public $title;
	public $callbackFunction = null;
	public $callbackParams   = array();
	public $sorting = false;
	
	public function __construct($field, $title=null, $callbackFunction=null, $callbackParams=null, $sorting=false) {
		$this->field = $field;
		$this->title = ($title)?$title:ucwords(str_replace('_',' ',$field));
		$this->callbackFunction = $callbackFunction;
		if ($callbackParams)
			$this->callbackParams   = $callbackParams;
		$this->sorting = ($sorting)?true:false;
	}
	
	public function getValue($row) {
		try {
			 if ($this->callbackFunction != null)
			 {
                            $params = array_merge(array($this->field, $this->title, $row),$this->callbackParams);
                            return call_user_func_array($this->callbackFunction, $params);
			 }
                         else
                            return $row[$this->field];
		}
		catch (Exception $e)
		{
			trigger_error($e->getMessage(),E_USER_WARNING);
		}
		return null;
	}
	
	public function enableSorting() {
        $this->sorting = true;		
	} 
	
	public function disableSorting() {
        $this->sorting = false;		
	} 
	
	
}
?>