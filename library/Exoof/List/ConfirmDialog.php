<?php
class Exoof_List_ConfirmDialog extends Exoof_List_ActionDialog{
    
    public function __construct($field, $link=null, $title=null, $icon='pencil') {
        parent::__construct($field, $link, $title, $icon, array('Exoof_List_Filters','confirmDialogLink'));
    }
}
?>