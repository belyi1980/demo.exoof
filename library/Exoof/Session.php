<?php
/**
 * Session
 *
 * @author eugene
 */
class Exoof_Session {

    static public function start() {
        if (strlen(session_id()) == 0)
        {
           session_start();
           //session_id($key);
           $key = sha1(dirname($_SERVER['SCRIPT_NAME']));
           if (!isset($_SESSION[$key]))
            $_SESSION[$key] = array();
        }
    }
    
    static public function set($key, $value) {
        self::start();

        $_SESSION[sha1(dirname($_SERVER['SCRIPT_NAME']))][$key] = $value;
    }

    static public function get($key) {
        self::start();
        if (isset($_SESSION[sha1(dirname($_SERVER['SCRIPT_NAME']))][$key]))
                return $_SESSION[sha1(dirname($_SERVER['SCRIPT_NAME']))][$key];
        else
                return null;
    }

    static public function kill($key) {
        self::start();
        unset($_SESSION[sha1(dirname($_SERVER['SCRIPT_NAME']))][$key]);
    }

}
