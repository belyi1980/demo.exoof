<?php
/**
 * Exoof Factory of objects:Exoof_Db, Exoof_Config etc...
 *
 * @author eugene
 */

class Exoof_Factory {

    /**
     * @var Exoof_Factory
     */
    private static $instance;

    /**
     * Constructor is locked due to Singleton pattern
     */
    private function __construct() {
        
    }

    /**
     * Returns Factory object
     * @return Exoof_Factory
     */
    public static function getInstance() {
        if (!is_object(self::$instance) || !(self::$instance instanceof Exoof_Factory))
            self::$instance = new Exoof_Factory();
        return self::$instance;
    }

    /*
     * Return object placed in factory
     *
     * @param string Name of the object placed in the factory. Example: 'Config', 'Db'
     * @return Object
     */
    public static function factory($name, $object=null) {
        static $factoryObjects=array();

        //print_r($factoryObjects);

        if (!$factoryObjects)
            $factoryObjects = array();

        if ($object)
            $factoryObjects[$name] = $object;

        if (array_key_exists($name, $factoryObjects))
            return $factoryObjects[$name];
    }

}
?>
