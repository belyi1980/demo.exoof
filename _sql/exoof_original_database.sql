-- phpMyAdmin SQL Dump
-- version 3.3.2deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Окт 14 2010 г., 15:42
-- Версия сервера: 5.1.41
-- Версия PHP: 5.3.2-1ubuntu4.2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `dev_demo`
--

-- --------------------------------------------------------

--
-- Структура таблицы `exf_administrators`
--

CREATE TABLE IF NOT EXISTS `exf_administrators` (
  `adm_id` int(11) NOT NULL AUTO_INCREMENT,
  `adm_login` varchar(20) NOT NULL,
  `adm_passw` varchar(40) NOT NULL,
  `adm_is_superuser` int(1) NOT NULL DEFAULT '0',
  `adm_email` varchar(140) NOT NULL,
  `adm_sections` text NOT NULL,
  PRIMARY KEY (`adm_id`),
  UNIQUE KEY `adm_login` (`adm_login`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='ExOOF Administrators Table';

--
-- Дамп данных таблицы `exf_administrators`
--

INSERT INTO `exf_administrators` (`adm_id`, `adm_login`, `adm_passw`, `adm_is_superuser`, `adm_email`, `adm_sections`) VALUES
(1, 'demo', '89e495e7941cf9e40e6980d14a16bf023ccd4c91', 1, '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `exf_catalog`
--

CREATE TABLE IF NOT EXISTS `exf_catalog` (
  `sp_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `sp_name` varchar(255) NOT NULL,
  `sp_text` text NOT NULL,
  `lang` varchar(2) NOT NULL,
  PRIMARY KEY (`sp_id`),
  KEY `page_id` (`page_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Exoof Static Pages';

--
-- Дамп данных таблицы `exf_catalog`
--


-- --------------------------------------------------------

--
-- Структура таблицы `exf_gallery`
--

CREATE TABLE IF NOT EXISTS `exf_gallery` (
  `gal_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `gal_name` varchar(255) NOT NULL,
  `gal_text` text NOT NULL,
  `lang` varchar(2) NOT NULL,
  PRIMARY KEY (`gal_id`),
  KEY `page_id` (`page_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Exoof Static Pages';

--
-- Дамп данных таблицы `exf_gallery`
--


-- --------------------------------------------------------

--
-- Структура таблицы `exf_gallery_photo`
--

CREATE TABLE IF NOT EXISTS `exf_gallery_photo` (
  `photo_id` int(11) NOT NULL AUTO_INCREMENT,
  `gal_id` int(11) NOT NULL,
  `photo_thumb_file` varchar(100) NOT NULL,
  `photo_file` varchar(100) NOT NULL,
  `photo_description` text NOT NULL,
  `photo_sort` int(3) DEFAULT NULL,
  PRIMARY KEY (`photo_id`),
  KEY `gal_id` (`gal_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='ExOOF Gallery Photos';

--
-- Дамп данных таблицы `exf_gallery_photo`
--


-- --------------------------------------------------------

--
-- Структура таблицы `exf_mail_blacklist`
--

CREATE TABLE IF NOT EXISTS `exf_mail_blacklist` (
  `email` varchar(130) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `date_added` int(11) NOT NULL,
  PRIMARY KEY (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='ExOOF Blacklist Mails List';

--
-- Дамп данных таблицы `exf_mail_blacklist`
--


-- --------------------------------------------------------

--
-- Структура таблицы `exf_mail_subscribe`
--

CREATE TABLE IF NOT EXISTS `exf_mail_subscribe` (
  `s_name` varchar(100) NOT NULL,
  `s_email` varchar(140) NOT NULL,
  PRIMARY KEY (`s_email`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `exf_mail_subscribe`
--


-- --------------------------------------------------------

--
-- Структура таблицы `exf_news_articles`
--

CREATE TABLE IF NOT EXISTS `exf_news_articles` (
  `news_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `news_title` varchar(300) NOT NULL,
  `news_announce` tinytext NOT NULL,
  `news_thumbnail` varchar(150) NOT NULL,
  `news_text` text NOT NULL,
  `news_date` int(11) NOT NULL,
  `lang` varchar(2) NOT NULL,
  PRIMARY KEY (`news_id`),
  KEY `nb_id` (`page_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `exf_news_articles`
--


-- --------------------------------------------------------

--
-- Структура таблицы `exf_news_blocks`
--

CREATE TABLE IF NOT EXISTS `exf_news_blocks` (
  `nb_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `nb_name` varchar(255) NOT NULL,
  `nb_text` text NOT NULL,
  `lang` varchar(2) NOT NULL,
  PRIMARY KEY (`nb_id`),
  KEY `page_id` (`page_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Exoof Static Pages';

--
-- Дамп данных таблицы `exf_news_blocks`
--


-- --------------------------------------------------------

--
-- Структура таблицы `exf_options`
--

CREATE TABLE IF NOT EXISTS `exf_options` (
  `opt_id` int(11) NOT NULL AUTO_INCREMENT,
  `opt_date_added` int(11) NOT NULL,
  `opt_variable` varchar(255) NOT NULL,
  `opt_title` varchar(255) NOT NULL,
  `opt_type` varchar(20) NOT NULL,
  `opt_value` blob NOT NULL,
  `opt_deletable` int(1) NOT NULL DEFAULT '1',
  `lang` varchar(2) NOT NULL,
  PRIMARY KEY (`opt_id`),
  UNIQUE KEY `opt_variable` (`opt_variable`,`lang`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='Exoof Options';

--
-- Дамп данных таблицы `exf_options`
--

INSERT INTO `exf_options` (`opt_id`, `opt_date_added`, `opt_variable`, `opt_title`, `opt_type`, `opt_value`, `opt_deletable`, `lang`) VALUES
(1, 1287057346, 'slogan', 'slogan', 'text', '', 1, 'ru'),
(2, 1287057346, 'contact phone', 'contact phone', 'text', '', 1, 'ru'),
(3, 1287057346, 'Subscribe text', 'Subscribe text', 'text', '', 1, 'ru'),
(4, 1287057346, 'contact email', 'contact email', 'text', '', 1, 'ru'),
(5, 1287057746, 'Title', 'Title', 'text', '', 1, 'ru'),
(6, 1287058039, 'SeoTitle', 'SeoTitle', 'text', '', 1, 'ru'),
(7, 1287058039, 'SeoDescription', 'SeoDescription', 'text', '', 1, 'ru'),
(8, 1287058039, 'SeoKeywords', 'SeoKeywords', 'text', '', 1, 'ru');

-- --------------------------------------------------------

--
-- Структура таблицы `exf_pages`
--

CREATE TABLE `exf_pages` (
  `page_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `ptype_id` int(11) NOT NULL COMMENT 'Type ID',
  `parent_page_id` int(11) DEFAULT NULL COMMENT 'Parent ID',
  `page_url` varchar(255) NOT NULL COMMENT 'URL',
  `page_path` varchar(500) NOT NULL COMMENT 'Path',
  `page_title` text NOT NULL COMMENT 'Title',
  `page_is_category` int(1) DEFAULT NULL,
  `page_is_linked` int(1) NOT NULL DEFAULT '0',
  `page_date_added` int(11) NOT NULL COMMENT 'Date Added',
  `page_activity` int(1) NOT NULL DEFAULT '1' COMMENT 'Activity',
  `page_level` int(11) DEFAULT NULL,
  `page_meta_title` varchar(255) DEFAULT NULL,
  `page_meta_description` varchar(255) DEFAULT NULL,
  `page_meta_keywords` varchar(255) DEFAULT NULL,
  `lang` varchar(3) NOT NULL,
  `sort_code` varchar(610) NOT NULL COMMENT 'Automatically generated. Do not delete ! Max 10 levels',
  PRIMARY KEY (`page_id`),
  KEY `ptype_id` (`ptype_id`),
  KEY `parent_page_id` (`parent_page_id`),
  KEY `sort_code` (`sort_code`(333)),
  FULLTEXT KEY `page_title` (`page_title`),
  FULLTEXT KEY `page_meta_description` (`page_meta_description`),
  FULLTEXT KEY `page_meta_keywords` (`page_meta_keywords`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Exoof Pages';


--
-- Дамп данных таблицы `exf_pages`
--


-- --------------------------------------------------------

--
-- Структура таблицы `exf_page_types`
--

CREATE TABLE IF NOT EXISTS `exf_page_types` (
  `ptype_id` int(11) NOT NULL AUTO_INCREMENT,
  `ptype_name` varchar(50) NOT NULL,
  `ptype_controller` varchar(100) NOT NULL,
  `ptype_depends_on` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ptype_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='Exoof Page Types';

--
-- Дамп данных таблицы `exf_page_types`
--

INSERT INTO `exf_page_types` (`ptype_id`, `ptype_name`, `ptype_controller`, `ptype_depends_on`) VALUES
(1, 'Main Page', 'StaticPage', NULL),
(2, 'Static Page', 'StaticPage', NULL),
(4, 'News Block', 'NewsBlock', NULL),
(5, 'News Article', 'News', 'News Block');

-- --------------------------------------------------------

--
-- Структура таблицы `exf_payment_transactions`
--

CREATE TABLE IF NOT EXISTS `exf_payment_transactions` (
  `trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `trans_key` varchar(40) NOT NULL COMMENT 'Unique our order number',
  `trans_merchant_order` varchar(15) NOT NULL COMMENT 'Merchant system order number',
  `trans_status` varchar(30) NOT NULL,
  `shopping_info` text NOT NULL,
  `buyer_info` text NOT NULL,
  `trans_amount` float(10,3) NOT NULL,
  `trans_currency` varchar(3) NOT NULL DEFAULT 'USD',
  `trans_date` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `trans_type` varchar(100) NOT NULL COMMENT 'Payment method',
  `trans_comments` varchar(255) NOT NULL,
  PRIMARY KEY (`trans_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `exf_payment_transactions`
--


-- --------------------------------------------------------

--
-- Структура таблицы `exf_static_pages`
--

CREATE TABLE IF NOT EXISTS `exf_static_pages` (
  `sp_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `sp_name` varchar(255) NOT NULL,
  `sp_text` text NOT NULL,
  `lang` varchar(2) NOT NULL,
  PRIMARY KEY (`sp_id`),
  KEY `page_id` (`page_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Exoof Static Pages';

--
-- Дамп данных таблицы `exf_static_pages`
--


-- --------------------------------------------------------

--
-- Структура таблицы `exf_users`
--

CREATE TABLE IF NOT EXISTS `exf_users` (
  `u_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_login` varchar(130) NOT NULL COMMENT 'E-mail is using as login field',
  `u_password` varchar(40) NOT NULL DEFAULT '' COMMENT 'SHA1 Hash',
  `u_fname` varchar(255) NOT NULL DEFAULT '',
  `u_lname` varchar(255) NOT NULL DEFAULT '',
  `u_country` varchar(100) NOT NULL DEFAULT '',
  `u_city` varchar(100) NOT NULL DEFAULT '',
  `u_phone` varchar(50) NOT NULL DEFAULT '',
  `u_date_added` int(11) NOT NULL DEFAULT '0',
  `u_bday` int(11) NOT NULL DEFAULT '0',
  `u_title` varchar(20) NOT NULL,
  `u_company` varchar(100) NOT NULL,
  `u_position` varchar(100) NOT NULL,
  `u_address1` varchar(255) NOT NULL,
  `u_address2` varchar(255) NOT NULL,
  `u_county` varchar(100) NOT NULL,
  `u_post_code` varchar(50) NOT NULL,
  `u_phone2` varchar(255) NOT NULL,
  `u_activation` varchar(40) NOT NULL,
  `u_active` int(1) NOT NULL DEFAULT '0' COMMENT '0-not active, 1-active, 2-temporary',
  PRIMARY KEY (`u_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Exoof Users';


-- Feedback Forms
INSERT INTO `exf_page_types` (
`ptype_id` ,
`ptype_name` ,
`ptype_controller` ,
`ptype_depends_on`
)
VALUES (
'3', 'Feedback Form', 'Forms', NULL
);

CREATE TABLE IF NOT EXISTS `exf_forms` (
  `form_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `form_title` varchar(255) NOT NULL,
  `form_text_before` text NOT NULL,
  `form_text_after` text NOT NULL,
  `form_mailto` varchar(130) NOT NULL,
  `form_send_mail` int(1) NOT NULL,
  `form_enable_captcha` int(1) NOT NULL,
  `form_fields_data` mediumtext NOT NULL COMMENT 'Serialized array',
  `lang` varchar(2) NOT NULL,
  PRIMARY KEY (`form_id`),
  KEY `page_id` (`page_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE `exf_page_types` ADD `ptype_model` VARCHAR( 50 ) NOT NULL;
UPDATE `exf_page_types` SET `ptype_model` = 'StaticPage' WHERE `exf_page_types`.`ptype_id` =1;
UPDATE `exf_page_types` SET `ptype_model` = 'StaticPage' WHERE `exf_page_types`.`ptype_id` =2;
UPDATE `exf_page_types` SET `ptype_model` = 'NewsBlock' WHERE `exf_page_types`.`ptype_id` =4;
UPDATE `exf_page_types` SET `ptype_model` = 'NewsArticle' WHERE `exf_page_types`.`ptype_id` =5;
UPDATE `exf_page_types` SET `ptype_model` = 'Form' WHERE `exf_page_types`.`ptype_id` =3;