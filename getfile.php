<?php
$CONFIG_FILE_PATH = "./config/config.ini";
$config = parse_ini_file($CONFIG_FILE_PATH,true);
require_once $config['paths']['libraryPath'].'/Exoof/Tools/Crypt.php';

$filename = Exoof_Tools_Crypt::Decrypt($_SERVER['QUERY_STRING']);

if(file_exists($filename)) {
    header("Content-Type: ".mime_content_type($filename));
    header("Content-Length: " .(string)(filesize($filename)) );
    header('Content-Disposition: attachment; filename="'.basename($filename).'"');
    readfile($filename);
    exit;
}
else {header ('HTTP/1.0 404 Not Found'); exit;}
?>