$(document).ready(function(){
    $('div.submenu').mouseout(function(){
        $(this).hide();
        $(this).parent().removeClass('over');
    });

    $('#mainMenu td').mouseover(
        function(){
            $(this).addClass('over');
            /*$(this).children('div.submenu')
            .show()*/
        }).mouseout(
        function(){
            $(this).removeClass('over');
        }
        );


    //LABELS
    $('label[required=true]').addClass('required');

    $('form').submit(function(){
        return validateRequiredFields($(this));
    })

//    $('a[role=lightbox]').lightBox();

    $('a[role=ajx]').click(function(){
        $.get(BASE_HREF+$(this).attr('href'),
            function(data){
                if (data.length) {
                    jAlert(data);
                }
            })
        return false;
    });

});

function validateRequiredFields(formObj) {
    returner = true;
    passwCnt = 0;
    passwVal = '';
    //check empty
    $(formObj).find(':input[required=true]').each(
        function() {
            var val = $(this).val();
            var id  = $(this).attr('id');
            var lid = '#label_'+id;
            if ($(this).attr('type')=='checkbox' && !$(this).is(':checked')) {
                $(this).addClass('error');
                $(lid).addClass('error');
                returner = false;
            }
            else {
                if (
                    !(String(val).length > 0) ||
                    ($(this).attr('role') == 'email' && (val.match(/^[a-zA-Z0-9\._-]+@+[a-zA-Z0-9\.-]+\.+[a-z]{2,6}$/))==null) ||
                    ($(this).attr('role') == 'numeric' && (val.match(/^\d*$/))==null) ||
                    (($(this).attr('chars')*1)>0 && val.length != $(this).attr('chars'))
                    )
                {
                    $(this).addClass('error');
                    $(lid).addClass('error');
                    //$(this).after("<span class='error'></span>");
                    returner = false;
                } else {
                    $(this).removeClass('error');
                    $(lid).removeClass('error');
                //$(this).next('span').remove();
                //.next('span[type=error]')
                //.remove();
                }
            }
    }
        
    );

//check password confirm (works only for 2 or more password fields with attr role=confirm)
$(formObj).find(':input[required=true][name][type=password][role=confirm]').each(
    function() {
        passwCnt++;
        if (passwCnt == 1)
            passwVal = $(this).val();
        else {
            if (passwVal != $(this).val()) {//password do not match
                $(this).addClass('error');
                returner = false;
            } else
                $(this).removeClass('error');
        }
    }
    );
return returner;
}
