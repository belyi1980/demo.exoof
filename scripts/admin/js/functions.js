/**
 * @version 1.1 / Last Updated: 19 October 2010 / Project: DEMO / Author: Eugene Belyaev
 */
function ucfirst( str ) {
	
    var f = str.charAt(0).toUpperCase();
    return f + str.substr(1, str.length-1);
}

function getQueryString(str) {
    var arr = str.match(/\/+(\?+[^\/]*)$/);
    return arr[1];
}

function makeURL(sid,obj)
{
    // obj - url field
    // sid - id of source field
    if (typeof(obj)!='object')
        obj = document.getElementById(obj);
    sobj = document.getElementById(sid);
	
    if (!(obj.value.length) && sobj.value.length)
    {
        obj.value = (sobj.value.toLowerCase().replace(/[\?:;&=!#"'\\\/,`@$^+*\(\)]/g,''));
        obj.value = obj.value.replace(/[ _\.]/g,'-');
        obj.value = obj.value.replace(/-$/g,'');
        obj.value = obj.value.replace(/-{2}/g,'-');
		
        rus2translit(obj);
    }
}


function makeTitle(sid,obj)
{
    if (typeof(obj)!='object')
        obj = document.getElementById(obj);
    $(obj).val($('#'+sid).val());
}

function makeKeywords(sid,obj)
{
    if (typeof(obj)!='object')
        obj = document.getElementById(obj);
    var src = $('#'+sid).val();
    src = src.toLowerCase().replace(/[\.\?:;&=!#"'\\\/,`@$^+*\(\)]/g,'');
    src = src.replace(/ +\w{1,3} +/g,' ');
    $(obj).val(src.split(" "));
}


var rusBig 		= new Array( "Э", "Ч", "Ш", "Ё", "Ё", "Ж", "Ю", "Ю", "\Я", "\Я", "А", "Б", "В", "Г", "Д", "Е", "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Щ", "Ъ", "Ы", "Ь"); 
var rusSmall 	= new Array("э", "ч", "ш", "ё", "ё","ж", "ю", "ю", "я", "я", "а", "б", "в", "г", "д", "е", "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "щ", "ъ", "ы", "ь" ); 
var engBig 		= new Array("E\'", "CH", "SH", "YO", "JO", "ZH", "YU", "JU", "YA", "JA", "A","B","V","G","D","E", "Z","I","J","K","L","M","N","O","P","R","S","T","U","F","H","C", "SCH","I","Y", "I"); 
var engSmall 	= new Array("e\'", "ch", "sh", "yo", "jo", "zh", "yu", "ju", "ya", "ja", "a", "b", "v", "g", "d", "e", "z", "i", "j", "k", "l", "m", "n", "o", "p", "r", "s",  "t", "u", "f", "h", "c", "sch", "i", "y", "i"); 
var rusRegBig 	= new Array( /Э/g, /Ч/g, /Ш/g, /Ё/g, /Ё/g, /Ж/g, /Ю/g, /Ю/g, /Я/g, /Я/g, /А/g, /Б/g, /В/g, /Г/g, /Д/g, /Е/g, /З/g, /И/g, /Й/g, /К/g, /Л/g, /М/g, /Н/g, /О/g, /П/g, /Р/g, /С/g, /Т/g, /У/g, /Ф/g, /Х/g, /Ц/g, /Щ/g, /Ъ/g, /Ы/g, /Ь/g); 
var rusRegSmall = new Array( /э/g, /ч/g, /ш/g, /ё/g, /ё/g, /ж/g, /ю/g, /ю/g, /я/g, /я/g, /а/g, /б/g, /в/g, /г/g, /д/g, /е/g, /з/g, /и/g, /й/g, /к/g, /л/g, /м/g, /н/g, /о/g, /п/g, /р/g, /с/g, /т/g, /у/g, /ф/g, /х/g, /ц/g, /щ/g, /ъ/g, /ы/g, /ь/g); 
var engRegBig 	= new Array( /E'/g, /CH/g, /SH/g, /YO/g, /JO/g, /ZH/g, /YU/g, /JU/g, /YA/g, /JA/g, /A/g, /B/g, /V/g, /G/g, /D/g, /E/g, /Z/g, /I/g, /J/g, /K/g, /L/g, /M/g, /N/g, /O/g, /P/g, /R/g, /S/g, /T/g, /U/g, /F/g, /H/g, /C/g, /W/g, /~/g, /Y/g, /'/g); 
var engRegSmall = new Array(/e'/g, /ch/g, /sh/g, /yo/g, /jo/g, /zh/g, /yu/g, /ju/g, /ya/g, /ja/g, /a/g, /b/g, /v/g, /g/g, /d/g, /e/g, /z/g, /i/g, /j/g, /k/g, /l/g, /m/g, /n/g, /o/g, /p/g, /r/g, /s/g, /t/g, /u/g, /f/g, /h/g, /c/g, /w/g, /~/g, /y/g, /'/g); 
 

function rus2translit(obj) 
{
    if (typeof(obj)!='object')
        obj = document.getElementById(obj);
    if (obj.value.length)
    {
        var textar = obj.value;
        if (textar)
        {
            for (i=0; i<rusRegSmall.length; i++)
            {
                textar = textar.replace(rusRegSmall[i], engSmall[i])
            }
			
            for (var i=0; i<rusRegBig.length; i++)
            {
                textar = textar.replace(rusRegBig[i], engBig[i])
            }
        }
        obj.value = textar;
    }
} 