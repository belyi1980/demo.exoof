function renderLinks() {
    renderCTable();

    $('a[role=section]').each(function(){
        var url1 = $(this).attr('href');
        url1 = url1.replace(/\/?$/,'');//trim last slash
        url2 = __curUrl.replace(/\/?$/,'');//trim last slash
        if (new RegExp('^'+url1+'$').test(url2) == true) {
            $(this).addClass('bold').parent().addClass('ui-state-hover');
            var sectitle = $(this).text();
            $('#sectitle').text(sectitle);
        }
    });

    //handle forms submit
    $('#__content form[enctype!=multipart/form-data]').ajaxForm({
        target:'#__content',
        beforeSubmit:saveHTML,
        success:renderLinks
    });

    $('#__content form[enctype=multipart/form-data]').ajaxForm({
        iframe:true,
        data:{
            __isAjaxUsed:'1'
        },
        beforeSubmit:saveHTML,
        success:function(data){
            $('#__content').html(data);
            renderLinks();
        }
    });

    //handle dialog buttons
    $('a[role=button]').click(function() {
        var href = $(this).attr('href');
        if (href.match('/'))
            href = href.split('/',1);
        $('a[role=section]').removeClass('bold').parent().removeClass('ui-state-hover');
        var sectitle = $('a[role=section][href='+href+']').addClass('bold').parent().addClass('ui-state-hover').text();
        $('#sectitle').text(sectitle);        
    });

    //handle links click
    $('#__content').find('a[target!=_blank]').click(function(){
        __curUrl = $(this).attr('href');
        if ($(this).attr('confirm')) {
            if (!confirm($(this).attr('confirm')))
                return false;
        }

        $('#__content').load($(this).attr('href'),function(){
            renderLinks();
        });
        return false;
    });

}

function ajaxSubmitForm(obj) {
    $(obj).ajaxSubmit({
        target:'#__content',
        beforeSubmit:saveHTML,
        success:renderLinks
    });
}

function loadUrl(url) {
    __curUrl = url;
    $('#__content').load(url, function() {
        renderLinks();
    } )
    return true;
}

function renderCTable() {
    $(".content-table th").addClass("ui-widget-header");
    $(".content-table td").addClass("ui-widget-content");
}

function tinyZe(selector) {
    if (!selector)
        selector = 'textarea[wysiwyg=true]';
    $(selector).tinymce();
}

function saveHTML() {

    returner = true;
    $('#__content form :input[required=true][name]').each(
        function() {
            //$(this).next('span[type=error]').remove();
            if (! (String($(this).val()).length > 0))  {
                $(this).addClass('ui-state-error');
                //.after('&nbsp;<span type=error> - error message</span>')
                //.next()
                //.addClass('u1i-widget-content');
                returner = false;
            } else {
                $(this).removeClass('ui-state-error');
            //.next('span[type=error]')
            //.remove();
            }
        }
        );

    if (!returner)
        return false;

    for(n in tinyMCEeditors) {
        //alert(tinyMCE.editors[n]);
        if (tinyMCEeditors[n]) {
            tinyMCE.triggerSave(true,true);
            tinyMCE.execCommand('mceRemoveControl', true, tinyMCEeditors[n]);
            tinyMCEeditors[n] = null;
        }
    }
}