/**
 * jQuery TinyMCE (http://mktgdept.com/jquery-tinymce-plugin)
 * A jQuery plugin for unobtrusive TinyMCE
 * 
 * v0.0.1 - 12 June 2009
 *
 * Copyright (c) 2009 Chad Smith (http://twitter.com/chadsmith)
 * Dual licensed under the MIT and GPL licenses.
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.opensource.org/licenses/gpl-license.php
 * 
 * Add TinyMCE to textareas or an element using: $('textarea').tinymce();
 * Or specify plugins and themes too: $('.advanced').tinymce({theme:'advanced',plugins:'contextmenu,save,print'});
 **/
;jQuery.fn.tinymce=jQuery.fn.tinyMCE=jQuery.fn.TinyMCE=function(){return this.each(function(i)
    {
           //tinyMCE.settings=o;
           this.id=this.id||this.name||(this.className?this.className:'jMCE')+i;
           tinyMCE.execCommand('mceToggleEditor',false,this.id);
           tinyMCEeditors[tinyMCEeditors.length] = this.id;
     })};
