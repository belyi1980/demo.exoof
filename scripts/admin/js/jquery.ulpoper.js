/*
 * jQuery ulPoper Plugin
 * version: 1.00 (12/25/2007)
 * @requires jQuery v1.1 or later
 * @author Eugene Belyaev
 *
 * usage:
   <ul id="menu">
   		<li>Item 1</li>
   		<li><span>Item 2</span>
   			<ul>
   				<li>Item 2.1</li>
   				<li>Item 2.2</li>
   				<li><span>Item 2.3</span>
   					<ul>
   						<li>Item 2.3.1</li>
   						...
   					</ul>
   				</li>
   			</ul>
   		</li>
   		<li>Item 3</li>
   		<li>Item 4</li>
   </ul>
   <script>
   	$('#menu').ulPoper();
   </script>
 *
 */
 
$.fn.ulPoper = function() 
{
	$(this).find("li ul").hide();
	$(this).children("li span").click(function(e) 
		{
			// can`t use "this" here so have to use "e.target" which is clicked object
			$(e.target).parent().children('ul').slideToggle('fast'); 
		});
	
};