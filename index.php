<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
define('NORM_STRIP_SLASHES',1);
define('NORM_ENTITIES',2);
define('NORM_TRIM',4);

if ($_SERVER['QUERY_STRING'] == 'phpinfo') {
    phpinfo ();
    exit;
}

error_reporting(E_ALL ^ E_NOTICE);
$time = microtime();

$configFile = './config/config.ini';
$config     = parse_ini_file($configFile, true);
require_once $config['paths']['libraryPath'].'/Exoof/Bootstrap.php';

$boot = Exoof_Bootstrap::getInstance();
$boot->init($config);
$boot->run();


//echo "<hr />Run in: ".(microtime()-$time);
//echo "<br />Memory usage: <b>".number_format(memory_get_usage()/1024/1024,4).'</b> Mb';
//echo "<br />Included files: ".sizeof(get_included_files());
?>